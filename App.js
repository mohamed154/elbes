/** @format */

import React from "react";
import { Image } from "react-native";
import { Images, AppConfig, Config, Theme } from "@common";
import { AppLoading, Asset, Font } from "@expo";
import { Provider as PaperProvider } from "react-native-paper";
import { Provider } from "react-redux";
import { persistStore } from "redux-persist";
import { PersistGate } from "redux-persist/es/integration/react";
import { WooWorker } from "api-ecommerce";
import WooWorkerr from './src/services/WooWorker';
import store from "@store/configureStore";
import RootRouter from "./src/Router";

function cacheFonts(fonts) {
  return fonts.map((font) => Font.loadAsync(font));
}

export default class App extends React.Component {
  state = { appIsReady: false };

  componentDidMount() {
    console.disableYellowBox = true;
    // const WooCommerce = {
    //   url: "http://elbsapp.com",
    //     consumerKey: "ck_5a3783c2fc9c34a80ab8d4f3ad900b93b124cc5c",
    //     consumerSecret: "cs_187cfca1cf8d7caf90ba9fabc7265f3685f5eebb"
    // },
    // var firebaseConfig = {
    //   apiKey: "AIzaSyBaO7bsVIGECrt_yDnH4ogCO6q8X-bNEQ8",
    //   authDomain: "elbes-8a6ab.firebaseapp.com",
    //   databaseURL: "https://elbes-8a6ab.firebaseio.com",
    //   projectId: "elbes-8a6ab",
    //   storageBucket: "elbes-8a6ab.appspot.com",
    //   messagingSenderId: "537903520556",
    //   appId: "1:537903520556:web:6798db9dd44b04ef"
    // };
    // firebase.initializeApp(firebaseConfig);
    WooWorker.init({
      url: AppConfig.WooCommerce.url,
      consumerKey: AppConfig.WooCommerce.consumerKey,
      consumerSecret: AppConfig.WooCommerce.consumerSecret,
      wp_api: true,
      version: "wc/v2",
      queryStringAuth: true,
    });
  }

  loadAssets = async () => {
    const fontAssets = cacheFonts([
      { OpenSans: require("@assets/fonts/OpenSans-Regular.ttf") },
      { Baloo: require("@assets/fonts/Baloo-Regular.ttf") },

      { Entypo: require("@expo/vector-icons/fonts/Entypo.ttf") },
      {
        "Material Icons": require("@expo/vector-icons/fonts/MaterialIcons.ttf"),
      },
      {
        MaterialCommunityIcons: require("@expo/vector-icons/fonts/MaterialCommunityIcons.ttf"),
      },
      {
        "Material Design Icons": require("@expo/vector-icons/fonts/MaterialCommunityIcons.ttf"),
      },
      { FontAwesome: require("@expo/vector-icons/fonts/FontAwesome.ttf") },
      {
        "simple-line-icons": require("@expo/vector-icons/fonts/SimpleLineIcons.ttf"),
      },
      { Ionicons: require("@expo/vector-icons/fonts/Ionicons.ttf") },
    ]);

    await Promise.all([...fontAssets]);
  };

  render() {
    const persistor = persistStore(store);

    if (!this.state.appIsReady) {
      return (
        <AppLoading
          startAsync={this.loadAssets}
          onFinish={() => this.setState({ appIsReady: true })}
        />
      );
    }

    return (
      <Provider store={store}>
        <PersistGate persistor={persistor}>
          <PaperProvider theme={Config.Theme.isDark ? Theme.dark : Theme.light}>
            <RootRouter />
          </PaperProvider>
        </PersistGate>
      </Provider>
    );
  }
}
