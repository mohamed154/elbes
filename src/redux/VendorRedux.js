/**
 * Created by InspireUI on 06/03/2017.
 *
 * @format
 */

import { Languages } from "@common";
import { RefAPI } from "@services";

const types = {
  FETCH_PRODUCTS_VENDOR_PENDING: "FETCH_PRODUCTS_VENDOR_PENDING",
  FETCH_PRODUCTS_VENDOR_SUCCESS: "FETCH_PRODUCTS_VENDOR_SUCCESS",
  FETCH_PRODUCTS_VENDOR_FAILURE: "FETCH_PRODUCTS_VENDOR_FAILURE",
  FETCH_VENDOR_FAILURE: "FETCH_VENDOR_FAILURE",
  FETCH_VENDOR_SUCCESS: "FETCH_VENDOR_SUCCESS",
  FETCH_VENDOR_PENDING: "FETCH_VENDOR_PENDING",
  FETCH_ALL_VENDOR_FAILURE: "FETCH_ALL_VENDOR_FAILURE",
  FETCH_ALL_VENDOR_SUCCESS: "FETCH_ALL_VENDOR_SUCCESS",
};

export const actions = {
  fetchProductsByVendor: async (dispatch, storeId, per_page = 2000, page = 1) => {
    let data = {
      storeId,
      per_page,
      page,
    };

    dispatch({
      type: types.FETCH_PRODUCTS_VENDOR_PENDING,
      message: Languages.pending,
    });
    const json = await RefAPI.getProductsByVendor(data);
    setTimeout(() => {
      if (json === undefined) {
        dispatch({
          type: types.FETCH_PRODUCTS_VENDOR_FAILURE,
          message: Languages.ErrorMessageRequest,
        });
      } else {
        dispatch({
          type: types.FETCH_PRODUCTS_VENDOR_SUCCESS,
          items: json,
          page,
          finish: true,
        });
      }
    }, 3000);
  },

  fetchVendor: async (dispatch, vendorId) => {
    dispatch({
      type: types.FETCH_VENDOR_PENDING,
      message: Languages.pending,
    });
    let json = await RefAPI.getVendor(vendorId);
    if (json === undefined) {
      dispatch({
        type: types.FETCH_VENDOR_FAILURE,
        message: Languages.ErrorMessageRequest,
      });
    } else {
      dispatch({
        type: types.FETCH_VENDOR_SUCCESS,
        vendor: json,
        finish: true,
      });
    }
  },
  fetchAllVendors: async (dispatch) => {
    let data = [];
    let page = 1;
    let json = await RefAPI.getAllVendors(page);
    while(json.length != 0) {
      data.push(...json);
      page = page + 1;
      json = await RefAPI.getAllVendors(page);
    }
    if (data === undefined) {
      dispatch({
        type: types.FETCH_ALL_VENDOR_FAILURE,
        message: Languages.ErrorMessageRequest,
      });
    } else {
      dispatch({
        type: types.FETCH_ALL_VENDOR_SUCCESS,
        items: data,
        finish: true,
      });
    }
  },
};

const initialState = {
  isFetching: false,
  isFetchingProducts: false,
  error: null,
  products: [],
  productFinish: false,
  vendor: null,
  list: [],
};

export const reducer = (state = initialState, action) => {
  const { type, error, items, finish } = action;
  switch (type) {

    case types.FETCH_VENDOR_PENDING: {
      return Object.assign({}, state, {
        isFetching: true,
        error: null
      });
    }

    case types.FETCH_PRODUCTS_VENDOR_PENDING: {
      return Object.assign({}, state, {
        isFetchingProducts: true,
        error: null,
      });
    }

    case types.FETCH_PRODUCTS_VENDOR_FAILURE: {
      return Object.assign({}, state, {
        isFetchingProducts: false,
        error,
      });
    }

    case types.FETCH_PRODUCTS_VENDOR_SUCCESS: {
      return Object.assign({}, state, {
        isFetchingProducts: false,
        products: items,
        error: null,
        productFinish: finish,
      });
    }

    case types.FETCH_VENDOR_SUCCESS: {
      return Object.assign({}, state, {
        isFetching: false,
        error: null,
        vendor: action.vendor,
      });
    }

    case types.FETCH_VENDOR_FAILURE: {
      return Object.assign({}, state, {
        isFetching: false,
        error,
      });
    }

    case types.FETCH_ALL_VENDOR_SUCCESS: {
      return Object.assign({}, state, {
        isFetching: false,
        error: null,
        list: items,
      });
    }

    case types.FETCH_ALL_VENDOR_FAILURE: {
      return Object.assign({}, state, {
        isFetching: false,
        error,
      });
    }

    default: {
      return state;
    }
  }
};
