/**
 * Created by InspireUI on 14/02/2017.
 *
 * @format
 */

const types = {
  LOGOUT: "LOGOUT",
  LOGIN: "LOGIN_SUCCESS",
  FINISH_INTRO: "FINISH_INTRO",
  CLEAR_CHAT: "CLEAR_CHAT",
};

export const actions = {
  login: (user, token) => {
    return  { type: types.LOGIN, user, token };
  },
  logout() {
    return { type: types.LOGOUT };
  },
  finishIntro() {
    return { type: types.FINISH_INTRO };
  },
  clearChat() {
    return { type: types.CLEAR_CHAT };
  },
};

const initialState = {
  user: null,
  token: null,
  finishIntro: null,
  chat: {},
};

export const reducer = (state = initialState, action) => {
  const { type, user, token } = action;

  switch (type) {
    case types.LOGOUT:
      return Object.assign({}, initialState);
    case types.LOGIN:
      return { ...state, user, token };
    case types.FINISH_INTRO:
      return { ...state, finishIntro: true };
    case types.CLEAR_CHAT: {
      return { ...state, chat: {} };
    }
    default:
      return state;
  }
};
