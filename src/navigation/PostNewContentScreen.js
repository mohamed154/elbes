/** @format */

import React, { Component } from "react";
import { PostNewContent } from "@containers";

export default class PostNewContentScreen extends Component {
  static navigationOptions = {
    header: null,
  };

  render() {
    const { navigation } = this.props;
    const { navigate } = navigation;

    return (
      <PostNewContent
        navigation={navigation}
        onBack={() => navigation.goBack()}
        onClose={() => navigate("UserProfileScreen")}
      />
    );
  }
}
