/** @format */

import React, {PureComponent} from "react";
import {Logo, Menu, EmptyView, CartWishListIcons} from "./IconNav";

import {Color, Config, Constants, Images, Styles} from "@common";
import {TabBarIcon} from "@components";
import {Categories} from "@containers";
import {BackHandler} from "react-native";
import {connect} from "react-redux";

class VendorsScreen extends PureComponent {
  static navigationOptions = ({navigation}) => ({
    headerTitle: Logo(),
    headerLeft: Menu(),
    headerRight: CartWishListIcons(navigation),
    headerTintColor: Color.headerTintColor,
    headerStyle: Styles.Common.toolbar,
    headerTitleStyle: Styles.Common.headerStyle,
  });

  render() {
    const {navigate} = this.props.navigation;
    return (
      <Categories
        onViewProductScreen={(item) => navigate("ProductDetailScreen", item)}
        onViewCategory={(item) => {
          navigate("CategoryScreen", item);
        }}
        onViewVendor={(store) => {
          navigate("Vendor", {store});
        }}
        onViewCartScreen={() => navigate("CartScreen")}
        vendors={true}
        onChat={(author) => {
          const { user } = this.props;
          if (user != null) {
            navigate("Chat", { author, backToRoute: "DetailScreen" });
          } else {
            navigate("LoginScreen");
          }
        }}
      />
    );
  }
}

const mapStateToProps = ({ user }) => ({ user: user.user });
export default connect(mapStateToProps)(VendorsScreen);
