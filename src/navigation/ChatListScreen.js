/** @format */

// @flow
import React from "react";
import {
  Animated,
  Text,
  Dimensions,
  Platform,
  View,
  StyleSheet,
} from "react-native";
import {
  Config,
  Constants,
  Device,
  Languages,
  withTheme,
  Images,
} from "@common";
import { TabBarIcon } from "@components";
import { ChatList } from "@containers";
import { Back } from "@navigation/IconNav";
import {BackWhite, CartWishListIcons} from "./IconNav";
const { width } = Dimensions.get("window");

@withTheme
export default class ChatListScreen extends React.Component {
  static navigationOptions = ({ navigation }) => ({
    header: null,
    headerRight: CartWishListIcons(navigation),
    tabBarIcon: ({ tintColor }) => (
      <TabBarIcon icon={Images.icons.iconMessage} tintColor={tintColor} />
    ),
  });

  state = {
    scrollY: new Animated.Value(0),
  };

  render() {
    const { navigate } = this.props.navigation;
    const { scrollY } = this.state;
    const {
      theme: {
        colors: { background, text },
      },
    } = this.props;

    return (
      <View style={[styles.body, { backgroundColor: background }]}>
        <View style={styles.headerView}>
          <Text style={styles.headerLabel}>{Languages.chatList}</Text>
          <View style={styles.homeMenu}>{BackWhite(this.props.navigation)}</View>
          <View style={[styles.headerRight]}>{CartWishListIcons(navigate)}</View>
        </View>

        <Animated.ScrollView
          ref={(sc) => (this._scroll = sc)}
          scrollEventThrottle={1}
          contentContainerStyle={[
            styles.scrollView,
            { backgroundColor: background },
          ]}
          onScroll={Animated.event(
            [{ nativeEvent: { contentOffset: { y: scrollY } } }],
            { useNativeDriver: true }
          )}>
          <ChatList
            onChat={(author) =>
              navigate("Chat", { author, backToRoute: "chatList" })
            }
            onLogin={() => navigate("login")}
            onHome={() => navigate("Home")}
          />
        </Animated.ScrollView>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  body: {
    flex: 1,
    backgroundColor: "#FFF",
  },
  scrollView: {
    flex: 1,
    backgroundColor: "#FFF",
    marginTop: 50,
  },
  //
  headerLabel: {
    color: "#fff",
    fontSize: 24,
    fontFamily: Constants.fontHeader,
    marginBottom: 0,
    marginLeft: 22,
    position: "absolute",
    ...Platform.select({
      ios: {
        top: 60,
      },
      android: {
        top: 34,
        left: 40
      },
    }),
  },
  headerView: {
    width,
    backgroundColor: "#0ca2ff",
    ...Platform.select({
      ios: {
        height: 60,
      },
      android: {
        height: 80//Config.showStatusBar ? 70 : 50,
      },
    }),
  },
  homeMenu: {
    marginLeft: 0,
    position: "absolute",
    // ...Platform.select({
    //   ios: {
    //     top: Device.isIphoneX ? 50 : 22,
    //   },
    //   android: {
    //     top: Config.showStatusBar ? 30 : 10,
    //   },
    // }),
    top: 20,
    left: 0,
    zIndex: 9,
  },
  headerRight: {
    marginLeft: 0,
    position: "absolute",
    // ...Platform.select({
    //   ios: {
    //     top: Device.isIphoneX ? 50 : 22,
    //   },
    //   android: {
    //     top: Config.showStatusBar ? 30 : 10,
    //   },
    // }),
    top: 20,
    right: 0,
    zIndex: 9,
    marginTop: 3
  }
});
