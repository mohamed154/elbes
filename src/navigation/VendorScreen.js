/** @format */

import React, { PureComponent } from "react";
import PropTypes from "prop-types";
import { ProductVendor } from "@components";
import { Color, Styles } from "@common";
import { Back, CartWishListIcons } from "./IconNav";
import {connect} from "react-redux";

class VendorScreen extends PureComponent {
  static navigationOptions = ({ navigation }) => ({
    tabBarVisible: false,
    headerLeft: Back(navigation),
    headerRight: CartWishListIcons(navigation),

    //headerTintColor: Color.headerTintColor,
    headerStyle: Styles.Common.toolbar,
    headerTransparent: true,
  });

  static propTypes = {
    navigation: PropTypes.object.isRequired,
  };

  render() {
    const { getParam, navigate } = this.props.navigation;
    const vendor = getParam("store");

    return (
      <ProductVendor
        vendor={vendor}
        navigation={this.props.navigation}
        onViewVendor={(store) => navigate("Vendor", { store })}
        onViewProductScreen={(item) => navigate("DetailScreen", item)}
        onViewCartScreen={() => navigate("CartScreen")}
        onChat={(author) => {
          const { user } = this.props;
          if (user != null) {
            navigate("Chat", { author, backToRoute: "DetailScreen" });
          } else {
            navigate("LoginScreen");
          }
        }}
      />
    );
  }
}

const mapStateToProps = ({ user }) => ({ user: user.user });
export default connect(mapStateToProps)(VendorScreen);
