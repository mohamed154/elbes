/** @format */

import React, { Component } from "react";
import { PostNewListing } from "@containers";

export default class PostNewListingScreen extends Component {
  static navigationOptions = {
    header: null,
  };

  render() {
    const { navigation } = this.props;
    return (
      <PostNewListing
        onBack={() => navigation.goBack(null)}
        next={(post) => navigation.navigate("postNewContent", { post })}
      />
    );
  }
}
