/** @format */

import React, { Component } from "react";

import {Back, Menu, NavBarLogo} from "./IconNav";
import { Languages, Color, Styles } from "@common";
import { Setting } from "@containers";

export default class SettingScreen extends Component {
  static navigationOptions = ({ navigation }) => ({
    // title: Languages.Language,
    headerLeft: Back(navigation),
    headerTitle: NavBarLogo({ navigation }),
    headerTintColor: Color.headerTintColor,
    headerStyle: Styles.Common.toolbar,
    headerTitleStyle: Styles.Common.headerStyle,
  });
  render() {
    return <Setting {...this.props} />;
  }
}
