/** @format */

import React, { PureComponent } from "react";
import {Menu, EmptyView, Back, CartWishListIcons, Logo} from "./IconNav";

import { Images, Config, Constants, Color, Styles, Languages } from "@common";
import { WishList } from "@containers";
import {BackHandler} from "react-native";

export default class WishListScreen extends PureComponent {
  // static navigationOptions = ({ navigation }) => ({
  //   title: Languages.WishList,
  //   header: null,
  //   headerLeft: Back(navigation, Images.icons.backsWhite),
  //   headerRight: CartWishListIcons(navigation),
  // });

  static navigationOptions = ({navigation}) => ({
    headerTitle: Logo(),
    headerLeft: Menu(),
    headerRight: CartWishListIcons(navigation),
    headerTintColor: Color.headerTintColor,
    headerStyle: Styles.Common.toolbar,
    headerTitleStyle: Styles.Common.headerStyle,
  });

  componentDidMount() {
    BackHandler.addEventListener('hardwareBackPress', this.onBackButtonPressAndroid)
  }

  onBackButtonPressAndroid = () => {
    this.props.navigation.navigate('HomeScreen');
  };

  render() {
    const { navigate } = this.props.navigation;
    // const rootNavigation = this.props.screenProps.rootNavigation;

    return (
      <WishList
        onViewProduct={(product) => navigate("DetailScreen", product)}
        onViewHome={() => navigate("Home")}
        navigation={this.props.navigation}
        onViewVendor={(store) => {navigate("Vendor", {store})}}
      />
    );
  }
}
