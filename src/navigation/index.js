/** @format */

import React from "react";
import {Color, Images, Languages} from "@common";
import {TabBar, TabBarIcon} from "@components";
import {
  View,
  Dimensions,
  I18nManager,
  StyleSheet,
  Animated,
  Platform,
  Text
} from "react-native";
import {
  createAppContainer,
  createStackNavigator,
  createBottomTabNavigator,
  NavigationActions,
} from "react-navigation";
import {TabViewPagerPan} from "react-native-tab-view";
import HomeScreen from "./HomeScreen";
import NewsScreen from "./NewsScreen";
import NewsDetailScreen from "./NewsDetailScreen";
import CategoriesScreen from "./CategoriesScreen";
import SubCategoriesScreen from "./SubCategoriesScreen";
import CategoryScreen from "./CategoryScreen";
import DetailScreen from "./DetailScreen";
import CartScreen from "./CartScreen";
import MyOrdersScreen from "./MyOrdersScreen";
import WishListScreen from "./WishListScreen";
import SearchScreen from "./SearchScreen";
import LoginScreen from "./LoginScreen";
import SignUpScreen from "./SignUpScreen";
import CustomPageScreen from "./CustomPageScreen";
import ListAllScreen from "./ListAllScreen";
import SettingScreen from "./SettingScreen";
import VendorScreen from "./VendorScreen";
import UserProfileScreen from "./UserProfileScreen";
import FiltersScreen from "./FiltersScreen";
import ChatScreen from "./ChatScreen";
import ChatListScreen from "./ChatListScreen";
import AddressScreen from "./AddressScreen";
import AddAddressScreen from "./AddAddressScreen";
import PostNewListingScreen from "./PostNewListingScreen";
import PostNewContentScreen from "./PostNewContentScreen";
import TransitionConfig from "./TransitionConfig";
import VendorsScreen from "./VendorsScreen";
import MyOrderDetailsScreen from "./MyOrderDetailsScreen";

const {width} = Dimensions.get("window");

const NewsStack = createStackNavigator(
  {
    News: {screen: NewsScreen},
    NewsDetailScreen: {screen: NewsDetailScreen},
  },
  {
    navigationOptions: {
      gestureDirection: I18nManager.isRTL ? "inverted" : "default",
    },
  }
);

const CategoryStack = createStackNavigator(
  {
    CategoriesScreen: {screen: CategoriesScreen},
    SubCategoriesScreen: {screen: SubCategoriesScreen},
    CategoryScreen: {screen: CategoryScreen},
    DetailScreen: {
      screen: DetailScreen,
      navigationOptions: {tabBarVisible: false},
    },
    Vendor: {screen: VendorScreen},
    CartScreen: {screen: CartScreen},
    Chat: {screen: ChatScreen},
  },
  {
    navigationOptions: {
      gestureDirection: I18nManager.isRTL ? "inverted" : "default",
    },
  }
);

const CategoryDetailStack = createStackNavigator(
  {
    CategoryScreen: {screen: CategoryScreen},
    CartScreen: {screen: CartScreen},
    Chat: {screen: ChatScreen},
    DetailScreen: {
      screen: DetailScreen,
      navigationOptions: {tabBarVisible: false},
    },
  },
  {
    navigationOptions: {
      gestureDirection: I18nManager.isRTL ? "inverted" : "default",
    },
  }
);

const WishListStack = createStackNavigator(
  {
    WishListScreen: {screen: WishListScreen},
    DetailScreen: {screen: DetailScreen},
    CartScreen: {screen: CartScreen},
    Chat: {screen: ChatScreen},
    Vendor: {screen: VendorScreen},
  },
  {
    navigationOptions: {
      gestureDirection: I18nManager.isRTL ? "inverted" : "default",
    },
  }
);

const SearchStack = createStackNavigator(
  {
    Search: {screen: SearchScreen},
    DetailScreen: {screen: DetailScreen},
    FiltersScreen: {screen: FiltersScreen},
    CategoryScreen: {screen: CategoryScreen},
    Chat: {screen: ChatScreen},
    CartScreen: {screen: CartScreen},
  },
  {
    navigationOptions: {
      gestureDirection: I18nManager.isRTL ? "inverted" : "default",
    },
  }
);

const HomeStack = createStackNavigator(
  {
    Home: {screen: HomeScreen},
    ListAllScreen: {screen: ListAllScreen},
    DetailScreen: {screen: DetailScreen},
    Vendor: {screen: VendorScreen},
    SettingScreen: {screen: SettingScreen},
    CategoryScreen: {screen: CategoryScreen},
    CartScreen: {screen: CartScreen},

    // WishListScreen: { screen: WishListScreen },

    UserProfile: {screen: UserProfileScreen},
    ChatList: {screen: ChatListScreen},
    Chat: {screen: ChatScreen},
    Address: {screen: AddressScreen},
    AddAddress: {screen: AddAddressScreen},
    // Cart: { screen: CartScreen },
    MyOrders: {screen: MyOrdersScreen},
    MyOrderDetailsScreen: {screen: MyOrderDetailsScreen},
  },
  {
    navigationOptions: {
      gestureResponseDistance: {horizontal: width / 2},
      gesturesEnabled: true,
      gestureDirection: I18nManager.isRTL ? "inverted" : "default",
    },
  }
);

const postNewListingStack = createStackNavigator(
  {
    postNewListing: {screen: PostNewListingScreen},
    postNewContent: {screen: PostNewContentScreen},
  },
  {
    navigationOptions: {
      gestureResponseDistance: {horizontal: width / 2},
      gesturesEnabled: true,
      gestureDirection: I18nManager.isRTL ? "inverted" : "default",
    },
  }
);

const CartScreenStack = createStackNavigator(
  {
    Cart: {screen: CartScreen},
    DetailScreen: {screen: DetailScreen},
  },
  {
    navigationOptions: {
      gestureDirection: I18nManager.isRTL ? "inverted" : "default",
    },
  }
);

const UserProfileStack = createStackNavigator(
  {
    UserProfile: {screen: UserProfileScreen},
    ChatList: {screen: ChatListScreen},
    Chat: {screen: ChatScreen},
    Address: {screen: AddressScreen},
    AddAddress: {screen: AddAddressScreen},
    Cart: {screen: CartScreen},
    MyOrders: {screen: MyOrdersScreen},
    // WishListScreen: { screen: WishListScreen },
  },
  {
    navigationOptions: {
      gestureDirection: I18nManager.isRTL ? "inverted" : "default",
    },
  }
);

const LoginStack = createStackNavigator(
  {
    LoginScreen: {screen: LoginScreen},
    SignUpScreen: {screen: SignUpScreen},
  },
  {
    mode: "modal",
    header: null,
    transitionConfig: () => TransitionConfig,
  }
);

const VendorStack = createStackNavigator(
  {
    VendorsScreen: {screen: VendorsScreen},
    Vendor: {screen: VendorScreen},
    CartScreen: {screen: CartScreen},
    Chat: {screen: ChatScreen},
    DetailScreen: {
      screen: DetailScreen,
      navigationOptions: {tabBarVisible: false},
    },
  },
  {
    navigationOptions: {
      gestureDirection: I18nManager.isRTL ? "inverted" : "default",
    },
  }
);

const AppNavigator = createBottomTabNavigator(
  {
    Default: {
      screen: HomeStack,
      navigationOptions: {
        tabBarIcon: ({tintColor}) => (
          <View style={{alignItems: 'center'}}>
            <TabBarIcon icon={Images.IconHome} tintColor={tintColor}/>
            <Text style={{color: tintColor}}>{Languages.home}</Text>
          </View>
        ),
      },
    },
    CategoriesScreen: {
      screen: CategoryStack,
      navigationOptions: {
        tabBarIcon: ({tintColor}) => (
          <View style={{alignItems: 'center'}}>
            <TabBarIcon icon={Images.IconCategory} tintColor={tintColor}/>
            <Text style={{color: tintColor}}>{Languages.categories}</Text>
          </View>
        ),
      },
    },
    VendorsScreen: {
      screen: VendorStack,
      navigationOptions: {
        tabBarIcon: ({tintColor}) => (
          <View style={{alignItems: 'center'}}>
            <TabBarIcon icon={Images.IconShops} tintColor={tintColor}/>
            <Text style={{color: tintColor}}>{Languages.shops}</Text>
          </View>
        ),
      },
    },
    Search: {
      screen: SearchStack,
      navigationOptions: {
        tabBarIcon: ({tintColor}) => (
          <View style={{alignItems: 'center'}}>
            <TabBarIcon icon={Images.IconSearch} tintColor={tintColor}/>
            <Text style={{color: tintColor}}>{Languages.search}</Text>
          </View>
        ),
      },
    },
    WishListScreen: {
      screen: WishListStack,
      navigationOptions: {
        tabBarIcon: ({tintColor}) => (
          <View style={{alignItems: 'center'}}>
            <TabBarIcon wishlistIcon icon={Images.IconWishList} tintColor={tintColor}/>
            <Text style={{color: tintColor}}>{Languages.favorites}</Text>
          </View>
        ),
      },
    },
    PostNewListing: {
      screen: postNewListingStack,
      navigationOptions: {
        tabBarIcon: ({tintColor}) => (
          <View style={{alignItems: 'center'}}>
            <TabBarIcon
              icon={Images.icons.iconPlus}
              tintColor={tintColor}
              cssView={{
                ...Platform.select({
                  ios: {
                    top: -24,
                    borderRadius: 30,
                  },
                  android: {
                    top: 0,
                    borderRadius: 30,
                  }
                }),
                backgroundColor: "transparent",
              }}
              css={{
                width: 36,
                height: 36,
                tintColor: Color.tabbarTint,
                resizeMode: "contain",
              }}
            />
          </View>
        ),
      },
    },
    // UserProfileScreen: {
    //   screen: UserProfileStack,
    //   navigationOptions: {
    //     tabBarIcon: ({ tintColor }) => (
    //       <TabBarIcon
    //         icon={Images.IconUser}
    //         tintColor={tintColor}
    //       />
    //     ),
    //   },
    // },
    // MyOrders: { screen: MyOrdersScreen },
    NewsScreen: {screen: NewsStack},
    // SettingScreen: { screen: SettingScreen },
    LoginStack: {screen: LoginStack},
    CustomPage: {screen: CustomPageScreen},
    CategoryDetail: {screen: CategoryDetailStack},
  },
  {
    initialRouteName: "Default",
    tabBarComponent: TabBar,
    tabBarPosition: "bottom",
    swipeEnabled: true,
    animationEnabled: false,
    tabBarOptions: {
      showIcon: true,
      showLabel: true,
      activeTintColor: Color.tabbarTint,
      inactiveTintColor: Color.tabbarColor,
    },
    lazy: true,
    navigationOptions: {
      gestureDirection: I18nManager.isRTL ? "inverted" : "default",
    },
  }
);

export default createAppContainer(AppNavigator);

/**
 * prevent duplicate screen
 */
const navigateOnce = (getStateForAction) => (action, state) => {
  const {type, routeName} = action;
  return state &&
  type === NavigationActions.NAVIGATE &&
  routeName === state.routes[state.routes.length - 1].routeName
    ? null
    : getStateForAction(action, state);
};

NewsStack.router.getStateForAction = navigateOnce(
  NewsStack.router.getStateForAction
);
CategoryStack.router.getStateForAction = navigateOnce(
  CategoryStack.router.getStateForAction
);
CategoryDetailStack.router.getStateForAction = navigateOnce(
  CategoryDetailStack.router.getStateForAction
);
WishListStack.router.getStateForAction = navigateOnce(
  WishListStack.router.getStateForAction
);
HomeStack.router.getStateForAction = navigateOnce(
  HomeStack.router.getStateForAction
);
SearchStack.router.getStateForAction = navigateOnce(
  SearchStack.router.getStateForAction
);
CartScreenStack.router.getStateForAction = navigateOnce(
  CartScreenStack.router.getStateForAction
);
VendorStack.router.getStateForAction = navigateOnce(
  VendorStack.router.getStateForAction
);
