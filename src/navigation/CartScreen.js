/** @format */

import React, { PureComponent } from "react";

import { Cart } from "@containers";

export default class CartScreen extends PureComponent {
  static navigationOptions = ({ navigation }) => ({
    header: null,
  });

  render() {
    const { goBack, navigate } = this.props.navigation;
    return (
      <Cart
        onMustLogin={() => {
          navigate("LoginScreen", { onCart: true });
        }}
        onBack={() => goBack()}
        onFinishOrder={() => navigate("MyOrders")}
        onViewHome={() => navigate("Default")}
        onViewProduct={(product) => navigate("DetailScreen", product)}
        navigation={this.props.navigation}
      />
    );
  }
}
