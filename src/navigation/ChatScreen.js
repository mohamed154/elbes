/** @format */

// @flow
import React from 'react'
import { View, SafeAreaView} from 'react-native'
import { Chat } from '@containers'
import { ChatToolBar } from '@components'
import {withTheme, Styles} from '@common'

@withTheme
export default class ChatScreen extends React.Component {
  static navigationOptions = ({ navigation }) => ({
    header: null,
    tabBarVisible: false,
  })

  _goBack = (backToRoute) => {
    const { goBack, navigate } = this.props.navigation;
    // if (typeof backToRoute != 'undefined') {
    //   navigate(backToRoute)
    // } else {
      goBack()
    // }
  }

  render() {
    const { getParam } = this.props.navigation
    const author = getParam('author', null)
    const backToRoute = getParam('backToRoute', undefined)

    if (author != null) {
      return (
        <SafeAreaView style={[Styles.container, {marginTop: 15}]}>
          <View style={{height: 15, width: '100%'}} />
          <ChatToolBar
            onBack={() => this._goBack(backToRoute)}
            label={
              author.name
                ? author.name
                : author.last_name + ' ' + author.first_name
            }
          />
          <View style={{height: 40, width: '100%'}} />
          <Chat author={author} />
        </SafeAreaView>
      )
    } else {
      return (
        <SafeAreaView style={Styles.container}>
          <ChatToolBar onBack={() => this._goBack()} label={''} />
        </SafeAreaView>
      )
    }
  }
}
