/** @format */

import React, { Component } from "react";
import { Images, Styles, Color, Languages } from "@common";
import { ProductList } from "@components";
import {Back, NavBarLogo, NavBarText} from "./IconNav";

export default class ListAllScreen extends Component {
  static navigationOptions = ({ navigation }) => ({
    headerLeft: Back(navigation, Images.icons.backsWhite),
    headerTitle: NavBarLogo({ navigation }),
    headerStyle: Styles.Common.toolbar,
    // header: null
  });

  render() {
    const { state, navigate } = this.props.navigation;
    const params = state.params;

    return (
      <ProductList
        headerImage={params.config.image && params.config.image.length > 0 ? {uri: params.config.image} : null}
        config={params.config}
        page={1}
        navigation={this.props.navigation}
        index={params.index}
        onViewProductScreen={(item) => navigate("DetailScreen", item)}
        onViewCartScreen={() => navigate("CartScreen")}
      />
    );
  }
}
