/** @format */

import React, { PureComponent } from "react";
import PropTypes from "prop-types";
import { View } from "react-native";
import { Color, Styles } from "@common";
import { SafeAreaView } from "@components";
import { Detail } from "@containers";
import { connect } from "react-redux";
import { Logo, Back, CartWishListIcons } from "./IconNav";

class DetailScreen extends PureComponent {
  static navigationOptions = ({ navigation }) => ({
    headerTitle: Logo(),
    tabBarVisible: false,
    headerLeft: Back(navigation),
    headerRight: CartWishListIcons(navigation),

    headerTintColor: Color.headerTintColor,
    headerStyle: Styles.Common.toolbar,
    headerTitleStyle: Styles.Common.headerStyle,
  });

  static propTypes = {
    navigation: PropTypes.object.isRequired,
  };

  _goBack = (backToRoute) => {
    const { goBack, getParam, navigate } = this.props.navigation;
    let route = backToRoute;
    if (typeof backToRoute !== "undefined") {
      const fromSearch = getParam("fromSearch", false);
      if (fromSearch) {
        route = "Search";
      }
      navigate(route);
    } else {
      goBack();
    }
  };

  render() {
    const { state, navigate } = this.props.navigation;

    return (
      <SafeAreaView isSafeAreaBottom>
        <View style={{ flex: 1 }}>
          {typeof state.params !== "undefined" && (
            <Detail
              product={state.params.product}
              onViewCart={() => navigate("CartScreen")}
              onViewProductScreen={(product) => {
                this.props.navigation.setParams(product);
                // navigate("DetailScreen", product)
              }}
              navigation={this.props.navigation}
              onLogin={() => navigate("LoginScreen")}
              onBack={() => this._goback(backToRoute)}
              onViewVendor={(store) => navigate("Vendor", { store })}
              onChat={(author) => {
                const { user } = this.props;
                if (user != null) {
                  navigate("Chat", { author, backToRoute: "DetailScreen" });
                } else {
                  navigate("LoginScreen");
                }
              }}
            />
          )}
        </View>
      </SafeAreaView>
    );
  }
}

const mapStateToProps = ({ user }) => ({ user: user.user });
export default connect(mapStateToProps)(DetailScreen);
