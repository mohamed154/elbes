/** @format */

import React, {PureComponent} from "react";
import {Logo, CartWishListIcons, Back} from "./IconNav";
import {Color, Config, Constants, Images, Styles} from "@common";
import {TabBarIcon} from "@components";
import {Categories} from "@containers";
import {SubCategories} from "@containers";

export default class SubCategoriesScreen extends PureComponent {
  static navigationOptions = ({navigation}) => ({
    headerTitle: Logo(),
    headerLeft: Back(navigation),
    headerRight: CartWishListIcons(navigation),
    headerTintColor: Color.headerTintColor,
    headerStyle: Styles.Common.toolbar,
    headerTitleStyle: Styles.Common.headerStyle,
  });

  render() {
    const {navigate} = this.props.navigation;
    return (
      <SubCategories
        onViewProductScreen={(item) => navigate("DetailScreen", item)}
        onViewSubCategory={(item) => {
          navigate("CategoryScreen", item);
        }}
        onViewVendor={(store) => {
          navigate("Vendor", {store});
        }}
        onViewCartScreen={() => navigate("CartScreen")}
      />
    );
  }
}
