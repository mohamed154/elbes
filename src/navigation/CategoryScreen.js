/** @format */

import React, { PureComponent } from "react";
import PropTypes from "prop-types";
import { BackHandler } from "react-native";
import { Color, Images, Styles } from "@common";
import { TabBarIcon } from "@components";
import { Category } from "@containers";
import { Logo, Back, BackTwice, EmptyView, CartWishListIcons } from "./IconNav";

export default class CategoryScreen extends PureComponent {
  static navigationOptions = ({ navigation }) => ({
    headerTitle: Logo(),
    headerLeft: BackTwice(navigation),
    // headerLeft: EmptyView(),
    headerRight: /*EmptyView(),*/  CartWishListIcons(navigation),
    tabBarIcon: ({ tintColor }) => (
      <TabBarIcon
        css={{ width: 18, height: 18 }}
        icon={Images.IconCategory}
        tintColor={tintColor}
      />
    ),

    headerTintColor: Color.headerTintColor,
    headerStyle: Styles.Common.toolbar,
    headerTitleStyle: Styles.Common.headerStyle,
  });

  static propTypes = {
    navigation: PropTypes.object.isRequired,
  };

  componentDidMount() {
    BackHandler.addEventListener('hardwareBackPress', this.handleBackPress);
  }

  componentWillUnmount() {
    BackHandler.removeEventListener('hardwareBackPress', this.handleBackPress);
  }

  handleBackPress = () => {
    this.props.navigation.goBack(null);
    this.props.navigation.goBack(null);
    return true;
  };

  render() {
    const { navigate } = this.props.navigation;
    return (
      <Category
        onViewProductScreen={(item) => {
          navigate("DetailScreen", item);
        }}
        onViewCartScreen={() => navigate("CartScreen")}
      />
    );
  }
}
