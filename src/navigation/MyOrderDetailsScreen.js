/** @format */

import React, { Component } from "react";
import PropTypes from "prop-types";
import { MyOrders, MyOrderDetails } from "@containers";

export default class MyOrderDetailsScreen extends Component {
  static navigationOptions = ({ navigation }) => ({
    header: null,
  });

  static propTypes = {
    navigation: PropTypes.object,
  };

  render() {
    const { navigate } = this.props.navigation;
    return (
      <MyOrderDetails
        navigate={this.props.navigation}
        onViewHomeScreen={() => navigate("Home")}
      />
    );
  }
}
