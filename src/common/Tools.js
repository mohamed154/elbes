/** @format */

import { AsyncStorage } from "react-native";
import { Constants, Languages, Images } from "@common";
import { AllHtmlEntities } from "html-entities";
import sanitizeHtml from "sanitize-html";
import truncate from "lodash/truncate";
import URI from "urijs";

export default class Tools {
  /**
   * refresh the tab bar & read later page
   */
  static getImage(data, imageSize) {
    if (typeof data === "undefined" || data == null) {
      return Constants.PlaceHolder;
    }
    if (typeof imageSize === "undefined") {
      imageSize = "medium";
    }

    const getImageSize = (mediaDetail) => {
      let imageURL = "";
      if (typeof mediaDetail.sizes !== "undefined") {
        if (typeof mediaDetail.sizes[imageSize] !== "undefined") {
          imageURL = mediaDetail.sizes[imageSize].source_url;
        }

        if (imageURL == "" && typeof mediaDetail.sizes.medium !== "undefined") {
          imageURL = mediaDetail.sizes.medium.source_url;
        }

        if (imageURL == "" && typeof mediaDetail.sizes.full !== "undefined") {
          imageURL = mediaDetail.sizes.full.source_url;
        }
      }

      if (typeof data.better_featured_image != null) {
        imageURL = data.better_featured_image.source_url;
      }

      return imageURL;
    };

    let imageURL =
      typeof data.better_featured_image !== "undefined" &&
      data.better_featured_image != null
        ? data.better_featured_image.source_url
        : Constants.PlaceHolderURL;

    if (
      typeof data.better_featured_image !== "undefined" &&
      data.better_featured_image !== null
    ) {
      if (typeof data.better_featured_image.media_details !== "undefined") {
        imageURL = getImageSize(data.better_featured_image.media_details);
      }
    }

    if (imageURL == "") {
      return Constants.PlaceHolderURL;
    }

    console.log(imageURL);

    return imageURL;
  }

  static formatText(desc, limit) {
    if (typeof limit === "undefined") {
      limit = 50;
    }

    if (typeof desc === "undefined") {
      return "";
    }

    var desc = desc.replace("<p>", "").replace("</p>", "");
    desc = truncate(desc, { length: limit, separator: " " });

    return AllHtmlEntities.decode(desc);
  }

  static getDescription(description, limit) {
    if (typeof limit === "undefined") {
      limit = 50;
    }

    if (typeof description === "undefined") {
      return "";
    }

    let desc = description.replace("<p>", "");
    desc = truncate(desc, { length: limit, separator: " " });

    return AllHtmlEntities.decode(desc);
  }

  static getFormatDescription(desc, limit) {
    if (typeof limit === "undefined") {
      limit = 50;
    }
    // desc.replace('<p>', '');
    let htmlContent = sanitizeHtml(desc, {
      allowedTags: ["span", "br", "ul", "ol", "li", "h1", "h2", "h3"],
      allowedAttributes: {
        a: ["href"],
        img: ["src"],
      },
    });
    htmlContent = truncate(htmlContent, { length: limit, separator: " " });

    return AllHtmlEntities.decode(htmlContent);
  }

  static getLinkVideo(content) {
    const regExp = /^.*((www.youtube.com\/)|(v\/)|(\/u\/\w\/)|(embed\/)|(watch\??v?=?))([^#&\?\/\ ]*).*/;
    let embedId = "";
    let youtubeUrl = "";

    URI.withinString(content, (url) => {
      var match = url.match(regExp);
      if (match && match[7].length === 11) {
        embedId = match[7];
        youtubeUrl = "www.youtube.com/embed/" + embedId;
      }
    });
    return youtubeUrl;
  }

  static async getFontSizePostDetail() {
    const data = await AsyncStorage.getItem("@setting_fontSize");
    if (typeof data !== "undefined") {
      return parseInt(data);
    }
    return Constants.fontText.size;
  }

  /**
   * getName user
   * @user
   */
  static getName = (user) => {
    if (user != null) {
      if (
        typeof user.last_name != "undefined" ||
        typeof user.first_name != "undefined"
      ) {
        let first = user.first_name != null ? user.first_name : "";
        let last = user.last_name != null ? user.last_name : "";
        return first + " " + last;
      } else if (typeof user.name != "undefined" && user.name != null) {
        return user.name;
      }
      return Languages.Guest;
    }
    return Languages.Guest;
  };

  /**
   * getAvatar
   * @user
   */
  static getAvatar = (user) => {
    if (user) {
      if (user.avatar_url) {
        return {
          uri: user.avatar_url,
        };
      } else if (user.picture) {
        return {
          uri: user.picture.data.url,
        };
      }
      return Images.defaultAvatar;
    }

    return Images.defaultAvatar;
  };

  /**
   * @get store name
   */
  static getShopName = (vendor) => {
    let shopName = "";
    if (
      vendor &&
      typeof vendor.shop_name !== "undefined" &&
      vendor.shop_name != ""
    ) {
      shopName = vendor.shop_name;
    } else if (
      vendor &&
      typeof vendor.pv_shop_name !== "undefined" &&
      vendor.pv_shop_name != ""
    ) {
      shopName = vendor && vendor.pv_shop_name;
    } else {
      shopName = vendor && vendor.store_name;
    }
    return shopName;
  };
}
