/** @format */
import { DefaultTheme, DarkTheme } from 'react-native-paper'

const dark = {
  ...DarkTheme,
  colors: {
    ...DarkTheme.colors,
    text: 'rgba(255, 255, 255, 0.9)',
    link: '#25a8dd',
    primary: 'tomato',
    accent: 'yellow',
    lineColor: '#222229',
    background: '#1B1920' // '#242424' // '#232D4C'
  },
}

const light = {
  ...DefaultTheme,
  colors: {
    ...DefaultTheme.colors,
    primary: '#0ca2ff',
    lineColor: "#F7F8FB",
    background: '#fff',
    accent: 'yellow',
    link: '#25a8dd',
  },
}

export default { dark, light }
