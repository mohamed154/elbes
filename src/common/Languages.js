/** @format */
import {DangerZone} from "expo";

import Localization from '@custom/Localization';


const en = {
  home: "Home",
  categories: "Categories",
  shops: "Shops",
  search: "Search",
  favorites: "Favorites",

  Exit: "Exit",
  ExitConfirm: "Are you sure you want to exit this app",
  YES: "YES",
  OK: "OK",
  ViewMyOrders: "View My Oders",
  CANCEL: "CANCEL",
  Confirm: "Confirm",

  // Scene's Titles
  Home: "Home",
  Intro: "Intro",
  Product: "Product",
  Cart: "Cart",
  WishList: "WishList",
  orderDetailsTitle: "Order Details",

  // Home
  products: "products",

  // TopBar
  ShowFilter: "Sub Categories",
  HideFilter: "Hide",
  Sort: "Sort",
  textFilter: "Recent",
  recents: "Recents",

  // Category
  ThereIsNoMore: "There is no more product to show",

  // Product
  AddtoCart: "Add to Cart",
  AddtoWishlist: "Add to Wishlist",
  ProductVariations: "Variations",
  NoVariation: "This product don't have any variation",
  AdditionalInformation: "Description",
  NoProductDescription: "No Product Description",
  ProductReviews: "Reviews",
  writeReview: "review",
  NoReview: "This product don't have any reviews ...yet",
  BUYNOW: "BUY NOW",
  OutOfStock: "OUT OF STOCK",
  ProductLimitWaring: "You can't add more than 10 product",
  EmptyProductAttribute: "This product don't have any attributes",
  ProductFeatures: "Features",
  ErrorMessageRequest: "Can't get data from server",
  NoConnection: "No internet connection",
  ProductRelated: "Related Products",
  chatWithUs: "Chat with us...",

  // Cart
  NoCartItem: "There is no product in cart",
  Total: "Total",
  EmptyCheckout: "Sorry, you can't check out an empty cart",
  RemoveCartItemConfirm: "Remove this product from cart?",
  MyCart: "Cart",
  Order: "Order",
  ShoppingCart: "Shopping Cart",
  ShoppingCartIsEmpty: "Your Cart is Empty",
  AddProductToCart: "Add a product to the shopping cart",
  TotalPrice: "Total Price:",
  YourDeliveryInfo: "Your Shipping Detail",
  ShopNow: "Shop Now",
  YourChoice: "Your cart:",
  YourSale: "Your Sale:",
  SubtotalPrice: "Subtotal Price:",
  BUYNOW: "BUY NOW",
  Items: "items",
  Item: "item",
  ThankYou: "Thank you",
  FinishOrderCOD: "You can use to number of order to track shipping status",
  FinishOrder:
    "Thank you so much for your purchased, to check your delivery status please go to My Orders",
  NextStep: "Next Step",
  ConfirmOrder: "Confirm Order",
  RequireEnterAllFileds: "Please enter all fields",
  Error: "Error",
  InvalidEmail: "Invalid email address",
  Finish: "Finish",

  // Wishlist
  NoWishListItem: "There is no item in wishlist",
  MoveAllToCart: "Add all to cart",
  EmptyWishList: "Empty wishlist",
  EmptyAddToCart: "Sorry, the wishlist is empty",
  RemoveWishListItemConfirm: "Remove this product from wishlist?",
  CleanAll: "Clean All",

  // Sidemenu
  SignIn: "Log In",
  Login: "Login",
  SignOut: "Log Out",
  GuestAccount: "Guest Account",
  CantReactEmailError:
    "We can't reach your email address, please try other login method",
  NoEmailError: "Your account don't have valid email address",
  EmailIsNotVerifiedError:
    "Your email address is not verified, we can' trust you",
  Login: "Login",
  Logout: "Logout",
  Shop: "Shop",
  Category: "Category",

  // Checkout
  Checkout: "Checkout",
  ProceedPayment: "Proceed Payment",
  Purchase: "Purchase",
  CashOnDelivery: "Cash on Delivery",
  CreditCard: "Credit Card",
  PaymentMethod: "Payment Method - Not select",
  PaymentMethodError: "Please select your payment method",
  PayWithCoD: "Your purchase will be pay when goods were delivered",
  PayWithPayPal: "Your purchase will be pay with PayPal",
  Paypal: "paypal",
  Stripe: "stripe",
  PayWithStripe: "Your purchase will be pay with Stripe",
  ApplyCoupon: "Apply",
  CouponPlaceholder: "Coupon Code",
  Apply: "Apply",
  Applying: "Applying",
  Back: "Back",
  CardNamePlaceholder: "Name written on card",
  BackToHome: "Back to Home",
  OrderCompleted: "Your order was completed",
  OrderCanceled: "Your order was canceled",
  OrderFailed: "Something went wrong...",
  OrderCompletedDesc: "Your order id is ",
  OrderCanceledDesc:
    "You have canceled the order. The transaction has not been completed",
  OrderFailedDesc:
    "We have encountered an error while processing your order. The transaction has not been completed. Please try again",
  OrderTip:
    'Tip: You could track your order status in "My Orders" section from side menu',
  Delivery: "Delivery",
  Payment: "Payment",
  Complete: "Complete",
  EnterYourFirstName: "Enter your First Name",
  EnterYourLastName: "Enter your Last Name",
  EnterYourEmail: "Enter your email",
  EnterYourPhone: "Enter your phone",
  EnterYourAddress: "Enter your address",
  CreateOrderError: "Cannot create new order. Please try again later",

  // myorder
  MyOrder: "My Order",
  NoOrder: "You don't have any orders",
  OrderDate: "Order Date: ",
  OrderStatus: "Status: ",
  OrderPayment: "Payment method: ",
  OrderTotal: "Total: ",
  OrderDetails: "Show detail",

  News: "News",
  PostDetails: "Post Details",
  FeatureArticles: "Feature articles",
  MostViews: "Most views",
  EditorChoice: "Editor choice",

  // settings
  Settings: "Settings",
  BASICSETTINGS: "BASIC SETTINGS",
  Language: "Language",
  INFO: "INFO",
  About: "About us",

  // language
  AvailableLanguages: "Available Languages",
  SwitchLanguage: "Switch Language",
  SwitchLanguageConfirm: "Switch language require an app reload, continue?",

  // about us
  AppName: "elbs",
  AppDescription: "ecommerce app",
  AppContact: " Contact us at: mstore.io",
  AppEmail: " Email: support@mstore.io",
  AppCopyRights: "© MSTORE 2016",

  // contact us
  contactus: "Contact Us",

  // form
  NotSelected: "Not selected",
  EmptyError: "This field is empty",
  DeliveryInfo: "Delivery Info",
  FirstName: "First Name",
  LastName: "Last Name",
  Address: "Address",
  City: "Town/City",
  State: "State",
  NotSelectedError: "Please choose one",
  Postcode: "Postcode",
  Country: "Country",
  Email: "Email",
  Phone: "Phone Number",
  Note: "Note",

  // search
  Search: "Search",
  SearchPlaceHolder: "Search product by name",
  NoResultError: "Your search keyword did not match any products.",
  Details: "Details",

  // filter panel
  Categories: "Categories",
  Loading: "LOADING...",
  welcomeBack: "Welcome, ",
  logoutSuccess: "Logout Successfully !",
  seeAll: "Show All",
  Vendors: "Vendors",

  // Layout
  cardView: "Card ",
  simpleView: "List View",
  twoColumnView: "Two Column ",
  threeColumnView: "Three Column ",
  listView: "List View",
  default: "Default",
  advanceView: "Advance ",
  horizontal: "Horizontal ",

  couponCodeIsExpired: "This coupon code is expired",
  invalidCouponCode: "This coupon code is invalid",
  remove: "Remove",
  applyCouponSuccess: "Congratulations! Coupon code applied successfully ",
  reload: "Reload",

  ShippingType: "Shipping method",

  // Place holder
  TypeFirstName: "Type your first name",
  TypeLastName: "Type your last name",
  TypeAddress: "Type address",
  TypeCity: "Type your town or city",
  TypeState: "Type your state",
  TypeNotSelectedError: "Please choose one",
  TypePostcode: "Type postcode",
  TypeEmail: "Type email (Ex. acb@gmail.com), ",
  TypePhone: "Type your phone number",
  TypeNote: "Note",
  TypeCountry: "Select country",
  SelectPayment: "Select Payment method",
  close: "CLOSE",
  noConnection: "NO INTERNET ACCESS",

  // user profile screen
  AccountInformations: "Account Informations",
  PushNotification: "Push notification",
  Privacy: "Privacy policies",
  SelectCurrency: "Select currency",
  Name: "Name",
  Currency: "Currency",
  Languages: "Languages",
  Guest: "Guest",
  FacebookLogin: "Facebook Login",
  Or: "Or",
  UserOrEmail: "Username or Email",
  DontHaveAccount: "Don't have account? ",
  accountDetails: "Account Details",
  username: "Username",
  email: "Email",
  generatePass: "Use generate password",
  password: "Password",
  signup: "Sign Up",
  alreadySignUp: "You've registered already ? Sign In NOW!",
  profileDetail: "Profile Details",
  firstName: "First name",
  lastName: "Last name",
  chatList: "Chat List",

  // Horizontal
  featureProducts: "Feature Products",
  bagsCollections: "Headphone Collections",
  onSaleProducts: "On Sale Products",
  topRatedProducts: "Top Rated Products",
  watchBestSeller: "SmartWatches Best Seller",
  accessCollections: "Accessories Collections",
  promotionProducts: "Promotions",
  citchenThings: "Citchen Things",
  illumination: "Illumination Best Seller",

  // Modal
  Select: "Select",
  Cancel: "Cancel",

  // review
  vendorTitle: "Shop",
  comment: "Leave a review",
  yourcomment: "Your comment",
  placeComment:
    "Tell something about your experience or leave a tip for others",
  writeReview: "Write A Review",
  thanksForReview:
    "Thanks for the review, your content will be verify by the admin and will be published later",
  errInputComment: "Please input your content to submit",
  errRatingComment: "Please rating to submit",
  send: "Send",
  termCondition: "Term & Condition",
  Subtotal: "Subtotal",
  Discount: "Discount",
  Shipping: "Shipping",
  Recents: "Recents",
  Filters: "Filters",
  Princing: "Princing",
  Filter: "Filter",
  ClearFilter: "Clear Filter",
  ProductCatalog: "Product Catalog",
  ProductTags: "Product Tags",

  //extra
  vendorTitle: "Shop",
  comment: "Leave a review",
  yourcomment: "Your comment",
  placeComment:
    "Tell something about your experience or leave a tip for others",
  writeReview: "Review",
  thanksForReview:
    "Thanks for the review, your content will be verify by the admin and will be published later",
  errInputComment: "Please input your content to submit",
  errRatingComment: "Please rating to submit",
  send: "Submit review",
  allowAccessCameraroll: "You need to turn on to allow access camera roll",

  // search screen
  recents: "Recents",
  filter: "Filters: ",
  Recommend: "Recommend",

  //language
  currentLanguage: "Current Language",
  SwitchRtlConfirm: "Switch language require an app reload, continue?",
  Confirm: "Confirm",
  changeLanguage: "Change Language",
  OK: "OK",
  CANCEL: "CANCEL",
  next: "next",

  //chat
  typeAmessage: "Type a message here",

  // vendor info
  storeName: "Store Name",
  storeEmail: "Email",
  storePhone: "Phone",
  storeAddress: "Address",
  storeLocation: "Location",
  storeRating: "Rating",
  review: "Reviews",

  //new listing
  publish: "Publish",
  allowAccessCameraroll: "You need to turn on to allow access camera roll",
  postHeading: "Product Title",
  selectCategory: "Select Category",
  errorMsgConnectServer: "Can not connect to server.",
  submit: "Submit",
  composeTheContent: "Type the content...",
  readyToSubmit: "Ready to submit?",
  notYet: "Not yet",
  successfull: "Successfull",
  close: "Close",
  msgConfirmClearHistory: "Are you sure you want to clear history?",
  confirmation: "Confirmation",
  listingType: "Listing Type",
  myListings: "My Listings",
  selectOnLocation: "Select Location",
  location: "Listing Location",
  informationField: "Fields Information",
  listingTitle: "Type Listing Title",
  listingContent: "Product Description",
  productPrice: 'Price',
  points: "Points",
  wallet_balance: "Wallet Balance",
  checkout_failed_msg: "Can not checkout with this account, please sign in with a customer account",
  couponUsedBefore: "you have used this coupon before"
};

const ar = {
  home: "الرئيسية",
  categories: "التصنيفات",
  shops: "المحلات",
  search: "البحث",
  favorites: "المفضلات",

  Exit: "خروج",
  ExitConfirm: "هل انت متاكد من الخروج من هذا التطبيق",
  YES: "نعم",
  OK: "حسنا",
  ViewMyOrders: "عرض الطلبات",
  CANCEL: "الغاء",
  Confirm: "تأكيد",

  // Scene's Titles
  Home: "الرئيسية",
  Intro: "المقدمة",
  Product: "منتج",
  Cart: "السلة",
  WishList: "القائمة المفضلة",

  // Home
  products: "المنتجات",

  // TopBar
  ShowFilter: "التصنيفات الفرعسة",
  HideFilter: "اخفاء",
  Sort: "ترتيب",
  textFilter: "مؤخرا",
  recents: "اخر المنتجات",

  // Category
  ThereIsNoMore: "لا يوجد منتج للعرض",

  // Product
  AddtoCart: "اضافة الى السلة",
  AddtoWishlist: "اضافة الى القائمة المفضلة",
  ProductVariations: "الاختلافات",
  NoVariation: "هذا المنتج لا يحتوي على اختلافات",
  AdditionalInformation: "الوصف",
  NoProductDescription: "لا يوجد وصف للمنتج",
  ProductReviews: "المراجعات",
  writeReview: "راجع",
  NoReview: "هذا النتج لايحتوي على مراجعات",
  BUYNOW: "اشتري الان",
  OutOfStock: "نفذت الكمية",
  ProductLimitWaring: "لا يمكنك اكثر من 10 منتجات",
  EmptyProductAttribute: "هذا المنتج لا يحتوي على سمات",
  ProductFeatures: "المميزات",
  ErrorMessageRequest: "يتعذر وصول البيانات من السرفر",
  NoConnection: "لا يوجد اتصال بالانترنت",
  ProductRelated: "المنتجات المتعلقة",
  chatWithUs: "تواصل معنا...",

  // Cart
  NoCartItem: "لا يوجد منتج في السلة",
  Total: "المجموع",
  EmptyCheckout: "لا يمكنك الشراء و السلة فارغة",
  RemoveCartItemConfirm: "حذف هذا المنتج من السلة ؟",
  MyCart: "السلة",
  Order: "طلب",
  ShoppingCart: "سلة المشتريات",
  ShoppingCartIsEmpty: "السلة فارغة",
  AddProductToCart: "اضف منتج الى سلة المشتريات",
  TotalPrice: "إجمالي السعر:",
  YourDeliveryInfo: "تفاصيل الشحن",
  ShopNow: "تسوق الان",
  YourChoice: "سلتك:",
  YourSale: "مبيعاتك:",
  SubtotalPrice: "إجمالي السعر الفرعي:",
  BUYNOW: "اشتري الان",
  Items: "العناصر",
  Item: "عنصر",
  ThankYou: "شكرا لك",
  FinishOrderCOD: "يمكنك استخدام لعدد من الأوامر لتتبع حالة الشحن",
  FinishOrder:
    "شكرًا جزيلاً على مشترياتك ، للتحقق من حالة التسليم ، يرجى الانتقال إلى طلباتي",
  NextStep: "الخطوة التالية",
  ConfirmOrder: "تأكيد الطلب",
  RequireEnterAllFileds: "من فضلك ادخل جميل الحقول",
  Error: "خطا",
  InvalidEmail: "بريد الكتروني خاطئ",
  Finish: "انهاء",

  // Wishlist
  NoWishListItem: "لا يوجد عنصر في القائمة المفضلة",
  MoveAllToCart: "اضافة الجميع الى السلة",
  EmptyWishList: "قائمة المفضلة فارغة",
  EmptyAddToCart: "القائمة المفضلة فارغة",
  RemoveWishListItemConfirm: "حذف هذا العنصر من القائمة المفضلة",
  CleanAll: "مسح الكل",

  // Sidemenu
  SignIn: "تسحيل الدخول",
  SignOut: "تسجيل الخروج",
  GuestAccount: "حساب الضيف",
  CantReactEmailError:
    "We can't reach your email address, please try other login method",
  NoEmailError: "لا يمكننا الوصول إلى عنوان بريدك الإلكتروني ، يرجى تجربة طريقة تسجيل الدخول اخرى",
  EmailIsNotVerifiedError:
    "لم يتم التحقق من عنوان بريدك الإلكتروني ،لا يمكننا الوثوق بك",
  Login: "تسجيل الدخول",
  Logout: "تسجيل الخروج",
  Shop: "المتجر",
  Category: "التصنيف",

  // Checkout
  Checkout: "شراء",
  ProceedPayment: "اتمام الدفع",
  Purchase: "شراء",
  CashOnDelivery: "الدفع عند التوصيل",
  CreditCard: "بطاقة إتمان",
  PaymentMethod: "طريقة الدفع - غير محددة",
  PaymentMethodError: "من فضلك اختر طريقة الدفع",
  PayWithCoD: "سيتم دفع عملية الشراء عند تسليم البضائع",
  PayWithPayPal: "عملية الشراء ستتم عبر paypal",
  Paypal: "paypal",
  Stripe: "stripe",
  PayWithStripe: "عملية الشراء ستتم عبر stripe",
  ApplyCoupon: "تطبيق",
  CouponPlaceholder: "رقم الكوبون",
  Apply: "تطبيق",
  Applying: "جاري التطبيق",
  Back: "الرجوع",
  CardNamePlaceholder: "الاسم المكتوب على الكارت",
  BackToHome: "الرجوع الى القائمة الرئيسية",
  OrderCompleted: "لقد تم انهاء الطلب",
  OrderCanceled: "لقد تم الغاء الطلب",
  OrderFailed: "لقد حدث خطا ما ...",
  OrderCompletedDesc: "رقم الاوردر هو... ",
  OrderCanceledDesc:
    "لقد ألغيت الطلب. لم تكتمل المعاملة",
  OrderFailedDesc:
    "لقد واجهنا خطأ أثناء معالجة طلبك. لم تكتمل المعاملة. يرجى المحاولة مرة أخرى",
  OrderTip:
    'نصيحة: يمكنك تتبع حالة طلبك في قسم "طلبي" من القائمة الجانبية',
  Delivery: "التسليم",
  Payment: "الدفع",
  Complete: "اكتمال",
  EnterYourFirstName: "ادخل الاسم الاول",
  EnterYourLastName: "ادخل الاسم الاخير",
  EnterYourEmail: "ادخل العنوان البريدي",
  EnterYourPhone: "ادخل رقم التليفون",
  EnterYourAddress: "ادخل عنوانك",
  CreateOrderError: "لا يمكن إنشاء طلب جديد. يرجى المحاولة مرة أخرى لاحقًا",

  // myorder
  MyOrder: "طلباتي",
  NoOrder: "ليس لديك اي طلبات",
  OrderDate: "تاريخ الطلب: ",
  OrderStatus: "الحالة: ",
  OrderPayment: "طريقة الدفع: ",
  OrderTotal: "اجمالي: ",
  OrderDetails: "عرض التفاصيل",

  News: "الاخبار",
  PostDetails: "تفاصيل المنشور",
  FeatureArticles: "مقالات مميزة",
  MostViews: "الاكثر مشاهدة",
  EditorChoice: "اختيار المحرر",

  // settings
  Settings: "الاعدادات",
  BASICSETTINGS: "الاعدادات الاساسية",
  Language: "اللغة",
  INFO: "معلومات",
  About: "عننا",

  // language
  AvailableLanguages: "اللغات المتاحة",
  SwitchLanguage: "تبديل اللغة",
  SwitchLanguageConfirm: "تبديل اللغة يتطلب اعادة تشغيل التطبيق هل انت متاكد ؟",

  // about us
  AppName: "elbs",
  AppDescription: "React Native mCommerce app",
  AppContact: "",
  AppEmail: "",
  AppCopyRights: "© MSTORE 2016",

  // contact us
  contactus: "تواصل معنا",

  // form
  NotSelected: "غيد محدد",
  EmptyError: "هذا الحقل فارغ",
  DeliveryInfo: "معلومات التوصيل",
  FirstName: "الاسم الاول",
  LastName: "الاسم الاخير",
  Address: "العنوان",
  City: "المدينة",
  State: "الولاية",
  NotSelectedError: "من فضلك اختر واحدة ",
  Postcode: "الكود البريدي",
  Country: "البلد",
  Email: "البريد الالكتروني",
  Phone: "رقم التليفون",
  Note: "ملاحظات",

  // search
  Search: "البحث",
  SearchPlaceHolder: "البحث عن المنتج عن طريق الاسم",
  NoResultError: "لم تتطابق كلمة البحث الرئيسية مع أي منتجات.",
  Details: "التفاصيل",

  // filter panel
  Categories: "التصنيفات",
  Loading: "جاري التحميل...",
  welcomeBack: "مرحبا, ",
  logoutSuccess: "تم تسجيل الخروج بنجاح!",
  seeAll: "عرض الكل",
  Vendors: "المتاجر",

  // Layout
  cardView: "السلة ",
  simpleView: "عرض كقائمة",
  twoColumnView: "عمودين ",
  threeColumnView: "ثلاثة اعمدة ",
  listView: "عرض كقائمة",
  default: "الافتراضي",
  advanceView: "عرض متقدم ",
  horizontal: "عرض افقي ",

  couponCodeIsExpired: " لقد انتهت صلاحية هذا الكوبون",
  invalidCouponCode: "هذا الكود غير صالح",
  remove: "إزالة",
  applyCouponSuccess: "تهانينا! تم تطبيق رمز الكوبون بنجاح ",
  reload: "اعادة التحميل",

  ShippingType: "طريقة التوصيل",

  // Place holder
  TypeFirstName: "ادخل الاسم الاول",
  TypeLastName: "ادخل الاسم الاخير",
  TypeAddress: "ادخل العنوان",
  TypeCity: "ادخل المدينة",
  TypeState: "ادخل ولايتك",
  TypeNotSelectedError: "من فضلك اختر واحدة",
  TypePostcode: "ادخل الرمز البريدي",
  TypeEmail: "ادخل عنوانك البريدي ",
  TypePhone: "ادخل رقم الهاتف",
  TypeNote: "ملاحظات",
  TypeCountry: "اختر البلد",
  SelectPayment: "اختر طريقة الدفع",
  close: "اغلاق",
  noConnection: "لا يوجد اتصال بالانترنت",

  // user profile screen
  AccountInformations: "معلومات الحساب",
  PushNotification: "الاشعارات",
  Privacy: "سياسات الخصوصية",
  SelectCurrency: "اختر العملة",
  Name: "الاسم",
  Currency: "العملة",
  Languages: "اللغات",
  Guest: "ضيف",
  FacebookLogin: "تسجيل الدخول عبر فيسبوك",
  Or: "او",
  UserOrEmail: "اسم المستخدم او البريد الالكتروني",
  DontHaveAccount: "هل لديك حساب ؟ ",
  accountDetails: "تفاصيل الحساب",
  username: "اسم المستخدم",
  email: "البريد الالكتروني",
  generatePass: "استخدم مولد كلمة السر",
  password: "كلمة السر ",
  signup: "تسجيل جديد",
  alreadySignUp: "هل قمت بالتسجيل من قبل ؟ قم بتسجيل دخولك الآن!",
  profileDetail: "تفصايل الصفحة الشخصية",
  firstName: "الاسم الاول",
  lastName: "الاسم الاخير",
  chatList: "قائمة الدردشة",

  // Horizontal
  featureProducts: "المنتجات المميزة",
  bagsCollections: "مجموعات سماعات الرأس",
  onSaleProducts: "على بيع المنتجات",
  topRatedProducts: "أعلى تصنيف المنتجات",
  womanBestSeller: "SmartWatches Best Seller",
  accessCollections: "مجموعات الملحقات",
  promotionProducts: "الترقيات",
  citchenThings: "أشياء المطبخ",
  illumination: "الإضاءة أفضل بائع",

  // Modal
  Select: "اختر",
  Cancel: "إلغاء",

  // review
  vendorTitle: "المتجر",
  comment: "اكتب مراجعة",
  yourcomment: "تعليقك",
  placeComment:
    "أخبر شيئًا عن تجربتك أو اترك نصيحة للآخرين",
  writeReview: "أكتب مراجعة",
  thanksForReview:
    "شكرًا للمراجعة ، سيتم التحقق من المحتوى الخاص بك بواسطة المشرف وسيتم نشره لاحقًا",
  errInputComment: "يرجى إدخال المحتوى الخاص بك لتقديم",
  errRatingComment: "يرجى التقييم للتقديم",
  send: "إرسال",
  termCondition: "الشروط و الأحكام",
  Subtotal: "حاصل الجمع",
  Discount: "خصم",
  Shipping: "التوصيل",
  Recents: "العناصر الجديدة",
  Filters: "مرشحات",
  Princing: "الاسعار",
  Filter: "مرشح",
  ClearFilter: "مسح المرشحات",
  ProductCatalog: "بيان المنتج",
  ProductTags: "علامات المنتج",

  //extra
  vendorTitle: "المتجر",
  comment: "أكتب مراجعة",
  yourcomment: "تعليقك",
  placeComment:
    "أخبر شيئًا عن تجربتك أو اترك نصيحة للآخرين",
  writeReview: "اكتب مراجعة",
  thanksForReview:
    "شكرًا للمراجعة ، سيتم التحقق من المحتوى الخاص بك بواسطة المشرف وسيتم نشره لاحقًا",
  errInputComment: "من فضلك ادخل المحتوى",
  errRatingComment: "يرجى تقييم لتقديم",
  send: "إرسال مراجعة",
  allowAccessCameraroll: "تحتاج إلى تشغيل للسماح بالوصول إلى الكاميرا ",

  // search screen
  recents: "حديثي",
  filter: "مرشحات: ",

  //language
  currentLanguage: "اللغة الحالية",
  SwitchRtlConfirm:
    "تتطلب تبديل اللغة إعادة تحميل التطبيق ، هل تريد المتابعة؟",
  Confirm: "تؤكد",
  changeLanguage: "تغيير اللغة",
  OK: "حسنا",
  CANCEL: "إلغاء",
  next: "التالي",

  //chat
  typeAmessage: "اكتب الرسالة هنا",
  // vendor info
  storeName: "اسم المتجر",
  storeEmail: "البريد الالكتروني",
  storePhone: "التليفون",
  storeAddress: "العنوان",
  storeLocation: "المكان",
  storeRating: "التقييم",
  review: "المراجعات",
  orderDetailsTitle: "تفاصيل الطلب",

  //new listing
  publish: "نشر",
  allowAccessCameraroll: "تحتاج إلى تشغيل للسماح بالوصول إلى الكاميرا ",
  postHeading: "عنوان المنشور",
  selectCategory: "اختر التصنيف",
  errorMsgConnectServer: "لا يمكن الاتصال بالسيرفر.",
  submit: "تأكيد",
  composeTheContent: "اكتب المحتوى...",
  readyToSubmit: "جاهز للتسليم ؟",
  notYet: "ليس بعد",
  successfull: "بنجاح",
  close: "إغلاق",
  msgConfirmClearHistory: "هل أنت متأكد أنك تريد مسح التاريخ؟",
  confirmation: "تأكيد",
  listingType: "نوع القائمة",
  myListings: "القوائم الخاصة بي",
  selectOnLocation: "اختر موقعا",
  location: "قائمة الموقع",
  informationField: "حقول المعلومات",
  listingTitle: "اكتب عنوان القائمة",
  listingContent: "وصف المنتج",
  productPrice: 'السعر',
  points: "نقاط",
  wallet_balance: "رصيد المحفظة",
  checkout_failed_msg: "لا يمكن الشراء بهذا الحساب من فضلك اعد تسجيل الدخول بحساب مشتري",
  couponUsedBefore: "تم استخدام هذا الكويون من قبل"
};

// i18n.fallbacks = true;
// i18n.translations = { en, ar };
// i18n.locale = Localization.locale;
const langs = {en, ar};
const Languages = new Localization.LocaleStore(langs);
export default Languages;

