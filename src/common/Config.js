/** @format */

import Images from "./Images";
import Constants from "./Constants";
import Icons from "./Icons";

export default {
  /**
   Step 1: Moved AppConfig.json
   */

  /**
   Step 2: Setting Product Images
   - ProductSize: The default config for ProductSize is disable due to some problem config for most of users.
   If you have success config it from the Wordpress site, please enable to speed up the app performance - ref: https://docs-mv.mstore.io/customize-your-app
   - HorizonLayout: Change the HomePage horizontal layout - https://docs-mv.mstore.io/customize-your-app#change-the-homepage-horizontal-layout
   */
  ProductSize: {
    enable: true,

    Small: 400,
    Medium: 700,
    Large: 1400,
  },

  // config the Circlr Category list
  HomeCategories: {
    layout: Constants.Layout.circle,
    theme: 1, // support themes: 1,2
    items: [
      // rehub config category
      {
        category: 49, // Anonse
        image: require("@images/icons-electric/Tablet.png"),
        colors: ["#30cfd0", "#330867"],
      },
      {
        category: 46, // Strange things
        image: require("@images/icons-electric/Watch-1.png"),
        colors: ["#2af598", "#009efd"],
      },
      {
        category: 47, // TV & Video
        image: require("@images/icons-electric/Gameboy.png"),
        colors: ["#6a11cb", "#2575fc"],
      },
      {
        category: 48, // Accessory
        image: require("@images/icons-electric/iMac.png"),
        colors: ["#2F80ED", "#56CCF2"],
      },
      {
        category: 37, // Gifts
        image: require("@images/icons-electric/Airpods.png"),
        colors: ["#00d2ff", "#3a47d5"],
      },
      {
        category: 41, // Media
        image: require("@images/icons-electric/Phone.png"),
        colors: ["#f05053", "#e1eec3"],
      },
      {
        category: 56, // Fitness
        image: require("@images/icons-electric/Laptop-Macbook.png"),
        colors: ["#22c1c3", "#fdbb2d"],
      },
      {
        category: 59, // Outdoors
        image: require("@images/icons-electric/Camera.png"),
        colors: ["#7F00FF", "#E100FF"],
      },

      // handly config categories
      // {
      //   category: 65, // Birthday Gifts
      //   image: require("@images/icons-electric/Tablet.png"),
      //   colors: ["#30cfd0", "#330867"],
      // },
      // {
      //   category: 66, // Decor Art
      //   image: require("@images/icons-electric/Watch-1.png"),
      //   colors: ["#2af598", "#009efd"],
      // },
      // {
      //   category: 68, // Personal
      //   image: require("@images/icons-electric/Gameboy.png"),
      //   colors: ["#6a11cb", "#2575fc"],
      // },
      // {
      //   category: 71, // Toys
      //   image: require("@images/icons-electric/iMac.png"),
      //   colors: ["#2F80ED", "#56CCF2"],
      // },
      // {
      //   category: 73, // Vintage
      //   image: require("@images/icons-electric/Airpods.png"),
      //   colors: ["#00d2ff", "#3a47d5"],
      // },
      // {
      //   category: 70, // Special Goods
      //   image: require("@images/icons-electric/Phone.png"),
      //   colors: ["#f05053", "#e1eec3"],
      // },
      // {
      //   category: 76, // Furniture
      //   image: require("@images/icons-electric/Laptop-Macbook.png"),
      //   colors: ["#22c1c3", "#fdbb2d"],
      // },
      // {
      //   category: 72, // Variables
      //   image: require("@images/icons-electric/Camera.png"),
      //   colors: ["#7F00FF", "#E100FF"],
      // },

      // Electron config category
      // {
      //   category: 321,
      //   image: require("@images/icons-electric/Tablet.png"),
      //   colors: ["#30cfd0", "#330867"],
      // },
      // {
      //   category: 317,
      //   image: require("@images/icons-electric/Watch-1.png"),
      //   colors: ["#2af598", "#009efd"],
      // },

      // {
      //   category: 361,
      //   image: require("@images/icons-electric/Gameboy.png"),
      //   colors: ["#6a11cb", "#2575fc"],
      // },
      // {
      //   category: 294,
      //   image: require("@images/icons-electric/iMac.png"),
      //   colors: ["#2F80ED", "#56CCF2"],
      // },
      // {
      //   category: 289,
      //   image: require("@images/icons-electric/Headphones.png"),
      //   colors: ["#00d2ff", "#3a47d5"],
      // },
      // {
      //   category: 314,
      //   image: require("@images/icons-electric/Phone.png"),
      //   colors: ["#f05053", "#e1eec3"],
      // },
      // {
      //   category: 366,
      //   image: require("@images/icons-electric/Laptop-Macbook.png"),
      //   colors: ["#22c1c3", "#fdbb2d"],
      // },
      // {
      //   category: 345,
      //   image: require("@images/icons-electric/Camera.png"),
      //   colors: ["#7F00FF", "#E100FF"],
      // },
    ],
  },

  /**
   step 3: Config image for the Payment Gateway
   Notes:
   - Only the image list here will be shown on the app but it should match with the key id from the WooCommerce Website config
   - It's flexible way to control list of your payment as well
   Ex. if you would like to show only cod then just put one cod image in the list
   * */
  Payments: {
    // bacs: require("@images/payment_logo/PayPal.png"),
    cod: require("@images/payment_logo/cash_on_delivery.png"),
    paypal: require("@images/payment_logo/PayPal.png"),
    stripe: require("@images/payment_logo/stripe.png"),
  },

  /**
   Step 4: Advance config:
   - showShipping: option to show the list of shipping method
   - showStatusBar: option to show the status bar, it always show iPhoneX
   - LogoImage: The header logo
   - LogoWithText: The Logo use for sign up form
   - LogoLoading: The loading icon logo
   - appFacebookId: The app facebook ID, use for Facebook login
   - CustomPages: Update the custom page which can be shown from the left side bar (Components/Drawer/index.js)
   - WebPages: This could be the id of your blog post or the full URL which point to any Webpage (responsive mobile is required on the web page)
   - CategoryListView: default layout for category (true/false)
   - intro: The on boarding intro slider for your app
   - menu: config for left menu side items (isMultiChild: This is new feature from 3.4.5 that show the sub products categories)
   * */
  shipping: {
    visible: true,
    time: {
      free_shipping: "4 - 7 Days",
      flat_rate: "1 - 4 Days",
      local_pickup: "1 - 4 Days",
    },
  },
  showStatusBar: false,
  LogoImage: require("@images/new_logo.png"),
  LogoWithText: require("@images/logo_with_text.png"),
  logoWhite: require("@images/logo-white.png"),
  LogoLoading: require("@images/logo.png"),

  showAdmobAds: false,
  AdMob: {
    deviceID: "pub-2101182411274198",
    unitID: "ca-app-pub-2101182411274198/4100506392",
    unitInterstitial: "ca-app-pub-2101182411274198/8930161243",
    isShowInterstital: true,
  },
  appFacebookId: "2130834456984861",
  CustomPages: {
    contact_id: 2508,
    about_us: 2494,
  },
  WebPages: {
    marketing: "http://inspireui.com",
  },

  CategoryListView: true,

  intro: [
    {
      key: "page1",
      title: "Welcome to elbs",
      text: "Get the hottest fashion by trend and season right on your pocket.",
      icon: "ios-basket-outline",
      colors: ["#0FF0B3", "#036ED9"],
    },
    {
      key: "page2",
      title: "Secure Payment",
      text: "All your payment infomation is top safety and protected",
      icon: "ios-card-outline",
      colors: ["#13f1fc", "#0470dc"],
    },
    {
      key: "page3",
      title: "High Performance",
      text: "Saving your value time and buy product with ease",
      icon: "ios-finger-print-outline",
      colors: ["#b1ea4d", "#459522"],
    },
  ],

  /**
   * Config Left Menu Side
   * @param goToScreen 3 Params (routeName, params, isReset = false)
   * BUG: Language can not change when set default value in Config.js ==> pass string to change Languages
   */
  menu: {
    // has child categories
    isMultiChild: true,
    // Unlogged
    listMenuUnlogged: [
      {
        text: "Login",
        routeName: "LoginScreen",
        params: {
          isLogout: false,
        },
        icon: Icons.MaterialCommunityIcons.SignIn,
      },
    ],
    // user logged in
    listMenuLogged: [
      {
        text: "Logout",
        routeName: "LoginScreen",
        params: {
          isLogout: true,
        },
        icon: Icons.MaterialCommunityIcons.SignOut,
      },
    ],
    // Default List
    listMenu: [
      {
        text: "Shop",
        routeName: "Default",
        icon: Icons.MaterialCommunityIcons.Home,
      },
      {
        text: "News",
        routeName: "NewsScreen",
        icon: Icons.MaterialCommunityIcons.News,
      },
      {
        text: "Contact Us",
        routeName: "CustomPage",
        params: {
          id: 2508,
          title: "Contact Us",
        },
        icon: Icons.MaterialCommunityIcons.Pin,
      },
      {
        text: "About Us",
        routeName: "CustomPage",
        params: {
          id: 2494,
          title: "About Us",
        },
        icon: Icons.MaterialCommunityIcons.Email,
      },
      {
        text: "Setting",
        routeName: "SettingScreen",
        icon: Icons.MaterialCommunityIcons.Setting,
      },
    ],
  },
  // define menu for profile tab
  ProfileSettings: [
    // {
    //   label: "Cart",
    //   routeName: "Cart",
    // },
    {
      label: "MyOrder",
      routeName: "MyOrders",
    },
    {
      label: "chatList",
      routeName: "ChatList",
    },
    // {
    //   label: "WishList",
    //   routeName: "WishListScreen",
    // },
    
    {
      label: "Currency",
      isActionSheet: true,
    },

    // {   label: "Languages",   routeName: "SettingScreen", },

    {
      label: "PushNotification",
    },
    {
      label: "Settings",
      routeName: "SettingScreen",
    },
    // {
    //   label: "contactus",
    //   routeName: "CustomPage",
    //   params: {
    //     id: 10941,
    //     title: "contactus",
    //   },
    // },
    // {
    //   label: "Privacy",
    //   routeName: "CustomPage",
    //   params: {
    //     url: "https://inspireui.com/privacy",
    //   },
    // },
    // {
    //   label: "termCondition",
    //   routeName: "CustomPage",
    //   params: {
    //     url: "https://inspireui.com/term-of-service",
    //   },
    // },
    // {
    //   label: "About",
    //   routeName: "CustomPage",
    //   params: {
    //     url: "http://inspireui.com",
    //   },
    // },
  ],

  // Layout modal select
  LayoutsSwicher: [
    {
      layout: Constants.Layout.card,
      image: Images.icons.iconCard,
      text: "cardView",
    },
    {
      layout: Constants.Layout.simple,
      image: Images.icons.iconRight,
      text: "simpleView",
    },
    {
      layout: Constants.Layout.twoColumn,
      image: Images.icons.iconColumn,
      text: "twoColumnView",
    },
    {
      layout: Constants.Layout.threeColumn,
      image: Images.icons.iconThree,
      text: "threeColumnView",
    },
    {
      layout: Constants.Layout.horizon,
      image: Images.icons.iconHorizal,
      text: "horizontal",
    },
    {
      layout: Constants.Layout.advance,
      image: Images.icons.iconAdvance,
      text: "advanceView",
    },
  ],
  // Config chatting for Firebase
  Chat: {
    defaultBg: Images.defaultBgChat,
    opacityBg: 0.3,
  },
  Firebase: {
    apiKey: "AIzaSyBaO7bsVIGECrt_yDnH4ogCO6q8X-bNEQ8",
    authDomain: "elbes-8a6ab.firebaseapp.com",
    databaseURL: "https://elbes-8a6ab.firebaseio.com",
    projectId: "elbes-8a6ab",
    storageBucket: "elbes-8a6ab.appspot.com",
    messagingSenderId: "537903520556",
    appId: "1:537903520556:web:6798db9dd44b04ef"
  },
  GoogleAnalytic: {
    enable: false,
    TrackingId: "UA-126748701-1",
  },

  // Default theme loading, this could able to change from the user profile
  // (reserve feature)
  Theme: {
    isDark: false,
  },

  DefaultCurrency: {
    symbol: "EGP",
    name: "EG Pound",
    code: "EGP",
    name_plural: "EG Pound",
    decimal: ".",
    thousand: ",",
    precision: 2,
    format: "%s%v", // %s is the symbol and %v is the value
  },
  DefaultCountry: {
    code: "en",
    RTL: false,
    language: "English",
    countryCode: "EG",
    hideCountryList: true, // when this option is try we will hide the country list from the checkout page, default select by the above 'countryCode'
  },
};
