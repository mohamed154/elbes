/**
 * Created by InspireUI on 17/02/2017.
 *
 * @format
 */

const Images = {
  defaultAvatar: require('@images/default_avatar.jpg'),
  categoryPlaceholder: require('@images/category_placehodler.png'),
  vendorPlaceholder: require('@images/vendor_placeholder.png'),
  splashScreen: require('@images/splash_screen.png'),
  defaultUserChat:
    'https://s3.amazonaws.com/uifaces/faces/twitter/adhamdannaway/128.jpg',
  defaultBgChat: require('@images/background.png'),
  imageHolder: require('@images/placeholderImage.png'),

  defaultPayment: require('@images/placeholderImage.png'),
  Stripe: require('@images/stripe.png'),
  PayPal: require('@images/PayPal.png'),
  CashOnDelivery: require('@images/cash_on_delivery.png'),
  PlaceHolder: require('@images/placeholderImage.png'),
  PlaceHolderURL: 'http://mstore.io/wp-content/uploads/2017/07/placeholder.png',

  IconWishList: require('@images/icons/icon-love.png'),
  IconCart: require('@images/icons/shopping-cart.png'),
  IconShare: require('@images/icons/icon-share.png'),
  IconPin: require('@images/icons/icon-pin.png'),
  IconMoney: require('@images/icons/icon-money.png'),
  IconFlag: require('@images/icons/icon-flag.png'),

  IconHome: require('@images/icons/icon2-home.png'),
  IconCategory: require('@images/icons/icon2-category.png'),
  IconShops: require('@images/icons/shop.png'),
  IconUser: require('@images/icons/icon2-setting.png'),
  IconHeart: require('@images/icons/icon-heart.png'),
  IconOrder: require('@images/icons/icon-cart2.png'),
  IconNews: require('@images/icons/icon-news.png'),
  IconSearch: require('@images/icons/icon2-search.png'),

  // Filter Icon in Category
  IconFilter: require('@images/icons/icon-filter.png'),
  IconList: require('@images/icons/icon-list.png'),
  IconGrid: require('@images/icons/icon-grid.png'),
  IconCard: require('@images/icons/icon-card.png'),
  sortIcon: require('@images/icons/sort-down.png'),

  IconSwitch: require('@images/icons/icon-switch.png'),
  icons: {
    home: require('@images/icons/line-menu.png'),

    back: require('@images/icons/icon-back.png'),
    backs: require('@images/icons/icon-backs.png'),
    backsWhite: require('@images/icons/icon-backs-white.png'),
    next: require('@images/icons/icon-next.png'),

    iconCard: require('@images/icon-card.png'),
    iconColumn: require('@images/icon-column.png'),
    iconLeft: require('@images/icon-listLeft.png'),
    iconRight: require('@images/icon-listRight.png'),
    iconThree: require('@images/icon-three.png'),
    iconAdvance: require('@images/icon-advance.png'),
    iconHorizal: require('@images/icon-horizal.png'),
    iconSearch: require('@images/icons/tab-search.png'),
    iconMessage: require('@images/icons/icon2-chat.png'),
    LongBack: require('@images/backlong.png'),

    iconCamera: require('@images/icons/icon-camera.png'),
    iconMore: require('@images/icons/icon-more.png'),
    iconPlus: require('@images/icons/icon-plus.png'),

    //chat
    videocall: require('@images/icons/videocall.png'),
    call: require('@images/icons/call.png'),
    emptyChat: require('@images/icons/empty-chat.png'),
  },
  Banner: {
    Feature: require('@images/banner/banner1.png'),
    Banner2: require('@images/banner/banner2.png'),
    Banner3: require('@images/banner/banner3.png'),
    Banner1: require('@images/banner/banner1.png'),
  },
  IconUkFlag: require('@images/ic_uk_country_flag.jpg'),
  IconOmanFlag: require('@images/ic_oman_country_flag.jpg'),
  IconAdd: require("@images/ic_add.png"),
  IconCheck: require("@images/ic_check.png"),

  EditIcon : require('@images/ic_edit.png'),
  AddIcon : require('@images/ic_add.png'),
  DownArrowIcon : require('@images/ic_down_arrow.png'),
  SuccessIcon : require('@images/ic_success.png'),
  SmallNextIcon : require('@images/ic_small_next.png'),
  profileHeaderBack: require('@images/profile-header-background.png')
};

export default Images
