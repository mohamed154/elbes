/** @format */

// @flow
/**
 * Created by InspireUI on 19/02/2017.
 */
import React from "react";
import {View, ScrollView, Image, Text, TouchableOpacity, Dimensions, Picker} from "react-native";
import {connect} from "react-redux";
import {Config, Tools, Images, Styles, withTheme, Constants, Color} from "@common";
import {Button,LogoSpinner} from "@components";
import {BlockTimer, getProductImage} from "@app/Omni";
import Icon from "@expo/vector-icons/FontAwesome";
import {isObject} from "lodash";
import styles from "./styles";
import {Modal} from "react-native-paper";

class VendorTab extends React.PureComponent {

  constructor(props) {
    super(props);
    this.state = {
      layout: 'list',
      list: [],
      cityFilter: null,
      regionFilter: null,
      rateFilter: null,
      modalVisible: false
    }
  }

  componentDidMount() {
    const {fetchAllVendors, list} = this.props;
    fetchAllVendors();
    this.setState({list: list});
  }

  componentWillReceiveProps(nextProps, nextContext) {
    const {list} = this.props;
    this.setState({list: list});
  }

  onRowClickVendor = (item) => {

    const {onViewVendor} = this.props;
    BlockTimer.execute(() => {
      onViewVendor(item);
    }, 1);
  };

  setModalVisible(visible) {
    this.setState({modalVisible: visible});
  }

  render() {
    const {
      theme: {
        colors: {background},
      },
      list,
      isFetching,
      onChat
    } = this.props;

    if (isFetching) {
      return <LogoSpinner fullStretch/>;
    }

    let cities = [];
    for (let item of list) {
      if (item.address && item.address.city) {
        cities.push(item.address.city);
      }
    }
    cities = [...new Set(cities)];

    let regions = [];
    for (let item of list) {
      if (item.address && item.address.state && item.address.state !== '') {
        regions.push(item.address.state);
      }
    }
    regions = [...new Set(regions)];

    let rates = [];
    for (let item of list) {
      if (item.address && item.rating.rating) {
        rates.push(item.rating.rating);
      }
    }
    rates = [...new Set(rates)];

    for (let item of list) {
      item.name = item.store_name;
      item.shop_name = item.store_name;
    }

    const onModalFilter = () => {
      let filteredList = list;
      if (this.state.cityFilter) {
        filteredList = filteredList.filter(item => item.address.city === this.state.cityFilter);
      }
      if (this.state.regionFilter) {
        filteredList = filteredList.filter(item => item.address.state === this.state.regionFilter);
      }
      if (this.state.rateFilter) {
        filteredList = filteredList.filter(item => item.rating.rating === this.state.rateFilter);
      }
      this.setState({list: filteredList, modalVisible: false});
    };

    return (
      <View style={{flex: 1, paddingTop: 0, backgroundColor: background}}>
        <View style={{
          flexDirection: "row",
          padding: 16,
          elevation: 4,
          shadowOffset: {width: 5, height: 5},
          shadowColor: "grey",
          shadowOpacity: 0.5,
          shadowRadius: 10,
          justifyContent: "space-between"
        }}>
          <View style={{flexDirection: 'row'}}>
            <TouchableOpacity style={{marginRight: 18}} onPress={() => {
              this.setState({layout: 'list'})
            }}>
              <Image source={Images.IconList}
                     style={[{width: 18, height: 18}, this.state.layout !== 'list' && {opacity: 0.2}]}/>
            </TouchableOpacity>
            <TouchableOpacity style={{marginRight: 20}} onPress={() => {
              this.setState({layout: 'grid'})
            }}>
              <Image source={Images.IconGrid}
                     style={[{width: 18, height: 18}, this.state.layout !== 'grid' && {opacity: 0.2}]}/>
            </TouchableOpacity>
          </View>
          <View>
            <TouchableOpacity style={{alignSelf: "flex-end"}} onPress={() => {
              this.setState({modalVisible: true})
            }}>
              <Image source={Images.IconFilter}
                     style={{width: 18, height: 18}}/>
            </TouchableOpacity>
          </View>
        </View>
        <ScrollView style={[styles.fill, {backgroundColor: background, paddingTop: 10}]}>
          <View style={this.state.layout == 'grid' && {
            flexDirection: 'row',
            flexWrap: 'wrap',
            width: Dimensions.get('window').width
          }}>
            {typeof this.state.list !== "undefined" &&
            this.state.list.length > 0 &&
            this.state.list.map((item, index) => {
              const image =
                typeof item.banner !== "undefined" && item.banner != ""
                  ? {uri: getProductImage(item.banner, Styles.width)}
                  : typeof item.pv_logo_image !== "undefined" &&
                  item.pv_logo_image != ""
                  ? {uri: getProductImage(item.pv_logo_image, Styles.width)}
                  : Images.categoryPlaceholder;

              const vendorAddress =
                typeof item.address !== "undefined" &&
                (item.address.length > 0 || isObject(item.address)) &&
                item.address.street_1 != ""
                  ? `${item.address.street_1}, ${item.address.city}, ${
                    item.address.state
                    }`
                  : typeof item.billing_address_1 !== "undefined" &&
                  item.billing_address_1 != ""
                  ? item.billing_address_1
                  : "";

              const emailStore =
                typeof item.email !== "undefined" && item.email != ""
                  ? item.email
                  : item.billing_email != ""
                  ? item.billing_email
                  : "";

              // console.log([item, emailStore, item.billing_email]);
              const rearrange = index % 2 == 0 ? "flex-end" : "flex-start";
              const textStyle =
                index % 2 == 0
                  ? {marginRight: 15, textAlign: "right"}
                  : {marginLeft: 15};

              return (
                <TouchableOpacity
                  key={index.toString()}
                  style={[
                    Config.Theme.isDark ? styles.overlayDark : styles.overlay,
                    this.state.layout === 'grid' && {width: Dimensions.get('window').width / 3}
                  ]}
                  onPress={() => this.onRowClickVendor(item)}>
                  <View style={[{
                    flex: 1,
                    borderBottomWidth: 1,
                    borderBottomColor: "#d4dce1",
                  }, {backgroundColor: background}, this.state.layout === 'grid' && {borderBottomWidth: 0}]}>
                    <View style={[{
                      flexDirection: "row",
                      margin: 10,
                    }, this.state.layout === 'grid' && {flexDirection: 'column'}]}>
                      <TouchableOpacity onPress={() => this.onRowClickVendor(item)}>
                        <Image
                          source={image}
                          style={{
                            width: 100,
                            height: 100,
                            borderRadius: 50,
                          }}
                        />
                      </TouchableOpacity>
                      <View
                        style={[
                          {
                            marginLeft: 20,
                            marginRight: 20,
                            flex: 1,
                          },
                          {width: Dimensions.get("window").width - 180},
                          this.state.layout === 'grid' && {
                            marginLeft: 0,
                            marginRight: 0,
                            width: Dimensions.get("window").width / 3 - 40
                          }
                        ]}>
                        <TouchableOpacity onPress={() => this.onRowClickVendor(item)}>
                          <Text style={[{
                            fontSize: 20,
                            fontFamily: Constants.fontFamily,
                            color: Color.Text,
                          }, {color: Color.text},
                            this.state.layout === 'grid' && {textAlign: 'center'}]}>{item.store_name}</Text>
                          {this.state.layout !== 'grid' && vendorAddress != "" && (
                            <View
                              style={[
                                {
                                  flexDirection: "row",
                                  alignItems: "center",
                                }
                              ]}>
                              <Icon
                                name="location-arrow"
                                style={[{margin: 5}]}
                                color={Color.text}
                                size={12}
                              />
                              <Text style={styles.numberOfProductsText}>
                                {vendorAddress}
                              </Text>
                            </View>
                          )}
                          {/*{this.state.layout !== 'grid' && emailStore != "" && (*/}
                            {/*<View*/}
                              {/*style={[*/}
                                {/*{*/}
                                  {/*flexDirection: "row",*/}
                                  {/*alignItems: "center",*/}
                                {/*}*/}
                              {/*]}>*/}
                              {/*<Icon*/}
                                {/*name="envelope-open"*/}
                                {/*style={[{margin: 5}]}*/}
                                {/*color={Color.text}*/}
                                {/*size={12}*/}
                              {/*/>*/}
                              {/*<Text style={styles.numberOfProductsText}>*/}
                                {/*{emailStore}*/}
                              {/*</Text>*/}
                            {/*</View>*/}
                          {/*)}*/}
                        </TouchableOpacity>
                      </View>
                      <View styl0e={{justifyContent: "center", height: '100%'}}>
                        <Button
                          type="image"
                          source={Images.icons.iconMessage}
                          imageStyle={styles.imageButtonChat}
                          buttonStyle={[styles.buttonChat]}
                          onPress={() => onChat(item)}
                        />
                      </View>
                    </View>
                  </View>
                </TouchableOpacity>
              );
            })}
          </View>
        </ScrollView>
        <Modal visible={this.state.modalVisible}
               onRequestClose={() => {
                 this.setModalVisible(false)
               }}>
          <View style={styles.modalContent}>
            <Text style={{marginBottom: 10, fontSize: 20, fontWeight: 'bold'}}>Filter</Text>
            <Text style={{marginTop: 10}}>Select city</Text>
            <View style={styles.modalPicker}>
              <Picker
                selectedValue={this.state.cityFilter}
                style={{height: 50}}
                onValueChange={(itemValue, itemIndex) =>
                  this.setState({cityFilter: itemValue})
                }
              >
                <Picker.Item label={"No filter"} value={null}/>
                {typeof cities !== "undefined" && cities.length > 0 &&
                cities.map((item, index) => <Picker.Item label={item} value={item}/>)
                }
              </Picker>
            </View>
            {regions.length > 0 && <Text style={{marginTop: 10}}>Select region</Text> }
            {regions.length > 0 && <View style={styles.modalPicker}>
              <Picker
                selectedValue={this.state.regionFilter}
                style={{height: 50}}
                onValueChange={(itemValue, itemIndex) =>
                  this.setState({regionFilter: itemValue})
                }
              >
                <Picker.Item label={"No filter"} value={null}/>
                {regions.length > 0 &&
                regions.map((item, index) => <Picker.Item label={item} value={item}/>)
                }
              </Picker>
            </View>}
            <Text style={{marginTop: 10}}>Select rate</Text>
            <View style={styles.modalPicker}>
              <Picker
                selectedValue={this.state.rateFilter}
                style={{height: 50}}
                onValueChange={(itemValue, itemIndex) =>
                  this.setState({rateFilter: itemValue})
                }
              >
                <Picker.Item label={"No filter"} value={null}/>
                {typeof rates !== "undefined" && rates.length > 0 &&
                rates.map((item, index) => <Picker.Item label={item} value={item}/>)
                }
              </Picker>
            </View>
            <View style={styles.modalButtonWrapper}>
              <TouchableOpacity onPress={() => this.setModalVisible(false)} style={styles.closeModalBtn}>
                <Text>close</Text>
              </TouchableOpacity>
              <TouchableOpacity style={styles.filterModalBtn} onPress={onModalFilter}>
                <Text>Filter</Text>
              </TouchableOpacity>
            </View>
          </View>
        </Modal>
      </View>
    );
  }
}

const mapStateToProps = ({vendor}) => {
  return {
    list: vendor.list,
    isFetching: vendor.isFetching,
  };
};

function mergeProps(stateProps, dispatchProps, ownProps) {
  const {dispatch} = dispatchProps;
  const {actions} = require("@redux/VendorRedux");

  return {
    ...ownProps,
    ...stateProps,
    fetchAllVendors: () => {
      actions.fetchAllVendors(dispatch);
    },
  };
}

export default withTheme(
  connect(
    mapStateToProps,
    undefined,
    mergeProps
  )(VendorTab)
);
