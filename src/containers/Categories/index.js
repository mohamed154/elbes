/** @format */

// @flow
/**
 * Created by InspireUI on 19/02/2017.
 */
import React from "react";
import {View} from "react-native";
import {withTheme, Languages} from "@common";
import ScrollableTabView, {
  ScrollableTabBar,
} from "react-native-scrollable-tab-view";
import styles from "./styles";
import CategoriesTab from "./CategoriesTab";
import VendorTab from "./VendorTab";

@withTheme
export default class CategoriesScreen extends React.PureComponent {
  render() {
    const {
      theme: {
        colors: {background},
      },
      onViewVendor,
      onViewCategory,
      onViewProductScreen,
      onViewCartScreen,
      onViewSubCategories,
      vendors,
      onChat
    } = this.props;

    return (
      <View style={{flex: 1, backgroundColor: background}}>
        {(!vendors) && <CategoriesTab
          onViewCategory={onViewCategory}
          onViewSubCategories={onViewSubCategories}
          onViewProductScreen={onViewProductScreen}
          tabLabel={Languages.Categories}
          onViewCartScreen={onViewCartScreen}
        />}
        {vendors &&
        <VendorTab onViewVendor={onViewVendor}
                   onViewProductScreen={onViewProductScreen}
                   tabLabel={Languages.Vendors}
                   onViewCartScreen={onViewCartScreen}
                   onChat={onChat}/>}
        {/*</ScrollableTabView>*/}
      </View>
    );
  }
}
