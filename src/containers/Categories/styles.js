/** @format */

import { Constants, Styles, Color } from "@common";
import { Platform, Dimensions } from "react-native";

export default {
  imageButtonChat: {
    width: 50,
    height: 50
  },
  fill: {
    flex: 1,
  },
  container: {
    flexGrow: 1,
    backgroundColor: "#f4f4f4",
  },
  header: {
    position: "absolute",
    top: 0,
    left: 0,
    right: 0,
    backgroundColor: "rgba(255, 255, 255, 0.5)",
    overflow: "hidden",
    height: Constants.Window.headerHeight,
  },
  backgroundImage: {
    position: "absolute",
    top: 10,
    left: 0,
    right: 0,
    width: null,
    height:
      Platform.OS === "ios"
        ? Constants.Window.headerHeight
        : Constants.Window.headerHeight + 100,
  },
  scrollViewContent: {
    position: "relative",
    marginBottom: 100,
  },

  containerStyle: {
    shadowColor: "#000",
    shadowOpacity: 0.4,
    shadowRadius: 8,
    shadowOffset: { width: 0, height: 12 },
    elevation: 10,
    margin: 15,
  },

  borderView: {
    flex: 1,
    width: Styles.width - 30,
    height: Styles.height / 5 - 10,
    borderRadius: 6,
    overflow: 'hidden',
    justifyContent: "center",
  },

  image: {
    flex: 1,
    width: Styles.width - 30,
    height: Styles.height / 5 - 10,
    borderRadius: 5,
    resizeMode: "cover",
  },
  dim_layout: {
    width: Styles.width - 30,
    height: Styles.height / 5 - 10,
    backgroundColor: "rgba(0,0,0,0.4)",
    padding: 10,
    position: "absolute",
    bottom: 0,
    left: 0,
  },
  mainCategoryText: {
    color: "white",
    fontSize: 25,
    fontFamily: Constants.fontHeader,
  },
  numberOfProductsText: {
    color: Color.text,
    fontSize: 12,
    fontFamily: Constants.fontFamily,
  },
  overlay: {
    // alignItems: "center",
    justifyContent: "center",
    backgroundColor: "rgba(0,0,0,0.4)",
  },

  overlayDark: {
    justifyContent: "center",
    backgroundColor: "rgba(0,0,0,0.2)",
  },

  fab: {
    position: "absolute",
    overflow: "hidden",
    bottom: 15,
    right: 12,
    height: 40,
    width: 40,
    alignItems: "center",
    justifyContent: "center",
    borderRadius: 50,
    backgroundColor: "rgba(0, 0, 0, .85)",

    elevation: 5,
    shadowColor: "#000",
    shadowOpacity: 0.2,
    shadowRadius: 4,
    shadowOffset: { width: 0, height: 2 },
  },

  // tabBar
  containerTab: {
    alignItems: "center",
    justifyContent: "center",
    padding: 0,
    paddingTop: 10,
  },
  tab: {
    borderBottomWidth: 0,
    paddingVertical: 5,
    boxShadow: "none",
  },
  activeTab: {
    height: 1,
    backgroundColor: "#FFF",
    fontSize: 30,
    fontFamily: Constants.fontHeader
  },
  textTab: {
    fontSize: 20,
    fontFamily: Constants.fontHeader,
  },
  modalContent: {
    backgroundColor: '#fff',
    // height: 150,
    width: 350,
    borderRadius: 5,
    padding: 20,
    marginLeft: (Dimensions.get('window').width - 350) / 2
  },
  modalPicker: {
    borderWidth: 1,
    borderColor: '#000',
    marginTop: 10,
    borderRadius: 5
  },
  modalButtonWrapper: {
    alignItems: 'center',
    justifyContent: 'center',
    flexDirection: 'row',
    marginTop: 20
  },
  closeModalBtn: {
    flex: 1,
    backgroundColor: '#eee',
    alignItems: 'center',
    justifyContent: 'center',
    padding: 10,
    borderRadius: 5,
    marginHorizontal: 10
  },
  filterModalBtn: {
    flex: 1,
    backgroundColor: Color.primary,
    alignItems: 'center',
    justifyContent: 'center',
    padding: 10,
    borderRadius: 5,
    marginHorizontal: 10
  }
};
