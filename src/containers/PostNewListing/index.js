/** @format */

import React, { PureComponent } from "react";
import { View, KeyboardAvoidingView, Platform } from "react-native";
import {
  SelectImage,
  HeaderPage,
  PostHeading,
  SelectCategory,
} from "@components";
import { Languages } from "@common";
import styles from "./styles";
import { connect } from "react-redux";

class PostNewListing extends PureComponent {
  state = {
    requiredImg: false,
    requiredTitle: false,
    requiredPrice: false,
    // requiredCategory: false,
  };

  render() {
    const { onBack, categories } = this.props;
    const {
      requiredImg,
      requiredTitle,
      requiredPrice,
      requiredCategory,
    } = this.state;

    return (
      <View style={styles.container}>
        <KeyboardAvoidingView
          behavior="position"
          enabled={Platform.OS == "ios"}>
          <HeaderPage
            onBack={onBack}
            title={Languages.publish}
            hideRightButton={false}
            rightTitle={Languages.next}
            onRightPress={this.next}
          />
          <View style={styles.content}>
            <SelectImage
              required={requiredImg}
              style={styles.selectImage}
              onSelectImage={this.onSelectImage}
            />
            <PostHeading
              required={requiredTitle}
              style={styles.postHeading}
              onChangeText={(title) => (this.title = title)}
            />

            <PostHeading
              required={requiredPrice}
              style={styles.postHeading}
              title={Languages.productPrice}
              onChangeText={(price) => (this.price = price)}
            />
            <SelectCategory
              required={requiredCategory}
              categories={categories}
              onSelectCategory={this.onSelectCategory}
            />
          </View>
        </KeyboardAvoidingView>
      </View>
    );
  }

  onSelectImage = (imageUri) => {
    this.imageUri = imageUri;
  };

  onSelectCategory = (category) => {
    this.category = category;
  };

  next = () => {
    this.setState({
      requiredImg: this.imageUri == undefined,
      requiredTitle: this.title == undefined,
      requiredPrice: this.price == undefined,
    });

    if (
      this.imageUri != undefined &&
      this.title != undefined &&
      this.price != undefined
    ) {
      this.props.next({
        imageUri: this.imageUri,
        title: this.title,
        price: this.price,
      });
    }
  };
}
const mapStateToProps = ({ categories }) => {
  return {
    categories: categories.list,
  };
};
export default connect(mapStateToProps)(PostNewListing);
