/** @format */

import React, { PureComponent } from "react";
import { View, TextInput } from "react-native";
import { HeaderPage, ProcessModal, PostNewsDialog } from "@components";
import { Languages } from "@common";
import { connect } from "react-redux";
import styles from "./styles";

const CREATE_POST_SUCCESS = "CREATE_POST_SUCCESS";
const CREATE_POST_FAIL = "CREATE_POST_FAIL";

class PostNewContent extends PureComponent {
  state = {
    value: "",
    percent: 0,
    dialogType: "",
    message: "",
  };

  postNews = false;

  render() {
    const { onBack } = this.props;
    const { value, percent, dialogType, message } = this.state;

    return (
      <View style={styles.container}>
        <HeaderPage
          onBack={onBack}
          title={Languages.listingContent}
          hideRightButton={false}
          rightTitle={Languages.submit}
          onRightPress={this.showConfirm}
        />
        <View style={styles.content}>
          <TextInput
            style={styles.input}
            underlineColorAndroid="transparent"
            autoCapitalize="none"
            autoCorrect={false}
            placeholderTextColor="#707070"
            placeholder={Languages.composeTheContent}
            value={value}
            onChangeText={(value) => this.setState({ value })}
            multiline
          />
        </View>
        <ProcessModal ref="process" progress={percent} />
        <PostNewsDialog
          ref="dialog"
          type={dialogType}
          onSubmit={this.submit}
          message={message}
        />
      </View>
    );
  }

  showConfirm = () => {
    this.setState({ dialogType: "confirm" }, () => {
      this.refs.dialog.show();
    });
  };

  submit = () => {
    this.refs.dialog.hide(() => {
      if (this.state.value.length > 0 && !this.postNews) {
        const { getParam } = this.props.navigation;
        const { data } = this.props;
        const post = getParam("post");
        this.postNews = true;
        this.refs.process.show();
        this.props.createPost(
          data.user,
          post.title,
          post.price,
          this.state.value,
          post.imageUri,
          (percent) => {
            this.setState({ percent });
          },
          data.token
        );
      }
    });
  };

  componentWillReceiveProps(nextProps) {
    if (
      nextProps.products.type &&
      nextProps.products.type == CREATE_POST_FAIL &&
      this.postNews == true
    ) {
      this.postNews = false;
      this.refs.process.hide(() => {
        this.setState({
          dialogType: "error",
          message: nextProps.products.message,
        });
        this.refs.dialog.show();
      });
    }

    if (
      nextProps.products.type &&
      nextProps.products.type == CREATE_POST_SUCCESS &&
      this.postNews == true
    ) {
      this.postNews = false;
      this.refs.process.hide(() => {
        this.setState({ dialogType: "success" });
        this.refs.dialog.show();
        setTimeout(() => {
          this.refs.dialog.hide(() => {
            this.props.onClose();
          });
        }, 3000);
      });
    }
  }
}

const mapStateToProps = ({ user, products }) => {
  return {
    data: user,
    products,
  };
};

const mapDispatchToProps = (dispatch) => {
  const { actions } = require("@redux/ProductRedux");
  return {
    createPost: (author, title, price, content, imageUri, onProgress, token) =>
      actions.createPost(
        dispatch,
        author,
        title,
        price,
        content,
        imageUri,
        onProgress,
        token
      ),
  };
};
export default connect(
  mapStateToProps,
  mapDispatchToProps
)(PostNewContent);
