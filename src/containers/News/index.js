/** @format */

import React from "react";
import PropTypes from "prop-types";
import { PostList } from "@components";
import { Constants } from "@common";

const NewsScreen = ({ onViewNewsScreen }) => {
  return (
    <PostList
      type="news"
      layoutNews={Constants.Layout.threeColumn}
      onViewNewsScreen={onViewNewsScreen}
      newsPage
    />
  );
};

NewsScreen.propTypes = {
  onViewNewsScreen: PropTypes.func,
};

export default NewsScreen;
