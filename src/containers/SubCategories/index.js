/** @format */

// @flow
/**
 * Created by InspireUI on 19/02/2017.
 */
import React from "react";
import {View} from "react-native";
import {withTheme, Languages} from "@common";
import ScrollableTabView, {
  ScrollableTabBar,
} from "react-native-scrollable-tab-view";
import styles from "./styles";
import SubCategoriesTab from "./SubCategoriesTab";

@withTheme
export default class SubCategoriesScreen extends React.PureComponent {
  render() {
    const {
      theme: {
        colors: {background},
      },
      onViewVendor,
      onViewSubCategory,
      onViewProductScreen,
      onViewCartScreen,
      vendors
    } = this.props;

    return (
      <View style={{flex: 1, backgroundColor: background}}>
        <SubCategoriesTab
          onViewSubCategory={onViewSubCategory}
          onViewProductScreen={onViewProductScreen}
          tabLabel={Languages.Categories}
          onViewCartScreen={onViewCartScreen}
        />
      </View>
    );
  }
}
