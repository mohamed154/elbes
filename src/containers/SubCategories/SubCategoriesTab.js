/** @format */

// @flow
/**
 * Created by InspireUI on 19/02/2017.
 */
import React from "react";
import {View, ScrollView, Image, Text, TouchableOpacity} from "react-native";
import {connect} from "react-redux";
import {Color, withTheme, Config, Styles, Tools} from "@common";
import {toast, BlockTimer} from "@app/Omni";
import {Empty, LogoSpinner, TouchableScale, SplitCategories} from "@components";
import styles from "./styles";
import { getProductImage } from "@app/Omni";

class SubCategoriesTab extends React.PureComponent {

  componentDidMount() {

  }

  componentWillReceiveProps(nextProps, nextContext) {

  }

  onRowClickHandle = (category) => {
    const {setSelectedCategory, onViewSubCategory} = this.props;
    BlockTimer.execute(() => {
      setSelectedCategory({
        ...category,
        mainCategory: category
      });
      onViewSubCategory({mainCategory: category});
    }, 500);
  };

  render() {
    const {
      categories,
      theme: {
        colors: {
          background
        }
      },
      onViewProductScreen,
      onViewSubCategory

    } = this.props;

    if (categories.error) {
      return <Empty text={categories.error}/>;
    }

    if (categories.isFetching) {
      return <LogoSpinner fullStretch/>;
    }

    if (Config.CategoryListView != true) {
      return (<SplitCategories onViewPost={(product) => onViewProductScreen({product})}/>);
    }

    // const mainCategories = typeof categories.list !== "undefined" && categories
    //   .list
    //   .filter((category) => category.parent === 0);

    const {subCategories} = this.props;
    subCategories.splice(0, 1);

    if (subCategories.length == 0) {
      onViewSubCategory();
    }
    return (
      <View
        style={{
        flex: 1,
        paddingTop: 0,
        backgroundColor: background
      }}>
        <ScrollView
          style={[
          styles.fill, {
            backgroundColor: background
          }
        ]}>
          {subCategories.map((category, index) => {
            const textStyle = index % 2 == 0
              ? {
                marginRight: 10,
                textAlign: "right"
              }
              : {
                marginLeft: 10,
                textAlign: "left"
              };

            const categoryImage = category.image !== null
              ? {
                uri: getProductImage(category.image.src, Styles.width)
              }
              : null;

            if (category.name == "Uncategorized")
              return <View key={index.toString()}/>;

            return (
              <TouchableScale
                key={index.toString()}
                style={[
                styles.containerStyle, index % 2 == 0 && {
                  alignItems: "flex-end"
                },
                index % 2 != 0 && {
                  alignItems: "flex-start"
                }
              ]}
                onPress={() => this.onRowClickHandle(category)}>
                <View
                  style={[
                  styles.borderView, Config.Theme.isDark
                    ? styles.overlayDark
                    : styles.overlay,
                  categoryImage == null && {
                    backgroundColor: Color.defaultCates[Math.floor(Math.random() * 15 + 1)]
                  }
                ]}>
                  <Image style={styles.image} source={categoryImage}/>
                  <View style={[styles.dim_layout]}>
                    <Text
                      style={[
                      styles.mainCategoryText, {
                        ...textStyle
                      }
                    ]}>
                      {Tools.getDescription(category.name)}
                    </Text>
                    <Text
                      style={[
                      styles.numberOfProductsText, {
                        ...textStyle,
                          color: '#fff'
                      }
                    ]}>
                      {`${category.count} products`}
                    </Text>
                  </View>
                </View>
              </TouchableScale>
            );
          })}
        </ScrollView>
        {/* this.renderLayoutButton() */}
      </View>
    );
  }
}

// const mapStateToProps = (state) => {
//   return {
//     categories: state.categories,
//     // selectedLayout: state.categories.selectedLayout,
//   };
// };
//
// function mergeProps(stateProps, dispatchProps, ownProps) {
//   const {dispatch} = dispatchProps;
//   const {actions} = require("@redux/CategoryRedux");
//
//   return {
//     ...ownProps,
//     ...stateProps,
//     fetchCategories: () => {
//       actions.fetchCategories(dispatch);
//     },
//     setActiveLayout: (value) => dispatch(actions.setActiveLayout(value)),
//     setSelectedCategory: (category) => dispatch(actions.setSelectedCategory(category))
//   };
// }

const mapStateToProps = (state) => {
  return {
    categories: state.categories,
    selectedCategory: state.categories.selectedCategory,
  };
};

function mergeProps(stateProps, dispatchProps, ownProps) {
  const { categories, selectedCategory } = stateProps;
  const { dispatch } = dispatchProps;
  const { actions } = require("./../../redux/CategoryRedux");
  const subId =
    selectedCategory && selectedCategory.mainCategory
      ? selectedCategory.mainCategory.id
      : selectedCategory.parent;
  return {
    ...ownProps,
    ...stateProps,
    subCategories: [
      selectedCategory.mainCategory,
      ...categories.list.filter((category) => category.parent === subId),
    ],
    setSelectedCategory: (category) =>
      dispatch(actions.setSelectedCategory(category)),
  };
}

export default withTheme(connect(mapStateToProps, undefined, mergeProps)(SubCategoriesTab));
