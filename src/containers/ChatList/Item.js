/** @format */
// @flow
import React from 'react'
import { Images, withTheme } from '@common'
import { TouchableOpacity, View, Image, Text } from 'react-native'
import TimeAgo from 'react-native-timeago'
import styles from './styles'

@withTheme
export default class Item extends React.Component {
  render() {
    const { onChat, item } = this.props
    const {
      theme: {
        colors: {text, lineColor },
      },
    } = this.props;

    return (
      <TouchableOpacity
        onPress={() => onChat(item)}
        style={styles.item}
        key={item.name}>
        <View style={styles.left}>
          <View style={styles.img}>
            <Image
              defaultSource={Images.defaultUserChat}
              source={{
                uri: item.avatar ? item.avatar : Images.defaultUserChat,
              }}
              style={styles.image}
            />
          </View>
          <View style={{}}>
            <Text style={[styles.name, {color: text}]}>{item.name}</Text>
            <TimeAgo style={[styles.time, {color: text}]} time={item.createdAt} />
          </View>
        </View>
        {/*<View style={styles.right}>*/}
          {/*<TouchableOpacity onPress={this._call}>*/}
            {/*<Image source={Images.icons.videocall} style={styles.vcall} />*/}
          {/*</TouchableOpacity>*/}
          {/*<TouchableOpacity onPress={this._call}>*/}
            {/*<Image source={Images.icons.call} style={styles.call} />*/}
          {/*</TouchableOpacity>*/}
        {/*</View>*/}
      </TouchableOpacity>
    )
  }
}
