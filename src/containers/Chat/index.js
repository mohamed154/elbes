/** @format */

// @flow
import React from 'react'
import { View, ImageBackground, Dimensions } from 'react-native'
import { GiftedChat, GiftedAvatar, Bubble } from 'react-native-gifted-chat'
import { Config, Device, withTheme, Languages } from '@common'
import { firebaseApp } from '@services'
import { connect } from 'react-redux'
const { width, height } = Dimensions.get('window')

@withTheme
class Chat extends React.Component {
  constructor(props) {
    super(props)

    this.author = props.author
    this.currentUserId = props.user.id

    this.state = {
      messages: [],
      height: 0,
    }

    if (this.author != null) {
      this.chatRef = firebaseApp
        .ref()
        .child(`chat/${this.generateChatId(this.currentUserId)}`)
      this.chatRefData = this.chatRef.orderByChild('order')
    }
  }

  componentDidMount() {
    this.props.clearChat()
    this.chatRef && this.listenForItems(this.chatRefData)
  }
  componentWillUnmount() {
    firebaseApp.off()
  }

  listenForItems = (chatRef) => {
    //--- case different
    chatRef.on('value', (snap) => {
      // get children as an array
      const items = []
      snap.forEach((child) => {
        items.push({
          _id: child.val().createdAt,
          text: child.val().text,
          createdAt: new Date(child.val().createdAt),
          user: {
            _id: child.val().uid,
          },
        })
      })

      this.setState({
        messages: items,
      })
    })
  }

  generateChatId = (userId) => {
    if (userId > this.author.id) return `${userId}-${this.author.id}`
    return `${this.author.id}-${userId}`
  }

  onSend = (messages = []) => {
    const { user } = this.props
    messages.forEach((message) => {
      const now = new Date().getTime()
      this.chatRef.push({
        _id: now,
        text: message.text,
        createdAt: now,
        uid: this.currentUserId,
        order: -1 * now,
        username: user.username,
        email: user.email,
        name: user.last_name + ' ' + user.first_name,
        avatar: user.avatar_url,
        author: this.author.id,
        read: 0,
      })
    })

    /*--- push for users ---*/

    //from userLogin to author
    firebaseApp
      .ref()
      .child('users')
      .child(this.currentUserId)
      .child(this.author.id)
      .set({
        id: this.author.id,
        name: this.author.username || this.author.name,
        shop_name: this.author.shop_name,
        ...this.author.email ? {email: this.author.email} : null
      });

    // from author to userLogin
    firebaseApp
      .ref()
      .child('users')
      .child(this.author.id)
      .child(this.currentUserId)
      .set({
        id: this.currentUserId,
        name: user.username,
        email: user.email,
        shop_name: ''
      })
  };

  _renderBubble = (props) => {
    // console.log('props', props)
    return (
      <Bubble
        {...props}
        textStyle={{}}
        wrapperStyle={{
          left: {
            borderRadius: 8,
            borderTopLeftRadius: 0,
            backgroundColor: '#FFF',
          },
          right: {
            borderRadius: 8,
            borderTopRightRadius: 0,
            backgroundColor: '#3889F2',
          },
        }}
      />
    )
  }

  _renderAvatar = (props) => {
    return <GiftedAvatar {...props} />
  }

  render() {
    const {
      user,
      theme: {
        colors: {text, lineColor, background },
      },
    } = this.props;

    return (
      <View style={{ flex: 1, paddongTop: 20 }}>
        <ImageBackground
          source={Config.Chat.defaultBg}
          style={{
            position: 'absolute',
            flex: 1,
            width,
            height,
            opacity: Config.Chat.opacityBg,
          }}
        />
        <GiftedChat
          messages={this.state.messages}
          onSend={this.onSend}
          placeholder={Languages.typeAmessage}
          renderBubble={this._renderBubble}
          renderAvatar={this._renderAvatar}
          renderAvatarOnTop={true}
          user={{
            _id: this.currentUserId,
            name: user.last_name || user.first_name,
          }}
          bottomOffset={Device.isIphoneX ? 30 : 0}
          listViewProps={{ backgroundColor: background }}
        />
      </View>
    )
  }
}

const mapStateToProps = ({ user }) => ({ user: user.user })

const mergeProps = (stateProps, dispatchProps, ownProps) => {
  const { dispatch } = dispatchProps
  const { actions: UserActions } = require('@redux/UserRedux')
  return {
    ...ownProps,
    ...stateProps,
    clearChat: () => dispatch(UserActions.clearChat()),
  }
}

export default connect(
  mapStateToProps,
  null,
  mergeProps
)(Chat)
