/** @format */

import React, {PureComponent} from "react";
import {View, ScrollView, Text, Switch, AsyncStorage} from "react-native";
import {connect} from "react-redux";
import {
  UserProfileHeader,
  UserProfileItem,
  ModalBox,
  CurrencyPicker,
} from "@components";
import {Languages, Color, Tools, Config, withTheme} from "@common";
import {getNotification} from "@app/Omni";
import _ from "lodash";
import styles from "./styles";
import {EmptyView} from "../../navigation/IconNav";

class UserProfile extends PureComponent {
  constructor(props) {
    super(props);

    this.state = {
      pushNotification: false,
      isLoading: true,
    };
  }

  async componentDidMount() {
    const notification = await getNotification();
    this.setState({
      pushNotification: notification || false,
    });
  }

  /**
   * TODO: refactor to config.js file
   */
  _getListItem = () => {
    const {
      currency,
      wishListTotal,
      myOrders,
      userProfile,
      carts,
    } = this.props;

    const listItem = [...Config.ProfileSettings];
    const items = [];
    let myOrderIndex = -1;
    for (let i = 0; i < listItem.length; i++) {
      const item = listItem[i];
      if (item.label === "PushNotification") {
        item.icon = () => (
          <Switch
            onValueChange={this._handleSwitch}
            value={this.state.pushNotification}
            tintColor={this.state.pushNotification ? Color.primary : Color.blackDivide}
            thumbTintColor={this.state.pushNotification ? Color.primary : null}
            thumbColor={this.state.pushNotification ? Color.primary : null}
          />
        );
      }

      switch (item.label) {
        case "MyOrder":
          myOrderIndex = i;
          items.push({
            ...item,
            label: `${Languages.MyOrder} (${myOrders})`,
          });
          break;

        case "Currency":
          item.value = currency.code;
          break;

        case "WishList":
          items.push({
            ...item,
            label: `${Languages.WishList} (${wishListTotal})`,
          });
          break;

        case "Cart":
          items.push({
            ...item,
            label: `${Languages.Cart} (${carts.total})`,
          });
          break;

        default:
          items.push({...item, label: Languages[item.label]});
          break;
      }
    }

    if (!userProfile.user) {
      let index = _.findIndex(items, (item) => item.label == Languages.MyOrder);
      if (index > -1) {
        items.splice(index, 1);
      }
      index = _.findIndex(items, (item) => item.label == Languages.Address);
      if (index > -1) {
        items.splice(myOrderIndex, 1);
      }
    }
    return items;
  };

  _handleSwitch = (value) => {
    AsyncStorage.setItem("@notification", JSON.stringify(value), () => {
      this.setState({
        pushNotification: value,
      });
    });
  };

  _handlePress = (item) => {
    const {navigation} = this.props;
    const {routeName, isActionSheet} = item;

    if (routeName && !isActionSheet) {
      // navigation(routeName, item.params);
      navigation(routeName, item.params);
    }

    if (isActionSheet) {
      this.currencyPicker.openModal();
    }
  };

  render() {
    const {userProfile, navigation, currency, changeCurrency} = this.props;
    const user = userProfile.user || {};
    const name = Tools.getName(user);
    const listItem = this._getListItem();
    const {
      theme: {
        colors: {background, text, lineColor},
      },
    } = this.props;

    let pointObject = user && user.meta_data && user.meta_data.find(data => data.key === "mwb_wpr_points");
    pointObject = pointObject ? pointObject : user && user.meta_data && user.meta_data.find(data => data.key === "_woo_pr_userpoints");
    const walletObject = user && user.meta_data && user.meta_data.find(data => data.key === "_current_woo_wallet_balance");

    const pointsNum = pointObject ? pointObject.value : 0;
    const walletBalance = walletObject ? walletObject.value : 0;

    return (
      <View style={[styles.container, {backgroundColor: background}]}>
        <View style={{
          backgroundColor: '#0ca2ff',
          height: 25,
        }}>
        </View>
        <ScrollView ref="scrollView">
          <UserProfileHeader
            onLogin={() => navigation("LoginScreen")}
            onLogout={() => {
              this.props.clearCart();
              this.props.cleanOldCoupon();
              navigation("LoginScreen", {isLogout: true});
            }}
            user={{
              ...user,
              name,
            }}
          />

          {userProfile.user && (
            <View style={[styles.profileSection, {borderColor: lineColor}]}>
              <Text style={styles.headerSection}>
                {Languages.AccountInformations.toUpperCase()}
              </Text>
              <UserProfileItem
                label={Languages.Name}
                onPress={this._handlePress}
                value={name}
              />
              <UserProfileItem label={Languages.Email} value={user.email} fontSize={10}/>
              <UserProfileItem label={Languages.points} value={pointsNum} fontSize={10}/>
              <UserProfileItem label={Languages.wallet_balance} value={walletBalance} fontSize={10}/>
              {/* <UserProfileItem label={Languages.Address} value={user.address} /> */}
            </View>
          )}

          <View style={[styles.profileSection, {borderColor: lineColor}]}>
            {listItem.map((item, index) => {
              return (
                item && (
                  <UserProfileItem
                    icon
                    key={index}
                    onPress={() => this._handlePress(item)}
                    {...item}
                  />
                )
              );
            })}
          </View>
        </ScrollView>

        <ModalBox ref={(c) => (this.currencyPicker = c)}>
          <CurrencyPicker currency={currency} changeCurrency={changeCurrency}/>
        </ModalBox>
      </View>
    );
  }
}

const mapStateToProps = ({user, language, currency, wishList, carts}) => ({
  userProfile: user,
  language,
  currency,
  wishListTotal: wishList.wishListItems.length,
  carts,
  myOrders: carts.myOrders.length,
});

function mergeProps(stateProps, dispatchProps, ownProps) {
  const {dispatch} = dispatchProps;
  const {actions} = require("@redux/CurrencyRedux");
  const {actions: cartActions} = require("@redux/CartRedux");
  const productActions = require("@redux/ProductRedux").actions;
  return {
    ...ownProps,
    ...stateProps,
    changeCurrency: (currnecy) => actions.changeCurrency(dispatch, currnecy),
    clearCart: () => cartActions.emptyCart(dispatch),
    cleanOldCoupon: () => {
      productActions.cleanOldCoupon(dispatch);
    },
  };
}

export default connect(
  mapStateToProps,
  null,
  mergeProps
)(withTheme(UserProfile));
