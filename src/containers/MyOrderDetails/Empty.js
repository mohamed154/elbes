/** @format */

import React, { PureComponent } from "react";
import { Text, View, Image } from "react-native";
import { Languages, Images, withTheme } from "@common";
import { ShopButton } from "@components";
import styles from "./styles";
import { Back } from "@navigation/IconNav";
import {BackWhite} from "../../navigation/IconNav";

class PaymentEmpty extends PureComponent {
  render() {
    const {
      theme: {
        colors: { background, text },
      },
      navigation,
    } = this.props;

    return (
      <View style={[styles.emptyContainer, { backgroundColor: background }]}>
        <View style={styles.headerView}>
          <Text style={styles.headerLabel}>{Languages.ViewMyOrders}</Text>
          <View style={styles.homeMenu}>{BackWhite(navigation)}</View>
        </View>
        <View style={styles.content}>
          <View>
            <Image
              source={Images.IconOrder}
              style={styles.icon}
              resizeMode="contain"
            />
          </View>
          <Text style={[styles.title, { color: text }]}>
            {Languages.MyOrder}
          </Text>
          <Text style={[styles.message, { color: text }]}>
            {this.props.text}
          </Text>

          <ShopButton
            onPress={this.props.onReload}
            text={Languages.reload}
            css={{
              backgroundColor: "#ccc",
              marginTop: 20,
              width: 120,
              height: 40,
            }}
          />
        </View>

        <ShopButton onPress={this.props.onViewHome} />
      </View>
    );
  }
}

export default withTheme(PaymentEmpty);
