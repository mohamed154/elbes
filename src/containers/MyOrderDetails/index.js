/** @format */

import React, {Component} from "react";
import {
  ListView,
  Animated,
  Platform,
  RefreshControl,
  FlatList,
  Text,
  View,
} from "react-native";

import {AnimatedHeader} from "@components";
import {Constants, Languages, withTheme} from "@common";
import styles from "../MyOrders/styles";
import Accordion from "../MyOrders";

const cardMargin = Constants.Dimension.ScreenWidth(0.05);

class MyOrderDetails extends Component {
  state = {scrollY: new Animated.Value(0)};

  renderRow = ({ item, index }) => {





    return (
      <View style={{ margin: cardMargin, marginBottom: 0 }}>
        <View style={styles.labelView}>
          <Text style={styles.label}>#{order.number}</Text>
        </View>
        <View style={{ padding: 5 }}>
          {renderAttribute(Languages.OrderDate, dateFormat(order.date_created))}
          {renderAttribute(Languages.OrderStatus, order.status.toUpperCase())}
          {renderAttribute(Languages.OrderPayment, order.payment_method_title)}
          {renderAttribute(
            Languages.OrderTotal,
            `${order.total} ${order.currency}`,
            {
              fontWeight: "bold",
              fontSize: 16,
              fontFamily: Constants.fontHeader,
              color: "#333",
            }
          )}

          <Accordion
            underlayColor="transparent"
            sections={[]}
            renderHeader={() => {
              return (
                <View style={{ flex: 1, alignItems: "flex-end" }}>
                  <Text style={styles.orderDetailLabel}>
                    {Languages.OrderDetails}
                  </Text>
                </View>
              );
            }}
            renderContent={() => {
              return (
                <ListView
                  contentContainerStyle={{ backgroundColor: "#FFF" }}
                  dataSource={dataSource2}
                  enableEmptySections
                  renderRow={(product) => (
                    <View
                      style={{
                        flexDirection: "row",
                        justifyContent: "space-between",
                      }}>
                      <Text
                        style={{
                          margin: 4,
                          color: "#333",
                          width: Constants.Dimension.ScreenWidth(0.6),
                        }}
                        numberOfLines={2}
                        ellipsizeMode="tail">
                        {product.name}
                      </Text>

                      <Text
                        style={{
                          margin: 4,
                          color: "#333",
                          alignSelf: "center",
                        }}>
                        {`x${product.quantity}`}
                      </Text>

                      <Text
                        style={{
                          margin: 4,
                          color: "#333",
                          alignSelf: "center",
                        }}>
                        {product.total}
                      </Text>
                    </View>
                  )}
                />
              );
            }}
          />
        </View>
      </View>
    );
  };

  render() {

    const order = this.props.order;

    const renderAttribute = (label, context, _style) => {
      return (
        <View style={styles.row}>
          <Text style={[styles.rowLabel, { color: text }]}>{label}</Text>
          <Text style={[styles.rowLabel, _style, { color: text }]}>
            {context}
          </Text>
        </View>
      );
    };

    const dateFormat = (date) => {
      const year = date.substr(0, 4);
      const month = date.substr(5, 2);
      const day = date.substr(8, 2);
      return `${day}/${month}/${year}`;
    };

    return (
      <View>
        <AnimatedHeader
          scrollY={this.state.scrollY}
          label={Languages.orderDetailsTitle}
        />
        <View style={{ margin: cardMargin, marginBottom: 0 }}>
          <View style={styles.labelView}>
            <Text style={styles.label}>#{order.number}</Text>
          </View>
          <View style={{ padding: 5 }}>
            {renderAttribute(Languages.OrderDate, dateFormat(order.date_created))}
            {renderAttribute(Languages.OrderStatus, order.status.toUpperCase())}
            {renderAttribute(Languages.OrderPayment, order.payment_method_title)}
            {renderAttribute(
              Languages.OrderTotal,
              `${order.total} ${order.currency}`,
              {
                fontWeight: "bold",
                fontSize: 16,
                fontFamily: Constants.fontHeader,
                color: "#333",
              }
            )}

            <Accordion
              underlayColor="transparent"
              sections={[]}
              renderHeader={() => {
                return (
                  <View style={{ flex: 1, alignItems: "flex-end" }}>
                    <Text style={styles.orderDetailLabel}>
                      {Languages.OrderDetails}
                    </Text>
                  </View>
                );
              }}
              renderContent={() => {
                return (
                  <ListView
                    contentContainerStyle={{ backgroundColor: "#FFF" }}
                    dataSource={dataSource2}
                    enableEmptySections
                    renderRow={(product) => (
                      <View
                        style={{
                          flexDirection: "row",
                          justifyContent: "space-between",
                        }}>
                        <Text
                          style={{
                            margin: 4,
                            color: "#333",
                            width: Constants.Dimension.ScreenWidth(0.6),
                          }}
                          numberOfLines={2}
                          ellipsizeMode="tail">
                          {product.name}
                        </Text>

                        <Text
                          style={{
                            margin: 4,
                            color: "#333",
                            alignSelf: "center",
                          }}>
                          {`x${product.quantity}`}
                        </Text>

                        <Text
                          style={{
                            margin: 4,
                            color: "#333",
                            alignSelf: "center",
                          }}>
                          {product.total}
                        </Text>
                      </View>
                    )}
                  />
                );
              }}
            />
          </View>
        </View>
      </View>
    );
  }

}

export default withTheme(MyOrderDetails);
