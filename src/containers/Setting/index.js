/** @format */

"use strict";

import React, { Component } from "react";
import {Text, ScrollView, Animated, View, Switch, AsyncStorage} from "react-native";
import { AnimatedHeader,UserProfileItem } from "@components";
import css from "./style";
import { Languages, Color, Tools, Config, withTheme } from "@common";
import LangSwitcher from "./LangSwitch";
import styles from "../UserProfile/styles";
import {connect} from "react-redux";

class Setting extends Component {

  constructor(props) {
    super(props);
    this.state = {
      pushNotification: false,
      scrollY: new Animated.Value(0)
    };
  }

  async componentDidMount() {
    const notification = await getNotification();
    this.setState({
      pushNotification: notification || false,
    });
  }

  _handleSwitch = (value) => {
    AsyncStorage.setItem("@notification", JSON.stringify(value), () => {
      this.setState({
        pushNotification: value,
      });
    });
  };

  render() {
    const { navigation, userProfile} = this.props;
    return (
      <ScrollView style={css.wrap}>
        {/*<AnimatedHeader*/}
          {/*scrollY={this.state.scrollY}*/}
          {/*hideIcon*/}
          {/*label={Languages.Settings}*/}
          {/*navigation={navigation}*/}
        {/*/>*/}
        <View style={css.boxSetting}>
          <Text style={css.text}>{Languages.changeLanguage}:</Text>
          <LangSwitcher />
        </View>
        {/*<View style={[css.boxSetting, {flexDirection: 'row', justifyContent: 'space-between'}]}>*/}
          {/*<Text style={css.text}>{'Push Notifications'}</Text>*/}
          {/*<Switch*/}
            {/*onValueChange={this._handleSwitch}*/}
            {/*value={this.state.pushNotification}*/}
            {/*tintColor={this.state.pushNotification ? Color.primary : Color.blackDivide}*/}
            {/*thumbTintColor={this.state.pushNotification ? Color.primary : null}*/}
            {/*thumbColor={this.state.pushNotification ? Color.primary : null}*/}
          {/*/>*/}
        {/*</View>*/}

        {/*{userProfile.user && (*/}
          {/*<View style={[styles.profileSection, { borderColor: lineColor }]}>*/}
            {/*<Text style={styles.headerSection}>*/}
              {/*{Languages.AccountInformations.toUpperCase()}*/}
            {/*</Text>*/}
            {/*<UserProfileItem*/}
              {/*label={Languages.Name}*/}
              {/*onPress={this._handlePress}*/}
              {/*value={name}*/}
            {/*/>*/}
            {/*<UserProfileItem label={Languages.Email} value={user.email} fontSize={10} />*/}
            {/*/!* <UserProfileItem label={Languages.Address} value={user.address} /> *!/*/}
          {/*</View>*/}
        {/*)}*/}
      </ScrollView>
    );
  }
}

const mapStateToProps = ({ user, language, currency, wishList, carts }) => ({
  userProfile: user,
  language,
  currency,
  wishListTotal: wishList.wishListItems.length,
  carts,
  myOrders: carts.myOrders.length,
});

function mergeProps(stateProps, dispatchProps, ownProps) {
  const { dispatch } = dispatchProps;
  const { actions } = require("@redux/CurrencyRedux");
  return {
    ...ownProps,
    ...stateProps,
    changeCurrency: (currnecy) => actions.changeCurrency(dispatch, currnecy),
  };
}

export default connect(
  mapStateToProps,
  null,
  mergeProps
)(withTheme(Setting));
