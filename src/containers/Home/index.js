/** @format */

// @flow
/**
 * Created by InspireUI on 19/02/2017.
 */
import React, { PureComponent } from "react";
import PropTypes from "prop-types";
import { View } from "react-native";
import { connect } from "react-redux";
import { Constants, withTheme } from "@common";
import { HorizonList, ModalLayout, PostList } from "@components";
import styles from "./styles";
import { Events } from "@common";
import { actions } from "@redux/ProductRedux";
import HList from "../../components/HorizonList";

class Home extends PureComponent {
  static propTypes = {
    fetchAllCountries: PropTypes.func.isRequired,
    layoutHome: PropTypes.any,
    onViewProductScreen: PropTypes.func,
    onShowAll: PropTypes.func,
    showCategoriesScreen: PropTypes.func,
    switchLayoutHomePage: PropTypes.func
  };

  componentWillMount() {
    this.props.switchLanguage(this.props.lang);
  }

  componentDidMount() {
    const { fetchAllCountries, isConnected } = this.props;
    if (isConnected) {
      //alert("CHECK")
      fetchAllCountries();
    }

    //Events.onOpenModalLayout(this.changeHomeLayout);
  }

  changeHomeLayout() {
    // const {switchLayoutHomePage} = this.props;
    this.props.switchLayoutHomePage(2);
  }

  render() {
    const {
      layoutHome,
      onViewProductScreen,
      showCategoriesScreen,
      onShowAll,
      onViewCategory,
      addCartItem,
      cartItems,
      onViewCartScreen,
      theme: {
        colors: { lineColor },
      },
    } = this.props;

    const isHorizontal = layoutHome === Constants.Layout.horizon;
    return (
      <View style={[styles.container, { backgroundColor: lineColor }]}>
        {isHorizontal && (
          <HorizonList
            onShowAll={onShowAll}
            onViewProductScreen={onViewProductScreen}
            showCategoriesScreen={showCategoriesScreen}
            onViewCategory={onViewCategory}
            addCartItem={addCartItem}
            cartItems={cartItems}
            onViewCartScreen={onViewCartScreen}
          />
        )}
        {!isHorizontal && (
          <PostList
            onViewProductScreen={onViewProductScreen}
            addCartItem={addCartItem}
            cartItems={cartItems}
            onViewCartScreen={onViewCartScreen}
          />
        )}
        <ModalLayout />
      </View>
    );
  }
}

const mapStateToProps = ({ user, products, countries, netInfo, carts, language }) => ({
  user,
  layoutHome: products.layoutHome,
  countries,
  isConnected: netInfo.isConnected,
  cartItems: carts.cartItems,
  lang: language.lang,
});

function mergeProps(stateProps, dispatchProps, ownProps) {
  const { dispatch } = dispatchProps;
  const CountryRedux = require("@redux/CountryRedux");
  const ProductRedux = require("@redux/ProductRedux");
  const { actions: CartRedux } = require("@redux/CartRedux");
  const LangRedux  = require("@redux/LangRedux");
  return {
    ...ownProps,
    ...stateProps,
    fetchAllCountries: () => CountryRedux.actions.fetchAllCountries(dispatch),
    switchLayoutHomePage: (layout) => ProductRedux.actions.switchLayoutHomePage(layout),
    addCartItem: (product, variation) => {
      CartRedux.addCartItem(dispatch, product, variation);
    },
    switchLanguage: (language) => LangRedux.actions.switchLanguage(dispatch, language),
  };
}

//const switchLayoutHomePage = actions.switchLayoutHomePage;

export default withTheme(
  connect(
    mapStateToProps,
    // {switchLayoutHomePage},
    undefined,
    mergeProps
  )(Home)
);
