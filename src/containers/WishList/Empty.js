/** @format */

import React, { Component } from "react";
import { Text, View, Image } from "react-native";
import styles from "./styles";
import { Languages, Images, withTheme } from "@common";
import { ShopButton } from "@components";
import { Back } from "@navigation/IconNav";
import {BackWhite, CartWishListIcons} from "../../navigation/IconNav";

class PaymentEmpty extends Component {
  render() {
    const {
      theme: {
        colors: { background, text },
      },
      navigation,
    } = this.props;

    return (
      <View style={[styles.container, { backgroundColor: background }]}>
        {/*<View style={styles.headerView}>*/}
          {/*<Text style={[styles.headerLabel, {color: "white"}]}>{Languages.WishList}</Text>*/}
          {/*<View style={[styles.homeMenu, {color: "white"}]}>{BackWhite(navigation)}</View>*/}
          {/*<View style={[styles.headerRight]}>{CartWishListIcons(navigation)}</View>*/}
        {/*</View>*/}
        <View style={styles.content}>
          <View>
            <Image
              source={Images.IconHeart}
              style={styles.icon}
              resizeMode="contain"
            />
          </View>
          <Text style={[styles.title, { color: text }]}>
            {Languages.EmptyWishList}
          </Text>
          <Text style={[styles.message, { color: text }]}>
            {Languages.NoWishListItem}
          </Text>
        </View>

        <ShopButton onPress={this.props.onViewHome} />
      </View>
    );
  }
}

export default withTheme(PaymentEmpty);
