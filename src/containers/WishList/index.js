/** @format */

import React, {PureComponent} from "react";
import {
  Animated,
  ScrollView,
  View,
  ListView,
  TouchableOpacity, Text, Image, Dimensions,
} from "react-native";
import SegmentedControlTab from 'react-native-segmented-control-tab'
import {connect} from "react-redux";
import {Button, ProductItem, AnimatedHeader} from "@components";
import {SwipeRow} from "react-native-swipe-list-view";
import FontAwesome from "@expo/vector-icons/FontAwesome";
import {Constants, Languages, withTheme, Color, Images} from "@common";
import WishListEmpty from "./Empty";
import styles from "./styles";
import {BackWhite, CartWishListIcons} from "../../navigation/IconNav";
import {getProductImage, BlockTimer} from "@app/Omni";

class WishList extends PureComponent {
  constructor(props) {
    super(props);

    const ds = new ListView.DataSource({
      rowHasChanged: (r1, r2) => r1 !== r2,
    });
    this.state = {
      dataSource: ds.cloneWithRows(props.wishListItems),
      scrollY: new Animated.Value(0),
      selectedTabIndex: 0,
    };
  }

  componentDidMount() {
    // console.log("wishListItems WishList Screen: ",this.props.wishListItems);
  }

  onNext = () => {
    this.setState({currentIndex: this.state.currentIndex + 1});
  };

  renderHiddenRow(rowData, index) {
    return (
      <TouchableOpacity
        style={styles.hiddenRow}
        onPress={() => {
          this.props.removeWishListItem(rowData.product, rowData.variation);
        }}>
        <View style={{marginRight: 23}}>
          <FontAwesome name="trash" size={30} color="white"/>
        </View>
      </TouchableOpacity>
    );
  }

  renderHiddenRowShop(rowData, index) {
    return (
      <TouchableOpacity
        style={styles.hiddenRow}
        onPress={() => {
          this.props.removeWishListShop(rowData.vendor);
        }}>
        <View style={{marginRight: 23}}>
          <FontAwesome name="trash" size={30} color="white"/>
        </View>
      </TouchableOpacity>
    );
  }

  moveAllToCart = () => {
    if (this.props.wishListItems.length === 0) alert(Languages.EmptyAddToCart);
    else {
      this.props.wishListItems.forEach((item) => {
        const inCartTotal = this.props.cartItems.reduce(
          (accumulator, currentValue) => {
            if (currentValue.product.id == item.product.id) {
              return accumulator + currentValue.quantity;
            }
            return 0;
          },
          0
        );

        if (inCartTotal < Constants.LimitAddToCart)
          this.props.addCartItem(item.product, item.variation);
        else alert(Languages.ProductLimitWaring);
      });
    }
  };

  cleanAll = () => {
    const self = this;
    this.props.wishListItems.forEach((currentValue, index, array) => {
      self.props.removeWishListItem(
        currentValue.product,
        currentValue.variation
      );
    });
  };

  cleanAllShops = () => {
    const self = this;
    this.props.wishListShops.forEach((currentValue, index, array) => {
      self.props.removeWishListShop(currentValue.vendor);
    });
  };

  handleIndexChange = (index) => {
    this.setState({
      ...this.state,
      selectedTabIndex: index,
    });
  };

  onRowClickVendor = (item) => {
    const { onViewVendor } = this.props;
    BlockTimer.execute(() => {
      onViewVendor(item);
    }, 500);
  };

  render() {
    const {wishListItems, wishListShops, onViewProduct, navigation} = this.props;
    const {
      theme: {
        colors: {background, text},
      },
    } = this.props;

    const titleTransformY = this.state.scrollY.interpolate({
      inputRange: [0, 50],
      outputRange: [0, 50],
      extrapolate: "clamp",
    });

    const renderProducts = wishListItems.length !== 0
      ? <View>
        <ScrollView
          style={styles.scrollView}
          scrollEventThrottle={1}
          onScroll={Animated.event([
            {nativeEvent: {contentOffset: {y: this.state.scrollY}}},
          ])}>
          <View style={styles.list}>
            {wishListItems &&
            wishListItems.map((item, index) => (
              <SwipeRow
                key={`wishlist${index}`}
                disableRightSwipe
                leftOpenValue={75}
                rightOpenValue={-75}>
                {this.renderHiddenRow(item, index)}
                <ProductItem
                  key={index}
                  product={item.product}
                  onPress={() => onViewProduct({product: item.product})}
                  variation={item.product.variation}
                />
              </SwipeRow>
            ))}
          </View>
          <View style={{height: 30}}></View>
        </ScrollView>
        <View style={styles.buttonContainer}>
          <Button
            text={Languages.CleanAll}
            style={[styles.button, {backgroundColor: "#ff1744"}]}
            textStyle={styles.buttonText}
            onPress={this.cleanAll}
          />
          <Button
            text={Languages.MoveAllToCart}
            style={styles.button}
            textStyle={styles.buttonText}
            onPress={this.moveAllToCart}
          />
        </View>
      </View>
      : <View style={styles.content}>
        <View>
          <Image
            source={Images.IconHeart}
            style={styles.icon}
            resizeMode="contain"
          />
        </View>
        <Text style={[styles.title, {color: text}]}>
          {Languages.EmptyWishList}
        </Text>
        <Text style={[styles.message, {color: text}]}>
          {Languages.NoWishListItem}
        </Text>
      </View>;

    const renderShops = wishListShops.length != 0
      ? <View>
        <ScrollView
          style={styles.scrollView}
          scrollEventThrottle={1}
          onScroll={Animated.event([
            {nativeEvent: {contentOffset: {y: this.state.scrollY}}},
          ])}>
          <View style={styles.list}>
            {wishListShops &&
            wishListShops.map((item, index) => (
              <SwipeRow
                key={`wishlistShop${index}`}
                disableRightSwipe
                leftOpenValue={75}
                rightOpenValue={-75}>
                {this.renderHiddenRowShop(item, index)}
                <View style={[{
                  flex: 1,
                  borderBottomWidth: 1,
                  borderBottomColor: "#d4dce1"
                }, {backgroundColor: background}]}>
                  <View style={{
                    flexDirection: "row",
                    margin: 10,
                  }}>
                    <TouchableOpacity onPress={() => this.onRowClickVendor(item.vendor)}>
                      <Image
                        source={{uri: getProductImage(item.vendor.banner, 100)}}
                        style={{
                          width: 100,
                          height: 100,
                          borderRadius: 50,
                        }}
                      />
                    </TouchableOpacity>
                    <View
                      style={[
                        {
                          marginLeft: 20,
                          marginRight: 20,
                          flex: 1,
                          justifyContent: 'center',
                        },
                        {width: Dimensions.get("window").width - 180},
                      ]}>
                      <TouchableOpacity onPress={() => this.onRowClickVendor(item.vendor)}>
                        <Text style={[{
                          fontSize: 20,
                          fontFamily: Constants.fontFamily,
                          color: Color.Text,
                        }, {color: text}]}>{item.vendor.store_name}</Text>
                      </TouchableOpacity>
                    </View>
                  </View>
                </View>
              </SwipeRow>
            ))}
          </View>
          <View style={{height: 50}}></View>
        </ScrollView>

        <View style={[styles.buttonContainer, {justifyContent: 'center'}]}>
          <Button
            text={"Unfollow All"}
            style={[styles.button, {backgroundColor: "#ff1744"}]}
            textStyle={styles.buttonText}
            onPress={this.cleanAllShops}
          />
          {/*<Button*/}
          {/*text={Languages.MoveAllToCart}*/}
          {/*style={styles.button}*/}
          {/*textStyle={styles.buttonText}*/}
          {/*onPress={this.moveAllToCart}*/}
          {/*/>*/}
        </View>
      </View>
      : <View style={styles.content}>
        <View>
          <Image
            source={Images.IconHeart}
            style={styles.icon}
            resizeMode="contain"
          />
        </View>
        <Text style={[styles.title, {color: text}]}>
          {Languages.EmptyWishList}
        </Text>
        <Text style={[styles.message, {color: text}]}>
          {Languages.NoWishListItem}
        </Text>
      </View>;

    // if (wishListItems.length == 0) {
    //   return (
    //     <WishListEmpty
    //       navigation={this.props.navigation}
    //       onViewHome={this.props.onViewHome}
    //     />
    //   );
    // }
    return (
      <View style={[styles.container, {backgroundColor: background}]}>
        <View>
          <SegmentedControlTab
            values={['Products', 'Shops']}
            selectedIndex={this.state.selectedTabIndex}
            onTabPress={this.handleIndexChange}
            borderRadius={0}
            tabStyle={{
              background: "#fff",
              borderColor: "#fff",
              borderWidth: 0,
              borderBottomWidth: 2,
              height: 45,
              borderBottomColor: "#eee"
            }}
            tabTextStyle={{color: Color.primary}}
            activeTabStyle={{backgroundColor: "#fff", borderBottomColor: Color.primary}}
            activeTabTextStyle={{color: Color.primary}}
          />
        </View>
        {this.state.selectedTabIndex === 1 ? renderShops : renderProducts}
      </View>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    wishListItems: state.wishList.wishListItems,
    wishListShops: state.wishList.wishListShops,
    cartItems: state.carts.cartItems,
  };
};

function mergeProps(stateProps, dispatchProps, ownProps) {
  const {dispatch} = dispatchProps;
  const CartRedux = require("./../../redux/CartRedux");
  const WishListRedux = require("./../../redux/WishListRedux");
  return {
    ...ownProps,
    ...stateProps,
    addCartItem: (product, variation) => {
      CartRedux.actions.addCartItem(dispatch, product, variation);
    },
    removeWishListItem: (product, variation) => {
      WishListRedux.actions.removeWishListItem(dispatch, product, variation);
    },
    removeWishListShop: (vendor, variation) => {
      WishListRedux.actions.removeWishListShop(dispatch, vendor, variation);
    }
  };
}

export default connect(
  mapStateToProps,
  undefined,
  mergeProps
)(withTheme(WishList));
