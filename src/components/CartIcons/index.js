/** @format */

import React, { PureComponent } from "react";
import { View } from "react-native";
import { Styles, Constants, Images } from "@common";
import { connect } from "react-redux";
import { NavigationBarIcon } from "@components";

class CartIcons extends PureComponent {
  render() {
    const { carts, wishList, navigation } = this.props;
    // console.log("carts:::", carts);
    // const totalCart = carts.cartItems.length;
    const totalCart = carts.total;
    const wishListTotal = wishList.wishListItems.length;

    return (
      <View
        style={[
          Styles.Common.Row,
          Constants.RTL ? { left: -10 } : { right: 5 },
        ]}>
        {/*<NavigationBarIcon*/}
          {/*icon={Images.IconWishList}*/}
          {/*number={wishListTotal}*/}
          {/*color={"#fff"}*/}
          {/*onPress={() => navigation.navigate("WishListScreen")}*/}
        {/*/>*/}
        <NavigationBarIcon
          icon={Images.IconCart}
          number={totalCart}
          color={"#fff"}
          onPress={() => navigation.navigate("CartScreen")}
        />
      </View>
    );
  }
}

const mapStateToProps = ({ carts, wishList }) => ({ carts, wishList });
export default connect(mapStateToProps)(CartIcons);
