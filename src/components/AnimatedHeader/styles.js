/** @format */

import { StyleSheet, Platform, Dimensions } from "react-native";
import { Device, Config, Constants } from "@common";

const isAndroid = Platform.OS === "android";
const { width } = Dimensions.get("window");

export default StyleSheet.create({
  headerLabel: {
    color: "#fff",
    fontSize: 24,
    fontFamily: Constants.fontHeader,
    marginBottom: 0,
    marginLeft: 22,
    position: "absolute",
    ...Platform.select({
      ios: {
        top: 60,
      },
      android: {
        top: 34,
        left: 40
      },
    }),
  },
  headerAndroid: {
    flexDirection: "row",
    height: Config.showStatusBar ? 72 : 80,
    backgroundColor: "#0ca2ff",
    borderBottomWidth: 2,
    borderBottomColor: "#0ca2ff"
  },
  headerLabelStatic: {
    color: "#fff",
    fontSize: 24,
    fontFamily: Constants.fontHeader,
    marginBottom: 0,
    marginLeft: 22,
    position: "absolute",
    ...Platform.select({
      ios: {
        top: 60,
      },
      android: {
        top: 34,
        left: 40
      },
    }),
  },

  headerView: {
    width,
    ...Platform.select({
      ios: {
        height: 60,
      },
      android: {
        height: 80//Config.showStatusBar ? 70 : 50,
      },
    }),
  },
  flatlist: {
    paddingTop: 40,
  },
  homeMenu: {
    marginLeft: 0,
    position: "absolute",
    // ...Platform.select({
    //   ios: {
    //     top: Device.isIphoneX ? 50 : 22,
    //   },
    //   android: {
    //     top: Config.showStatusBar ? 30 : 10,
    //   },
    // }),
    top: 20,
    left: 0,
    zIndex: 9,
  },
});
