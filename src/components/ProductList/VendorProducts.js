/** @format */

import React, {Component} from "react";
import {
  FlatList,
  Image,
  Platform,
  RefreshControl,
  Animated,
  View,
  TouchableOpacity, Picker
} from "react-native";
import {Button, Text} from 'native-base';
import {PostLayout, AnimatedHeader, VendorInfo, Spinkit,} from "@components";
import LogoSpinner from "@components/LogoSpinner";
import {Constants, Tools, withTheme} from "@common";
import {connect} from "react-redux";
import styles from "./styles";
import {Color, Icons, Images} from "../../common";
import { Icon } from "@app/Omni";
import {Modal} from "react-native-paper";

const AnimatedFlatList = Animated.createAnimatedComponent(FlatList);

@withTheme
class ProductListVendor extends Component {
  state = {scrollY: new Animated.Value(0)};

  constructor(props) {
    super(props);
    this.page = props.page ? props.page : 0;
    this.limit = Constants.pagingLimit;
    this.isProductList = props.type === undefined;
    this.state = {
      isFetching: true,
      isFetching2: true,
      modalVisible: false,
      sortType: null
    };
    this.list = [];
  }

  componentWillMount() {
    this.fetchData();
    this.props.fetchVendor(this.props.vendor.id)
  }

  fetchData = (reload = false) => {
    if (reload) {
      this.page = 1;
    }
    const {vendor, fetchProducts, fetchVendor} = this.props;

    // this already fetch from previous detail page
    // fetchVendor(vendor.id);
    // console.log(vendor)
    fetchProducts(vendor.id, 2000, this.page);
  };

  fetchMore = () => {
    this.page += 1;
    this.fetchData();
  };

  onRowClickHandle = (item) => {
    if (this.isProductList) {
      this.props.onViewProductScreen({product: item});
    } else {
      this.props.onViewNewsScreen({post: item});
    }
  };

  onViewVendor = (product) => {
    this.props.onViewVendor(product.store);
  };

  renderItem = ({item, index}) => {
    if (item == null) return <View/>;

    const layout = Constants.Layout.twoColumn;

    const {addCartItem, cartItems, onViewCartScreen} = this.props;

    return (
        <PostLayout
          post={item}
          type={this.props.type}
          key={`key-${index}`}
          onViewPost={this.onRowClickHandle.bind(this, item, this.props.type)}
          onViewVendor={this.onViewVendor.bind(this, item, this.props.type)}
          layout={layout}
          addCartItem={addCartItem}
          cartItems={cartItems}
          onViewCartScreen={onViewCartScreen}
        />
    );
  };

  shouldComponentUpdate(nextProps) {
    return nextProps.list != this.props.list;
  }

  componentWillReceiveProps(nextProps, nextContext) {
    // this.state.isFetching = false;
  }

  componentDidMount() {
    setTimeout(() => {
      this.state.isFetching = false
    }, 5000)
  }

  headerComponent = () => {
    return (
      <View style={styles.headerView}>
        <VendorInfo vendor={this.props.vendor} onChat={this.props.onChat} />
        <View style={{flexDirection: 'row', justifyContent: 'flex-end', paddingHorizontal: 15, paddingBottom: 15}}>
          <TouchableOpacity onPress={() => this.setModalVisible(true)}>
            <Image source={Images.sortIcon} style={{width: 25, height: 25}} />
          </TouchableOpacity>
        </View>
      </View>
    );
  };

  setModalVisible(visible) {
    this.state.modalVisible = visible;
    this.forceUpdate();
  }

  render() {
    const {list, vendor, isFetching, onChat} = this.props;
    const {
      theme: {
        colors: {background},
      },
    } = this.props;

    this.setState({list: list});

    const renderFooter = () => isFetching && <Spinkit/>;

    if (this.props.isFetchingProducts || this.props.isFetching || this.state.isFetching) {
      return <LogoSpinner fullStretch/>;
    }

    let filteredList = list;
    switch (this.state.sortType) {
      case 'priceLowToHigh':
        filteredList = filteredList.sort((a, b) => a.price >= b.price);
        break;
      case 'priceHighToLow':
        filteredList = filteredList.sort((a, b) => a.price <= b.price);
        break;
      case 'rateHighToLow':
        filteredList = filteredList.sort((a, b) => a.average_rating <= b.average_rating);
        break;
      default:
        filteredList = list;
    }

    const onModalSort = () => {
      this.setState({list: filteredList, modalVisible: false});
      this.forceUpdate();
    };

    return (
      <View style={[styles.listView, {backgroundColor: background}]}>
        {/*<AnimatedHeader*/}
        {/*scrollY={this.state.scrollY}*/}
        {/*hideIcon*/}
        {/*backIcon*/}
        {/*navigation={this.props.navigation}*/}
        {/*label={Tools.getShopName(vendor)}*/}
        {/*/>*/}

        {/*<View style={styles.container}>*/}
        {/*<View style={styles.header}>*/}
        {/*<Image source={Images.PlaceHolder} style={styles.logo}/>*/}
        {/*<View style={styles.insideContainer}>*/}
        {/**/}
        {/*</View>*/}
        {/*</View>*/}
        {/*</View>*/}
        <AnimatedFlatList
          contentContainerStyle={[styles.flatlist, {marginTop: 50, paddingBottom: 80}]}
          data={filteredList}
          numColumns={2}
          keyExtractor={(item, index) => `${item.id} || ${index}`}
          renderItem={this.renderItem}
          ListHeaderComponent={this.headerComponent}
          ListFooterComponent={renderFooter()}
          refreshing={isFetching}
          refreshControl={
            <RefreshControl
              refreshing={isFetching}
              onRefresh={() => this.fetchData(true)}
            />
          }
          onEndReachedThreshold={100}
          onEndReached={(distance) =>
            distance.distanceFromEnd > 100 && this.fetchMore()
          }
          scrollEventThrottle={1}
          onScroll={Animated.event(
            [{nativeEvent: {contentOffset: {y: this.state.scrollY}}}],
            {useNativeDriver: Platform.OS != "android"}
          )}
        />
        <Modal visible={this.state.modalVisible}
               onRequestClose={() => {
                 this.setModalVisible(false)
               }}>
          <View style={styles.modalContent}>
            <Text style={{marginBottom: 10, fontSize: 20, fontWeight: 'bold'}}>Sort</Text>
            <Text style={{marginTop: 10}}>Sort by</Text>
            <View style={styles.modalPicker}>
              <Picker
                selectedValue={this.state.sortType}
                style={{height: 50}}
                onValueChange={(itemValue, itemIndex) => {
                  this.state.sortType = itemValue;
                  this.forceUpdate();
                }}
              >
                <Picker.Item label={"No filter"} value={null}/>
                <Picker.Item label={"Price - Low to High"} value={'priceLowToHigh'}/>
                <Picker.Item label={"Price - High to Low"} value={'priceHighToLow'}/>
                <Picker.Item label={"Rate - High to Low"} value={'rateHighToLow'}/>
              </Picker>
            </View>
            <View style={styles.modalButtonWrapper}>
              <TouchableOpacity onPress={() => this.setModalVisible(false)} style={styles.closeModalBtn}>
                <Text>close</Text>
              </TouchableOpacity>
              <TouchableOpacity style={styles.filterModalBtn} onPress={onModalSort}>
                <Text>Sort</Text>
              </TouchableOpacity>
            </View>
          </View>
        </Modal>
      </View>
    );
  }
}

const mapStateToProps = ({vendor, carts}, ownProps) => {
  const list = vendor && vendor.products;
  const isFetching = vendor && vendor.isFetching;
  const isFetchingProducts = vendor && vendor.isFetchingProducts;
  return {
    list,
    isFetching,
    vendor: ownProps.vendor,
    vendorInfo: vendor.vendor,
    cartItems: carts.cartItems,
  };
};

const mergeProps = (stateProps, dispatchProps, ownProps) => {
  const {dispatch} = dispatchProps;
  const Vendor = require("@redux/VendorRedux");
  const {actions: CartRedux} = require("@redux/CartRedux");
  return {
    ...ownProps,
    ...stateProps,
    fetchProducts: (vendorId, per_page, page) => {
      return Vendor.actions.fetchProductsByVendor(
        dispatch,
        vendorId,
        per_page,
        page
      );
    },
    fetchVendor: (vendorId) => {
      return Vendor.actions.fetchVendor(dispatch, vendorId);
    },
    addCartItem: (product, variation) => {
      CartRedux.addCartItem(dispatch, product, variation);
    },
  };
};

export default connect(
  mapStateToProps,
  null,
  mergeProps
)(ProductListVendor);
