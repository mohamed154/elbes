/** @format */

import { StyleSheet, Platform, Dimensions } from "react-native";
import { Constants } from "@common";
import {Color} from "../../common";

const { width, height } = Dimensions.get("window");

export default StyleSheet.create({
  flatlist: {
    flexWrap: "wrap",
    flexDirection: "row",
    paddingBottom: 40,
    paddingTop: Platform.OS === "ios" ? 50 : 20,
  },
  more: {
    width,
    alignItems: "center",
    marginBottom: 10,
    marginTop: 10,
  },
  spinView: {
    width,
    backgroundColor: "#fff",
    flex: 1,
    paddingTop: 20,
  },
  header: {
    position: "absolute",
    top: 0,
    left: 0,
    right: 0,
    backgroundColor: Color.primary,
    overflow: "hidden",
    height: Constants.Window.headerHeight,
  },
  headerText: {
    fontSize: 22,
    fontFamily: Constants.fontHeader,
    width,
    marginBottom: 20,
    marginTop: 50,
    marginLeft: 15,
  },

  bannerImage: {
    width: width - 40,
    marginLeft: 20,
    borderRadius: 6,
    flex: 1,
    height: (25 * height) / 100,
    // resizeMode: 'cover',
  },
  headerView: {
    width,
    backgroundColor: '#fff',
  },
  listView: {
    flex: 1,
    backgroundColor: "rgba(255,255,255,1)",
    ...Platform.select({
      android: {
        marginTop: 0,
      },
    }),
    paddingTop: 0,
    marginTop: 0,
  },
  listHeaderText: {
    fontWeight: 'bold',
    fontSize: 24,
    marginLeft: 15,
    /*transform : [
      {translateY: -50}
    ]*/
    position: 'absolute',
    top: 50,
    left: 10
  },
  modalContent: {
    backgroundColor: '#fff',
    // height: 150,
    width: 350,
    borderRadius: 5,
    padding: 20,
    marginLeft: (Dimensions.get('window').width - 350) / 2
  },
  modalPicker: {
    borderWidth: 1,
    borderColor: '#000',
    marginTop: 10,
    borderRadius: 5
  },
  modalButtonWrapper: {
    alignItems: 'center',
    justifyContent: 'center',
    flexDirection: 'row',
    marginTop: 20
  },
  closeModalBtn: {
    flex: 1,
    backgroundColor: '#eee',
    alignItems: 'center',
    justifyContent: 'center',
    padding: 10,
    borderRadius: 5,
    marginHorizontal: 10
  },
  filterModalBtn: {
    flex: 1,
    backgroundColor: Color.primary,
    alignItems: 'center',
    justifyContent: 'center',
    padding: 10,
    borderRadius: 5,
    marginHorizontal: 10
  },
  buttonChat: {
    position: "absolute",
    bottom: 35,
    right: 5,
    zIndex: 99999,
    width: 50,
    height: 50,
    backgroundColor: 'transparent',
  },
  imageButtonChat: {
    width: 32,
    height: 32,
    resizeMode: "contain",
    position: "absolute",
    right: 5,
    zIndex: 999,
  },
});
