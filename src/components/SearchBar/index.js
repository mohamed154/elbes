/** @format */

import React, { PureComponent } from 'react'
import { View, Image, Text, TouchableOpacity, FlatList, TextInput, I18nManager } from 'react-native'
import {Images, Constants, Languages } from '@common'
import styles from './styles'

class SearchBar extends PureComponent {

  _onSearch = (searchText) => {
    if (searchText.length > 0 && searchText.trim().length > 2) {
      this.props.onSearch(searchText)
    } else {
      this.props.stopSearch()
    }
  }

  render() {
    const { style } = this.props
    const hitSlop = { top: 15, right: 15, left: 15, bottom: 15 }
    
    return (
      <View style={[styles.container, style]}>
        <View style={styles.searchWrap}>
          <TextInput
            style={[styles.input, I18nManager.isRTL && {direction: 'rtl'}]}
            autoCapitalize={'none'}
            autoCorrect={false}
            placeholder={Languages.SearchPlaceHolder + '...'}
            placeholderTextColor="#999"
            underlineColorAndroid={'transparent'}
            clearButtonMode={'while-editing'}
            // value={searchText}
            onChangeText={this._onSearch}
            // onSubmitEditing={this._onSearch}
          />
          <TouchableOpacity style={styles.btnFilSearch} hitSlop={hitSlop} onPress={this._onSearch}>
            <Image source={Images.IconSearch} style={styles.searchIcon} />
          </TouchableOpacity>
        </View>
      </View>
    )
  }
}

export default SearchBar
