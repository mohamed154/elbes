/** @format */

import React, {PureComponent} from "react";
import {
  Text,
  TextInput,
  FlatList,
  View,
  TouchableOpacity,
  I18nManager,
  Animated,
} from "react-native";
import {connect} from "react-redux";

import {Color, Constants, withTheme, Icons, Languages} from "@common";
import {FlatButton, ProductItem, AnimatedHeader} from "@components";
import {BlockTimer, warn} from "@app/Omni";
import styles from "./styles";

import SearchBar from "./SearchBar";
import Recents from "./Recents";
import DefaultSearch from "./DefaultSearch";

const AnimatedFlatList = Animated.createAnimatedComponent(FlatList);

class Search extends PureComponent {
  constructor(props) {
    super(props);
    this.page = 1;
    this.limit = Constants.pagingLimit;
    this.vendor = null;
    this.state = {
      text: "",
      isSubmit: false,
      loading: false,
      focus: true,
      scrollY: new Animated.Value(0),
      filter: {},
    };
  }

  scrollY = new Animated.Value(0);

  onBack = () => {
    this.setState({text: ""});
    Keyboard.dismiss();
    this.props.onBack();
  };

  componentDidMount() {
    const {fetchCategories, listCategories} = this.props;
    if (listCategories && listCategories.length === 0) {
      fetchCategories();
    }
  }

  startNewSearch = async () => {
    const {list} = this.props;
    this.setState({loading: true, isSubmit: true});
    await this.props.fetchProductsByName(
      this.state.text,
      this.limit,
      this.page
    );
    if (typeof list !== "undefined") {
      this.setState({loading: false});
    }
  };

  onRowClickHandle = (product) => {
    BlockTimer.execute(() => {
      this.props.onViewProductScreen({product});
    }, 500);
  };

  renderItem = ({item}) => {
    warn(item);

    if (item == null) return <View/>;

    return (
      <ProductItem
        small
        product={item}
        onPress={() => this.onRowClickHandle(item)}
      />
    );
  };

  nextPosts = () => {
    this.page += 1;
    this.props.fetchProductsByName(this.state.text, this.limit, this.page);
  };

  renderHeader = () => {
    const {
      theme: {
        colors: {background, text},
      },
      navigation,
    } = this.props;

    return (
      <Recents
        histories={this.props.histories}
        searchText={this.state.text}
        onClear={this.props.clearSearchHistory}
        onSearch={this.onSearch}
      />
    );
  };

  _showCategory = (category) => {
    const {setSelectedCategory, onViewCategory} = this.props;
    setSelectedCategory(category);
    onViewCategory(category);
  };

  _onViewProductScreen = (item, index, isCategory) => {
    if (isCategory) {
      this._showCategory(item);
    } else {
      this.props.onViewProductScreen(item);
    }
  };

  _onViewVendor = (item) => {
    this.props.onViewVendor(item);
  };

  renderResultList = () => {
    const {list, vendorList, isFetching, listSearch, listCategories} = this.props;

    list.concat(vendorList);

    const onScroll = Animated.event(
      [
        {
          nativeEvent: {
            contentOffset: {
              y: this.state.scrollY,
            },
          },
        },
      ],
      {useNativeDriver: true}
    );

    if (this.state.text == "" && list.length == 0) {
      return (
        <View>
          {this.renderHeader()}
          <DefaultSearch
            {...this.props}
            onViewProductScreen={this._onViewProductScreen}
            onViewVendor={this._onViewVendor}
          />
        </View>
      );
    }

    warn(list)

    return (
      <AnimatedFlatList
        keyExtractor={(item, index) => `${item.id} || ${index}`}
        contentContainerStyle={styles.flatlist}
        data={list}
        scrollEventThrottle={1}
        renderItem={this.renderItem}
        ListHeaderComponent={this.renderHeader}
        ListFooterComponent={() => {
          return list.length > 20 ? (
            <View style={styles.more}>
              <FlatButton
                name="arrow-down"
                text={isFetching ? "LOADING..." : "MORE"}
                load={this.nextPosts}
              />
            </View>
          ) : null;
        }}
        {...{onScroll}}
      />
    );
  };

  render() {
    const {
      theme: {
        colors: {background, text},
      },
    } = this.props;

    return (
      <View style={[styles.container, {backgroundColor: background}]}>
        <SearchBar
          scrollY={this.state.scrollY}
          autoFocus={this.state.focus}
          value={this.state.text}
          onChangeText={(text) => this.setState({text})}
          onSubmitEditing={this.searchProduct}
          onClear={() => this.setState({text: ""})}
          onFilter={() => this.props.onFilter(this.onFilter)}
          isShowFilter={true}
          haveFilter={Object.keys(this.state.filter).length > 0}
        />
        {this.renderResultList()}
      </View>
    );
  }

  searchProduct = () => {
    this.props.saveSearchHistory(this.state.text);
    this.startNewSearch();
  };

  onSearch = (text) => {
    this.setState({text}, this.searchProduct);
  };

  onFilter = async (filter, vendor) => {
    const {list} = this.props;
    this.setState({loading: true, isSubmit: true, filter});

    await this.props.filterProducts(
      this.state.text,
      this.limit,
      this.page,
      filter
    );
    await this.props.fetchProducts(this.vendor.id, 20, 1);
    if (typeof list !== "undefined") {
      this.setState({loading: false});
    }
  };
}

Search.defaultProps = {
  histories: [],
};

const mapStateToProps = ({products, categories, vendor}) => ({
  list: products.productsByName,
  vendorList: vendor && vendor.products,
  isFetching: products.isFetching,
  histories: products.histories,
  listCategories: categories.list,
});

const mergeProps = (stateProps, dispatchProps, ownProps) => {
  const {dispatch} = dispatchProps;
  const {actions} = require("@redux/ProductRedux");
  const CategoryRedux = require("@redux/CategoryRedux");
  const Vendor = require("@redux/VendorRedux");

  return {
    ...ownProps,
    ...stateProps,
    fetchCategories: () => CategoryRedux.actions.fetchCategories(dispatch),
    setSelectedCategory: (category) =>
      dispatch(CategoryRedux.actions.setSelectedCategory(category)),
    fetchProductsByName: (name, per_page, page, filter = {}) => {
      if (name.length > 0) {
        actions.fetchProductsByName(dispatch, name, per_page, page, filter);
      }
    },
    saveSearchHistory: (searchText) => {
      if (searchText.length > 0) {
        actions.saveSearchHistory(dispatch, searchText);
      }
    },
    clearSearchHistory: () => {
      actions.clearSearchHistory(dispatch);
    },
    filterProducts: (name, per_page, page, filter = {}) => {
      actions.fetchProductsByName(dispatch, name, per_page, page, filter);
    },
    fetchVendor: (vendorId) => {
      return Vendor.actions.fetchVendor(dispatch, vendorId);
    },
    fetchProducts: (vendorId, per_page, page) => {
      return Vendor.actions.fetchProductsByVendor(
        dispatch,
        vendorId,
        per_page,
        page
      );
    },
  };
};
module.exports = withTheme(
  connect(
    mapStateToProps,
    null,
    mergeProps
  )(Search)
);
