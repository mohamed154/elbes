/** @format */

"use strict";
import React, { Component } from "react";
import { Text, View, Image, TouchableOpacity } from "react-native";
import styles from "./styles";
import { Images, Tools, Styles } from "@common";
import { LinearGradient } from "expo";
import {getProductImage} from "@app/Omni"

export default class HorizonItemSearch extends Component {
  render() {
    const { onViewProductScreen, data, width, height, styleImage } = this.props;
    const imageURL =
      data.image !== null && data.image.src
        ? getProductImage(data.image.src, Styles.width/2)
        : Images.dataPlaceholder;

    return (
      <TouchableOpacity
        activeOpacity={0.9}
        style={styles.panel}
        onPress={onViewProductScreen}>
        <Image
          source={{ uri: imageURL }}
          style={[
            styles.imagePanel,
            width && { width },
            height && { height },
            styleImage,
          ]}
        />

        <LinearGradient
          style={[styles.linearGradient, width && { width }]}
          colors={["rgba(0,0,0, 0)", "rgba(0, 0, 0, 0.4)"]}
        />

        <View style={styles.titleView}>
          <Text style={styles.title}>
            {Tools.getDescription(data.name, 200)}
          </Text>
        </View>
      </TouchableOpacity>
    );
  }
}
