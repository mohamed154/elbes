/** @format */

"use strict";
import React, { Component } from "react";
import { View, Text, FlatList, ScrollView } from "react-native";
import styles from "./styles";
import { Languages } from "@common";
import HorizonItemSearch from "./HorizonItemSearch";
import VerticalItemSearch from "./VerticalItemSearch";

export default class DefaultSearch extends Component {
  _renderHeader = (title) => {
    return (
      <View style={styles.headerTitle}>
        <Text style={styles.headerText}>{title}</Text>
      </View>
    );
  };

  _keyExtractor = (item, index) => index.toString();

  render() {
    const {
      onViewProductScreen,
      onViewVendor,
      listCategories,
      listProducts,
    } = this.props;
    let list = listCategories.filter(
      (category) => category.parent === 0 && category.name != "Uncategorized"
    );
    return (
      <ScrollView>
        {this._renderHeader(Languages.Categories)}
        <FlatList
          data={list}
          numColumns={2}
          horizontal={false}
          showsHorizontalScrollIndicator={false}
          keyExtractor={this._keyExtractor}
          renderItem={({ item, index }) => (
            <HorizonItemSearch
              key={index}
              data={item}
              onViewProductScreen={() => onViewProductScreen(item, index, true)}
              onViewVendor={() => onViewVendor(item.store)}
              width={140}
              height={140}
            />
          )}
        />
        {listProducts && this._renderHeader(Languages.Recommend)}
        <FlatList
          data={listProducts}
          horizontal={false}
          keyExtractor={this._keyExtractor}
          renderItem={({ item, index }) => (
            <VerticalItemSearch
              key={index}
              data={item}
              onViewProductScreen={() => onViewProductScreen(item, index)}
              onViewVendor={() => onViewVendor(item.store)}
              width={null}
              height={180}
              styleImage={{
                marginRight: 8,
              }}
            />
          )}
        />
      </ScrollView>
    );
  }
}
