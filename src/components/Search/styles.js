/** @format */

import { StyleSheet, Platform, Dimensions } from "react-native";
import { Constants } from "@common";
const { width, height } = Dimensions.get("window");

export default StyleSheet.create({
  container: {
    flex: 1,
    paddingTop: 30,
  },
  flatlist: {
    paddingTop: 8,
    paddingBottom: 20,
  },
  more: {
    width,
    alignItems: "center",
    paddingBottom: 10,
    paddingTop: 10,
  },

  headerLabel: {
    color: "#333",
    fontSize: 28,
    fontFamily: Constants.fontHeader,
    marginBottom: 0,
    paddingTop: 30,
    marginLeft: 22,
  },

  navi: {
    flexDirection: "row",
    height: Platform.OS === "ios" ? 64 : 44,
    borderBottomWidth: 0.5,
    borderColor: "#cccccc",
    marginTop: 40,
  },
  btnClose: {
    marginTop: 18,
    height: 46,
    width: 60,
    justifyContent: "center",
    alignItems: "center",
    borderRightWidth: 0.5,
    borderColor: "#cccccc",
  },
  inputSearch: {
    flex: 1,
    marginTop: 18,
    height: 46,
    marginLeft: 8,
    marginRight: 8,
  },
  //
  searchBar: {
    flex: 0,
  },
  searchIcon: {
    width: 15,
    height: 15,
    resizeMode: "contain",
    tintColor: "#999",
    marginHorizontal: 10,
  },
  input: {
    flex: 1,
    fontSize: 16,
    fontFamily: Constants.fontFamily,
    marginVertical: 15,
  },
  msgWrap: {
    flex: 1,
    alignItems: "center",
    justifyContent: "center",
  },

  msg: {
    fontSize: Constants.fontText.fontSizeMax,
    fontFamily: Constants.fontFamily,
    color: "#999",
  },

  loadingContainer: {
    height: height * 0.8,
  },

  panel: {
    flex: 1,
    marginBottom: 15,
    marginLeft: 10,
    borderRadius: 8,
  },

  imagePanel: {
    borderRadius: 8,
    alignItems: "center",
    marginLeft: 8,
  },
  overlay: {
    alignItems: "center",
    justifyContent: "center",
    backgroundColor: "rgba(0,0,0,0.4)",
  },
  titleView: {
    flex: 1,
    alignItems: "center",
    justifyContent: "center",
    position: "absolute",
    left: 20,
    bottom: 10,
  },
  title: {
    fontSize: 18,
    color: "white",
    fontFamily: Constants.fontFamilyBold,
    marginRight: 20,
  },
  count: {
    fontSize: 15,
    color: "#fff",
    marginTop: 4,
    backgroundColor: "transparent",
    fontFamily: Constants.fontFamily,
  },
  headerTitle: {
    marginTop: 30,
    marginBottom: 15,
    flexDirection: "row",
    justifyContent: "flex-start",
    alignItems: "center",
    marginLeft: 20,
  },
  headerText: {
    fontSize: 18,
    color: "#333",
    letterSpacing: 0.5,
    fontWeight: '600',
    fontFamily: Constants.fontFamilyBold,
  },
  linearGradient: {
    height: 60,
    position: "absolute",
    bottom: 0,
    left: 8,
    borderRadius: 8,
    justifyContent: "flex-end",
  },
  filters: {
    flexDirection: "row",
    alignItems: "center",
    flexWrap: "wrap",
    paddingHorizontal: 10,
    marginBottom: 30,
    paddingTop: 5,
  },
  txtFilter: {
    fontSize: 14,
    fontWeight: "600",
    color: "#000",
  },
  txtText: {
    fontSize: 12,
    color: "#333",
    fontWeight: "400",
    fontStyle: "italic",
  },
  //close Button
  closeWrap: {
    position: "absolute",
    top: 2,
    right: 10,
    zIndex: 9999,
  },
  btnCloseSearch: {
    zIndex: 9999,
  },
});
