import {StyleSheet, Platform} from 'react-native'
import {Color, Constants} from '@common'

export default StyleSheet.create({
  container:{
    flexDirection: 'row',
    marginHorizontal: 20,
    marginTop: 20,
    marginBottom: 10,
    alignItems:'center',
    justifyContent:'space-between',
  },
  recents:{
    flexDirection:'row',
    alignItems:'center'
  },
  text:{
    fontSize: 18,
    marginRight: 5,
    fontWeight: '600',
    fontFamily: Constants.fontFamilyBold,
  }
})
