/** @format */

'use strict'
import React, { PureComponent } from 'react'
import { Text, View, TouchableOpacity } from 'react-native'
import styles from './styles'
import { Constants, Images, Tools, Config } from '@common'
import { PostLayout } from '@components'

export default class VerticalItemSearch extends PureComponent {
  render() {
    const { onViewProductScreen, onViewVendor, data, width, height, styleImage } = this.props

    return (
      <PostLayout
        post={data}
        key={data.ID}
        onViewPost={onViewProductScreen}
        onViewVendor={onViewVendor}
        layout={Constants.Layout.list}
      />
    )
  }
}
