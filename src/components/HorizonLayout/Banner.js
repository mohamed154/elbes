/** @format */

import React, {PureComponent} from "react";
import {View, Text, I18nManager} from "react-native";
import {Images, Styles} from "@common";
import {ImageCache, WishListIcon, Rating, TouchableScale} from "@components";
import {getProductImage, currencyFormatter} from "@app/Omni";
import {LinearGradient} from "@expo";
import css from "./style";
import {Color} from "../../common";

export default class BannerLarge extends PureComponent {
  render() {
    const {viewPost, title, store, product} = this.props;
    const imageURI =
      typeof product.images[0] !== "undefined"
        ? getProductImage(product.images[0].src, Styles.width)
        : Images.PlaceHolder;
    const productPrice = `${currencyFormatter(product.price)} `;
    const productPriceSale = product.on_sale
      ? `${currencyFormatter(product.regular_price)} `
      : null;

    return (
      <TouchableScale onPress={viewPost} style={css.bannerViewShadow}>
        <View activeOpacity={1} style={css.bannerView}>
          <ImageCache
            uri={imageURI}
            defaultSource={Images.imageHolder}
            style={css.bannerImage}
          />

          <LinearGradient
            colors={["rgba(255,255,255,0)", "rgba(255,255,255, 0.3)"]}
            style={[css.bannerOverlay, I18nManager.isRTL && {
              position: 'absolute', bottom: 0, left: 0
            }]}>
            <Text numberOfLines={1} ellipsizeMode='tail' style={[css.bannerTitle, I18nManager.isRTL && {
              textAlign: 'left', direction: 'ltr'
            }]}>
              {title} {store && ' - ' + store.toUpperCase()}
            </Text>
            <View style={[css.priceView, I18nManager.isRTL && {
              marginRight: null, marginLeft: 10, justifyContent: "flex-start",
            }]}>
              <Text style={[css.price]}>{productPrice}</Text>

              <Text style={[css.price, product.on_sale && css.sale_price]}>
                {productPriceSale}
              </Text>

              {productPriceSale &&
              <Text style={{backgroundColor: Color.primary, borderRadius: 5, marginLeft: 2, paddingHorizontal: 2}}>
                -{(product.price / product.regular_price * 100).toFixed(0)}%
              </Text>}

              <Rating rating={Number(product.average_rating)}/>
            </View>
          </LinearGradient>

          <WishListIcon product={product} bottom={true}/>
        </View>
      </TouchableScale>
    );
  }
}
