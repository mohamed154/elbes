/** @format */

import React, { PureComponent } from "react";
import { Text, TouchableOpacity } from "react-native";
import { Styles, Images, withTheme } from "@common";
import { ProductPrice, ImageCache, Rating, WishListIcon } from "@components";
import { getProductImage } from "@app/Omni";
import css from "./style";

class ThreeColumn extends PureComponent {
  render() {
    const {
      viewPost,
      title,
      product,
      store,
      theme: {
        colors: { text, background },
      },
    } = this.props;
    const imageURI =
      typeof product.images[0] !== "undefined"
        ? getProductImage(product.images[0].src, Styles.width/3)
        : Images.PlaceHolderURL;

    return (
      <TouchableOpacity
        activeOpacity={0.9}
        style={[css.panelThree, {backgroundColor: background}]}
        onPress={viewPost}>
        <ImageCache
          uri={imageURI}
          defaultSource={Images.imageHolder}
          style={css.imagePanelThree}
        />
        <Text numberOfLines={3} style={[css.nameThree, { color: text }]}>
          {title} {store && '- ' + store.toUpperCase()}
        </Text>
        <ProductPrice product={product} hideDisCount />
        <WishListIcon product={product} />
        <Rating rating={Number(product.average_rating)}/>
      </TouchableOpacity>
    );
  }
}

export default withTheme(ThreeColumn);
