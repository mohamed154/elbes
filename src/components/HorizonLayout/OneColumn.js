/** @format */

import React, { PureComponent } from "react";
import { Text, TouchableOpacity } from "react-native";
import { ProductPrice, ImageCache, WishListIcon, Rating } from "@components";
import { Images, Styles, withTheme } from "@common";
import { getProductImage } from "@app/Omni";
import css from "./style";

class OneColumn extends PureComponent {
  render() {
    const {
      product,
      title,
      viewPost,
      store,
      theme: {
        colors: { text, background },
      },
    } = this.props;

    const imageURI =
      typeof product.images[0] !== "undefined"
        ? getProductImage(product.images[0].src, 190)
        : Images.PlaceHolderURL;

    return (
      <TouchableOpacity
        activeOpacity={0.9}
        style={[css.panelTwoHigh, {backgroundColor: background} ]}
        onPress={viewPost}>
        <ImageCache
          uri={imageURI}
          defaultSource={Images.imageHolder}
          style={css.imagePanelTwoHigh}
        />
        <Text numberOfLines={3} style={[css.nameTwo, { color: text }]}>
          {title} {store && '- ' + store.toUpperCase()}
        </Text>
        <ProductPrice product={product} hideDisCount />
        <WishListIcon product={product} />
        <Rating rating={product.average_rating} />
      </TouchableOpacity>
    );
  }
}

export default withTheme(OneColumn);
