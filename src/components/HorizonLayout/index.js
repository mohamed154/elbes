/** @format */

import React, {Component} from "react";
import {Images, Constants, Styles, Tools} from "@common";
import {getProductImage} from "@app/Omni";

import ColumnHigh from "./OneColumn";
import TwoColumn from "./TwoColumn";
import ThreeColumn from "./ThreeColumn";
import Card from "./Card";
import BannerLarge from "./BannerLarge";
import Banner from "./Banner";
import BannerHigh from './BannerHigh'
import BannerSale from './BannerSale'

export default class HorizonLayout extends Component {
  render() {
    const {onViewPost, product, addCartItem, cartItems, onViewCartScreen} = this.props;
    const title = Tools.getDescription(product.name);

    const imageURL = product.images.length > 0
      ? getProductImage(product.images[0].src, Styles.width)
      : Images.PlaceHolderURL;

    let store = typeof product.store != 'undefined' ? product.store.shop_name : ''

    const props = {
      imageURL,
      title,
      viewPost: onViewPost,
      product,
      store,
      addCartItem,
      cartItems,
      onViewCartScreen
    };

    switch (this.props.layout) {
      case Constants.Layout.twoColumn:
        return <TwoColumn {...props}/>;
      case Constants.Layout.threeColumn:
        return <ThreeColumn {...props}/>;
      case Constants.Layout.BannerLarge:
        return <BannerLarge {...props}/>;
      case Constants.Layout.card:
        return <Card {...props}/>;
      case Constants.Layout.Banner:
        return <Banner {...props}/>;
      case Constants.Layout.BannerHigh:
        return <BannerHigh {...props}/>;
      case Constants.Layout.BannerSale:
        return <BannerSale {...props} />;
      default:
        return <ColumnHigh {...props}/>;
    }
  }
}
