/** @format */

import React, {PureComponent} from "react";
import {Text, TouchableOpacity, Button} from "react-native";
import {
  Images,
  Styles,
  Languages,
  Config,
  AppConfig,
  Constants,
  Events,
  withTheme,
} from "@common";
import {WishListIcon, ImageCache, ProductPrice, Rating} from "@components";
import {getProductImage} from "@app/Omni";
import css from "./style";
import Color from "../../common/Color";
import {connect} from "react-redux";
import {compose} from "redux";
import "@redux/CartRedux";
import PropTypes from "prop-types";
import {MyToast} from "../../containers";
import {toast} from "@app/Omni";

class TwoColumn extends PureComponent {

  static propTypes = {
    addCartItem: PropTypes.func
  };

  addToCart = (go = false) => {

    const {product, addCartItem, cartItems, onViewCartScreen} = this.props;

    if (cartItems.length < Constants.LimitAddToCart) {
      addCartItem(product, product.variation/*this.state.selectVariation*/);
      toast('Product added to cart');
    } else {
      alert(Languages.ProductLimitWaring);
    }
    if (go) onViewCartScreen();
  };

  render() {
    const {
      title,
      product,
      viewPost,
      store,
      cartItems,
      theme: {
        colors: {text, background},
      },
    } = this.props;
    const imageURI =
      typeof product.images[0] !== "undefined"
        ? getProductImage(product.images[0].src, Styles.width / 2)
        : Images.PlaceHolderURL;

    const isAddToCart = !!(
      cartItems &&
      cartItems.filter((item) => item.product.id === product.id).length > 0
    );

    let disabled = true;

    if (!isAddToCart && product.in_stock || product.stock_status === 'instock') disabled = false;

    return (
      <TouchableOpacity
        activeOpacity={0.9}
        style={[css.panelTwo, {backgroundColor: background}]}
        onPress={viewPost}>
        <ImageCache
          uri={imageURI}
          defaultSource={Images.imageHolder}
          style={css.imagePanelTwo}
        />
        <Text numberOfLines={3} style={[css.nameTwo, {color: text}]}>
          {title} {store && '- ' + store.toUpperCase()}
        </Text>


        <ProductPrice product={product}/>

        <WishListIcon product={product}/>

        <Rating rating={Number(product.average_rating)}/>
        <TouchableOpacity style={[css.addToCartBtn,
          !product.in_stock || !product.stock_status === 'instock' && {backgroundColor: Color.product.OutOfStockButton},disabled && {opacity: 0.3}]}
                          onPress={() => {
                            !disabled && this.addToCart(true);
                          }} activeOpacity={ disabled ? 0.2 : 0.9}
                          disabled={!product.in_stock || !product.stock_status === 'instock' || isAddToCart}>
          {product.in_stock || product.stock_status === 'instock'
            ? <Text style={css.addToCartText}>{Languages.AddtoCart}</Text>
            : <Text style={css.addToCartText}>{Languages.OutOfStock}</Text>
          }
        </TouchableOpacity>
      </TouchableOpacity>
    );
  }
}

export default withTheme(TwoColumn);
