/** @format */

"use strict";
import React, { Component } from "react";
import { Text, TouchableOpacity, Image } from "react-native";
import css from "./style";
import { Images, Styles, withTheme } from "@common";
import { ProductPrice, ImageCache, Rating, WishListIcon } from "@components";
import { getProductImage } from "@app/Omni";

class CardLayout extends Component {
  render() {
    const {
      product,
      title,
      viewPost,
      store,
      theme: {
        colors: { text, background},
      },
    } = this.props;

    const imageURI =
      typeof product.images[0] != "undefined"
        ? getProductImage(product.images[0].src, Styles.width)
        : Images.PlaceHolderURL;

    return (
      <TouchableOpacity
        activeOpacity={0.9}
        style={[css.panelCard, {backgroundColor: background}]}
        onPress={viewPost}>
        <ImageCache
          uri={imageURI}
          defaultSource={Images.imageHolder}
          style={css.imagePanelCard}
        />
        <Text numberOfLines={3} style={[css.nameCard, { color: text }]}>
          {title} {store && '- ' + store.toUpperCase()}
        </Text>
        <ProductPrice product={product} />
        <WishListIcon product={product} />
        <Rating rating={Number(product.average_rating)}/>
      </TouchableOpacity>
    );
  }
}

export default withTheme(CardLayout);
