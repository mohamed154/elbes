/** @format */

import React, { PureComponent } from "react";
import { View, Text } from "react-native";
import { Images, Styles } from "@common";
import { ImageCache, WishListIcon, TouchableScale } from "@components";
import { getProductImage, currencyFormatter } from "@app/Omni";
import { LinearGradient } from "@expo";
import css from "./style";

export default class BannerSale extends PureComponent {
  render() {
    const { viewPost, title, product } = this.props;
    const imageURI =
      typeof product.images[0] !== "undefined"
        ? getProductImage(product.images[0].src)
        : Images.PlaceHolderURL;
    const productPrice = `${currencyFormatter(product.price)} `;
    const productPriceSale = product.on_sale
      ? `${currencyFormatter(product.regular_price)} `
      : null;

    return (
      <TouchableScale onPress={viewPost} style={css.bannerSale}>
        <ImageCache
          uri={imageURI}
          defaultSource={Images.imageHolder}
          style={css.bannerImageSale}
        />
      </TouchableScale>
    );
  }
}
