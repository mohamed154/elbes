/** @format */

import React from "react";
import {
  Text,
  Image,
  Dimensions,
  StyleSheet,
  View,
  TouchableOpacity,
  ImageBackground,
  Linking
} from "react-native";
import {
  Languages,
  AppConfig,
  Constants,
  withTheme,
  Tools,
  Images,
  Color,
  Icons,
  Styles
} from "@common";
import {Rating, Button} from "@components";
import {isObject} from "lodash";
import {getProductImage, Icon} from "@app/Omni";
import {connect} from "react-redux";

const {width, height} = Dimensions.get("window");

@withTheme
class VendorInfo extends React.PureComponent {

  constructor(props) {
    super(props);
    this.handleVisitLink = this.handleVisitLink.bind(this);
  }

  componentWillMount() {
    this.props.fetchVendor(this.props.vendor.id)
  }

  renderRating = () => {
    const {
      vendor,
      theme: {
        colors: {text, lineColor},
      },
    } = this.props;

    if (vendor && typeof vendor.rating !== "undefined") {
      return (
        <View
          style={[
            styles.row,
            Constants.RTL && {flexDirection: "row-reverse"},
          ]}>
          <View style={[styles.lbContainer, {backgroundColor: lineColor}]}>
            <Text style={[styles.label, {color: text}]}>
              {Languages.storeRating}
            </Text>
          </View>

          <View style={[styles.lbRating]}>
            <Rating rating={Number(vendor.rating.rating)}/>
            <Text style={[{marginLeft: 8, color: text}]}>
              {`${vendor.rating.count} ${Languages.review}`}
            </Text>
          </View>
        </View>
      );
    }
  };

  getVendorAddress = () => {
    const {vendor} = this.props;

    let str = "";
    if (
      vendor &&
      typeof vendor.address !== "undefined" &&
      vendor.address != ""
    ) {
      if (!AppConfig.WooCommerce.useWCV && isObject(vendor.address)) {
        str = `${vendor.address.street_1}, ${vendor.address.city}, ${
          vendor.address.state
          }`;
      } else {
        str = vendor.address;
      }
    } else if (
      vendor &&
      typeof vendor.billing_address_1 !== "undefined" &&
      vendor.billing_address_1 != ""
    ) {
      str = vendor.billing_address_1;
    }
    // console.log([str, vendor]);
    return str;
  };

  addToWishList() {
    const {
      wishList,
      vendor,
      removeWishListShop,
      addWishListShop,
    } = this.props;
    const isInWishList =
      wishList.wishListShops.find((item) => item.vendor.id === vendor.id) !==
      undefined;

    if (isInWishList) {
      removeWishListShop(vendor);
    } else addWishListShop(vendor);
  }

  handleVisitLink = (link) => {
    Linking.canOpenURL(link).then(supported => {
      if (supported) {
        Linking.openURL(link);
      } else {
        console.log("Don't know how to open URI: " + this.props.url);
      }
    });
  };

  render() {
    const {
      vendor,
      wishList,
      list,
      vendorInfo,
      theme: {
        colors: {text, lineColor, link},
      },
      onChat
    } = this.props;

    const email =
      vendorInfo && vendorInfo.email
        ? vendorInfo.email
        : vendorInfo && vendorInfo.billing_email && vendorInfo.billing_email;
    const phone =
      vendorInfo && vendorInfo.phone
        ? vendorInfo.phone
        : vendorInfo && vendorInfo.billing_phone && vendorInfo.billing_phone;

    const facebook = vendorInfo && vendorInfo.social.fb && vendorInfo.social.fb;
    const instagram = vendorInfo && vendorInfo.social.instagram && vendorInfo.social.instagram;
    const youtube = vendorInfo && vendorInfo.social.youtube && vendorInfo.social.youtube;
    const twitter = vendorInfo && vendorInfo.social.twitter && vendorInfo.social.twitter;
    const googlePlus = vendorInfo && vendorInfo.social.gplus && vendorInfo.social.gplus;
    const pinterest = vendorInfo && vendorInfo.social.pinterest && vendorInfo.social.pinterest;
    const linkedIn = vendorInfo && vendorInfo.social.linkedin && vendorInfo.social.linkedin;
    const flickr = vendorInfo && vendorInfo.social.flickr && vendorInfo.social.flickr;

    const store = [
      // {name: Languages.storeName, value: Tools.getShopName(vendor)},
      {name: Languages.storeAddress + ": ", value: this.getVendorAddress()},
    ];

    // if (typeof email != "undefined" && email !== '') {
    //   store.push({
    //     name: Languages.storeEmail + ': ',
    //     value: email,
    //   });
    // }

    // if (typeof phone != "undefined" && phone !== '') {
    //   store.push({
    //     name: Languages.storePhone + ": ",
    //     value: phone,
    //   });
    // }

    if (typeof facebook != "undefined" && facebook !== '') {
      store.push({
        name: "Facebook",
        icon: "facebook-box",
        value: facebook,
        color: "#3b5998",
        link: true
      });
    }

    if (typeof instagram != "undefined" && instagram !== '') {
      store.push({
        name: "Instagram: ",
        value: instagram,
        icon: "instagram",
        color: "#833AB4",
        link: true
      });
    }

    if (typeof youtube != "undefined" && youtube !== '') {
      store.push({
        name: "Youtube: ",
        value: youtube,
        icon: "youtube",
        color: " #ff0000",
        link: true
      });
    }

    if (typeof twitter != "undefined" && twitter !== '') {
      store.push({
        name: "Twitter: ",
        value: twitter,
        icon: "twitter",
        color: "#1DA1F2",
        link: true
      });
    }

    if (typeof pinterest != "undefined" && pinterest !== '') {
      store.push({
        name: "Pinterest: ",
        value: pinterest,
        icon: "pinterest",
        color: "#BF0012",
        link: true
      });
    }

    if (typeof linkedIn != "undefined" && linkedIn !== '') {
      store.push({
        name: "LinkedIn: ",
        value: linkedIn,
        icon: "linkedin-box",
        color: "#0077B5",
        link: true
      });
    }

    if (typeof flickr != "undefined" && flickr !== '') {
      store.push({
        name: "Flickr: ",
        value: flickr,
        icon: "flickr",
        color: "#000",
        link: true
      });
    }

    const attributes = [];
    const socialMedia = [];

    for (let i = 0; i < store.length; i++) {

      if (!store[i].link) {
        attributes.push(
          <View key={i} style={{flexDirection: 'row'}}>
            <View>
              <Text style={{fontWeight: 'bold', paddingRight: 5}}>{store[i].name}</Text>
            </View>
            <View>
              <Text>
                {store[i].value}
              </Text>
            </View>
          </View>
        );
      }
      if (store[i].link) {
        socialMedia.push(
          <View key={i} style={{flexDirection: 'row'}}>
            <View style={{marginRight: 5, justifyContent: 'center'}}>
              <TouchableOpacity onPress={() => this.handleVisitLink(store[i].value)}>
                <Icon name={store[i].icon} size={30} color={store[i].color}/>
              </TouchableOpacity>
            </View>
          </View>
        )
      }
    }

    const bannerImg =
      typeof vendorInfo.banner != "undefined" && vendorInfo.banner != ""
        ? {uri: getProductImage(vendorInfo.banner, styles.width)}
        : typeof vendorInfo.pv_logo_image !== "undefined" &&
        vendorInfo.pv_logo_image != ""
        ? {uri: getProductImage(vendorInfo.pv_logo_image, Styles.width)}
        : Images.categoryPlaceholder;

    let isFavorite =
      wishList.wishListShops.filter((item) => item.vendor.id == vendorInfo.id) ==
      ""
        ? false
        : true;

    return (
      <View style={{padding: 10}}>
        <View style={{flexDirection: 'row', paddingTop: 10}}>

          <View style={{flex: 1, alignItems: 'center'}}>
            <Image source={bannerImg} style={{width: 75, height: 75, borderRadius: 75 / 2}}/>
          </View>
          <View style={{flex: 3}}>
            <View style={{flexDirection: 'row', justifyContent: 'space-around'}}>
              <View style={{alignItems: 'center'}}>
                <Text>{vendorInfo.rating.rating}</Text>
                <Text style={{fontSize: 10, color: 'grey'}}>rate</Text>
              </View>
              <View style={{alignItems: 'center'}}>
                <Text>100+</Text>
                <Text style={{fontSize: 10, color: 'grey'}}>Followers</Text>
              </View>
              <View style={{alignItems: 'center'}}>
                <Text>{list ? list.length : ""}</Text>
                <Text style={{fontSize: 10, color: 'grey'}}>Products</Text>
              </View>
            </View>
            <View style={{flexDirection: 'row', paddingTop: 10, justifyContent: 'center'}}>
              <TouchableOpacity style={[{
                flex: 3,
                marginLeft: 10,
                justifyContent: 'center',
                textAlign: 'center',
                height: 30,
                borderRadius: 15,
                borderWidth: 2,
                borderColor: Color.primary
              }, isFavorite && {backgroundColor: Color.primary}]} onPress={this.addToWishList.bind(this)}
              >
                {!isFavorite
                  ? <Text style={{textAlign: 'center'}}>
                    {/*<Icon name={Icons.MaterialCommunityIcons.WishlistEmpty} size={20} color={Color.primary}*/}
                    {/*style={{paddingRight: 10}}/>*/}
                    <Text style={{paddingLeft: 10}}>Favorite</Text>
                  </Text>
                  : <Text style={{textAlign: 'center'}}>
                    {/*<Text style={{paddingHorizontal: 10, marginHorizontal: 10}}>*/}
                    {/*<Icon name={Icons.MaterialCommunityIcons.Wishlist} size={20} color={"#fff"}/>*/}
                    {/*</Text>*/}
                    <Text style={{marginHorizontal: 10, paddingHorizontal: 10, color: "#fff"}}>Unfavorite</Text>
                  </Text>
                }
              </TouchableOpacity>
            </View>
          </View>

        </View>

        <View style={{padding: 10}}>
          <Text style={{fontWeight: 'bold', fontSize: 20}}>{Tools.getShopName(vendorInfo)}</Text>
          {attributes}
          <View style={{justifyContent: 'space-between', flexDirection: "row"}}>
            <View style={{flexDirection: 'row'}}>
              {socialMedia}
            </View>
            <View style={{justifyContent: 'flex-end'}}>
              <Button
                type="image"
                source={Images.icons.iconMessage}
                imageStyle={styles.imageButtonChat}
                buttonStyle={[styles.buttonChat]}
                onPress={() => onChat(vendor)}
              />
            </View>
          </View>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "rgba(255,255,255,1)",
    width: width - 40,
    margin: 20,
    borderRadius: 9,
    paddingBottom: 8,
  },
  row: {
    flexDirection: "row",
    minHeight: 40,
  },
  lbContainer: {
    flex: 0.3,
    justifyContent: "center",
    alignItems: "flex-end",
    paddingRight: 4,
    borderRightWidth: 0.5,
    borderRightColor: "#CED7DD",
    backgroundColor: "rgba(255,255,255,1)",
  },
  lbValue: {
    flex: 0.7,
    justifyContent: "center",
  },
  lbRating: {
    flex: 0.7,
    justifyContent: "flex-start",
    alignItems: "center",
    flexDirection: "row",
  },
  label: {
    margin: 5,
    fontSize: 14,
    fontWeight: "bold",
    textAlign: "center",
    opacity: 0.7,
  },
  value: {
    margin: 5,
    marginLeft: 10,
    color: "#5B5B5B",
    fontSize: 15,
  },
  valueLink: {
    margin: 5,
    marginLeft: 10,
    fontSize: 13,
  },
  emptyvendor: {
    height: 100,
    justifyContent: "center",
    alignItems: "center",
    padding: 10,
    backgroundColor: "rgba(255,255,255,1)",
  },

  bannerImage: {
    width: width - 40,
    resizeMode: "cover",
    marginBottom: 10,
    borderRadius: 6,
    flex: 1,
    height: (16 * height) / 100,
  },
  buttonChat: {
    position: "absolute",
    bottom: 35,
    right: 5,
    zIndex: 99999,
    width: 50,
    height: 50,
    backgroundColor: 'transparent',
  },
  imageButtonChat: {
    width: 32,
    height: 32,
    resizeMode: "contain",
    right: 5,
    zIndex: 999,
  },
});

const mapStateToProps = ({wishList, vendor}, ownProps) => {
  const list = vendor.products;
  const vendorInfo = vendor.vendor;

  return ({
    ...ownProps,
    wishList,
    list,
    vendorInfo
  });
};

function mergeProps(stateProps, dispatchProps, ownProps) {
  const {netInfo} = stateProps;
  const {dispatch} = dispatchProps;
  const WishListRedux = require("@redux/WishListRedux");
  const Vendor = require("@redux/VendorRedux");
  return {
    ...ownProps,
    ...stateProps,
    fetchProducts: (vendorId, per_page, page) => {
      return Vendor.actions.fetchProductsByVendor(
        dispatch,
        vendorId,
        per_page,
        page
      );
    },
    fetchVendor: (vendorId) => {
      return Vendor.actions.fetchVendor(dispatch, vendorId);
    },
    addWishListShop: (vendor) => {
      WishListRedux.actions.addWishListShop(dispatch, vendor);
    },
    removeWishListShop: (vendor) => {
      WishListRedux.actions.removeWishListShop(dispatch, vendor);
    },
  };
}

export default connect(
  mapStateToProps,
  null,
  mergeProps
)(VendorInfo);
