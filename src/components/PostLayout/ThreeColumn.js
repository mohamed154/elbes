/** @format */

import React, { PureComponent } from "react";
import PropTypes from "prop-types";
import { Text, TouchableOpacity, I18nManager, View } from "react-native";
import TimeAgo from "@custom/react-native-timeago";
import { WishListIcon, ImageCache, ProductPrice, Rating } from "@components";
import css from "./style";
import {withTheme, Constants} from '@common'
import Color from "../../common/Color";
import {toast} from "../../Omni";

class ThreeColumn extends PureComponent {
  static propTypes = {
    post: PropTypes.object,
    title: PropTypes.string,
    type: PropTypes.string,
    imageURL: PropTypes.string,
    date: PropTypes.any,
    viewPost: PropTypes.func,
  };

  addToCart = (go = false) => {

    const {post, addCartItem, cartItems, onViewCartScreen} = this.props;

    if (cartItems.length < Constants.LimitAddToCart) {
      addCartItem(post, post.variation/*this.state.selectVariation*/);
      toast('Product added to cart');
    } else {
      alert(Languages.ProductLimitWaring);
    }
    if (go) onViewCartScreen();
  };

  render() {
    const { viewPost, title, post, type, imageURL, date, cartItems } = this.props;
    const {
      theme:{
        colors:{
          background, text
        }
      }
    } = this.props;

    const isAddToCart = !!(
      cartItems &&
      cartItems.filter((item) => item.product.id === post.id).length > 0
    );

    let disabled = true;

    if (!isAddToCart && post.in_stock) disabled = false;

    return (
      <TouchableOpacity
        activeOpacity={0.9}
        style={css.panelThree}
        onPress={viewPost}>
        <ImageCache uri={imageURL} style={css.imagePanelThree} />

        <Text numberOfLines={1} ellipsizeMode='tail' style={[css.nameThree, {color: text}]}>{title}</Text>
        {typeof type !== "undefined" && (
          <Text style={css.timeThree}>
            <TimeAgo time={date} />{" "}
          </Text>
        )}
        {typeof type === "undefined" && (
          <ProductPrice product={post} hideDisCount />
        )}
        {typeof type === "undefined" && (
          <WishListIcon
            product={post}
            style={I18nManager.isRTL ? { left: 10 } : { right: 10 }}
          />
        )}
        {typeof type === "undefined" && (
          <Rating rating={post.average_rating} />
        )}
        {/*<TouchableOpacity style={[css.addToCartBtnPanelTree,*/}
          {/*!post.in_stock && {backgroundColor: Color.product.OutOfStockButton},disabled && {opacity: 0.3}]}*/}
                          {/*onPress={() => {*/}
                            {/*!disabled && this.addToCart(true);*/}
                          {/*}} activeOpacity={ disabled ? 0.2 : 0.9}*/}
                          {/*disabled={!post.in_stock || isAddToCart}>*/}
          {/*{post.in_stock*/}
            {/*? <Text style={css.addToCartText}>ADD TO CART</Text>*/}
            {/*: <Text style={css.addToCartText}>OUT OF STOCK</Text>*/}
          {/*}*/}
        {/*</TouchableOpacity>*/}
      </TouchableOpacity>
    );
  }
}

export default withTheme(ThreeColumn)
