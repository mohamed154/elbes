/** @format */

import React, { PureComponent } from "react";
import PropTypes from "prop-types";
import { Text, TouchableOpacity, I18nManager } from "react-native";
import TimeAgo from "react-native-timeago";
import { WishListIcon, ImageCache, ProductPrice, Rating } from "@components";
import css from "./style";
import {withTheme, Constants} from '@common'
import Color from "../../common/Color";
import {toast} from "../../Omni";

@withTheme
export default class CardLayout extends PureComponent {
  static propTypes = {
    post: PropTypes.object,
    title: PropTypes.string,
    type: PropTypes.string,
    imageURL: PropTypes.string,
    date: PropTypes.any,
    viewPost: PropTypes.func,
  };

  addToCart = (go = false) => {

    const {post, addCartItem, cartItems, onViewCartScreen} = this.props;

    if (cartItems.length < Constants.LimitAddToCart) {
      addCartItem(post, post.variation/*this.state.selectVariation*/);
      toast('Product added to cart');
    } else {
      alert(Languages.ProductLimitWaring);
    }
    if (go) onViewCartScreen();
  };

  render() {
    const { post, title, type, imageURL, date, viewPost, cartItems } = this.props;
    const {
      theme:{
        colors:{
          background, text
        }
      }
    } = this.props

    const wishIcon = {
      bottom: 20,
      right: 20,
    };
    const wishIconRTL = {
      top: 17,
      left: 17,
    };

    const isAddToCart = !!(
      cartItems &&
      cartItems.filter((item) => item.product.id === post.id).length > 0
    );

    let disabled = true;

    if (!isAddToCart && post.in_stock) disabled = false;

    return (
      <TouchableOpacity
        activeOpacity={0.9}
        style={css.panelOne}
        onPress={viewPost}>
        <ImageCache uri={imageURL} style={css.imagePanelOne} />
        <Text style={[css.nameOne, {color: text}]}>{title}</Text>

        {typeof type !== "undefined" && (
          <Text style={[css.timeOne, { textAlign: "center" }]}>
            <TimeAgo time={date} />
          </Text>
        )}
        {typeof type === "undefined" && (
          <ProductPrice product={post} hideDisCount />
        )}
        {typeof type === "undefined" && (
          <WishListIcon
            product={post}
            style={I18nManager.isRTL ? wishIconRTL : wishIcon}
          />
        )}
        {typeof type === "undefined" && <Rating rating={post.average_rating} />}
        <TouchableOpacity style={[css.addToCartBtn,
          !post.in_stock && {backgroundColor: Color.product.OutOfStockButton},disabled && {opacity: 0.3}]}
                          onPress={() => {
                            !disabled && this.addToCart(true);
                          }} activeOpacity={ disabled ? 0.2 : 0.9}
                          disabled={!post.in_stock || isAddToCart}>
          {post.in_stock
            ? <Text style={css.addToCartText}>{Languages.AddtoCart}</Text>
            : <Text style={css.addToCartText}>{Languages.OutOfStock}</Text>
          }
        </TouchableOpacity>
      </TouchableOpacity>
    );
  }
}
