/** @format */

import { StyleSheet, Platform, Dimensions } from 'react-native'
import { Color, Styles, Device, Config, Constants } from '@common'
const { width } = Dimensions.get('window')

export default StyleSheet.create({
  body: {
    ...Platform.select({
      ios: {
        zIndex: 9,
      },
    }),
    position: 'absolute',
    top: 0,
    left: 0,
    flexDirection: 'row',
    width,
    paddingVertical: 10,
    height: 60,
  },
  headerImage: {
    left: 0,
    top: 20,
    width: width / 3,
    resizeMode: 'contain',
    position: 'absolute',
    ...Platform.select({
      android: {
        top: 42,
      },
    }),
  },
  homeMenu: {
    width: width / 3,
    position: 'absolute',
    top: 30,
    justifyContent: 'center',
    alignItems: 'center',
    left: width / 3,
  },
  headerRight: {
    position: 'absolute',
    zIndex: 9,
    top: 15,
    right: 10,
    ...Platform.select({
      android: {
        top: 42,
      },
    }),
  },
})
