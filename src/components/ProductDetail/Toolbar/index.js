/** @format */

import React, { Component } from 'react'
import { View, Image, TouchableOpacity, I18nManager } from 'react-native'
import { Images, Styles } from '@common'
import styles from './styles'
import { Logo, CartWishListIcons } from '@navigation/IconNav'
export default class ProductDetailToolbar extends Component {
  render() {
    const { navigation, onBack } = this.props
    const hitSlop = { top: 20, right: 20, bottom: 20, left: 20 }
    return (
      <View style={styles.body}>
        <View style={styles.headerView} />
        <View style={[styles.headerImage]}>
          <TouchableOpacity hitSlop={hitSlop} onPress={onBack}>
            <Image
              source={Images.icons.LongBack}
              style={[
                Styles.Common.toolbarIcon,
                I18nManager.isRTL && {
                  transform: [{ rotate: '180deg' }],
                },
              ]}
            />
          </TouchableOpacity>
        </View>
        <View style={styles.homeMenu}>{Logo()}</View>
        <View style={[styles.headerRight]}>
          {CartWishListIcons(navigation)}
        </View>
      </View>
    )
  }
}
