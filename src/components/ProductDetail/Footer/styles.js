/** @format */
import { StyleSheet, Platform, Dimensions } from 'react-native'
import { Constants, Color } from '@common'
const { width, height } = Dimensions.get('window')
const vw = width / 100
const vh = height / 100

export default StyleSheet.create({
  //bottom Buttons
  bottomView: {
    height: 45,
    width,
    zIndex: 999,
    position: 'absolute',
    left: 0,
    bottom: 0,
    flexDirection: 'row',
    borderTopWidth: 0.5,
    borderTopColor: 'rgba(0, 0, 0, .2)',
    backgroundColor: '#FFF',
    shadowColor: '#000',
    shadowOpacity: 0.2,
    shadowRadius: 3,
    shadowOffset: { width: 0, height: -1 },
  },
  buttonContainer: {
    flex: 1,
    backgroundColor: '#FFF',
    flexDirection: 'row',
    alignItems: 'center',
  },

  buttonAction: {
    width: 170,
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    paddingRight: 10,
    ...Platform.select({
      android: {
        paddingVertical: 12,
      },
    }),
  },
  imageButton: {
    width: 18,
    height: 18,
    tintColor: '#ccc',
    flex: 1,
  },
  buttonStyle: {
    flex: 1 / 3,
    justifyContent: 'center',
    alignItems: 'center',
  },
  btnBuy: {
    width: 110,
    flexDirection: 'row',
    paddingHorizontal: 10,
    backgroundColor: Color.BuyNowButton,
    alignItems: 'center',
    justifyContent: 'center',
  },
  outOfStock: {
    backgroundColor: Color.OutOfStockButton,
  },
  btnBuyText: {
    color: 'white',
    fontSize: 14,
    fontWeight: 'bold',
    fontFamily: Constants.fontHeader,
  },
  withChat: {
    paddingLeft: 10,
  },
  chatText: {
    color: '#ccc',
  },
})
