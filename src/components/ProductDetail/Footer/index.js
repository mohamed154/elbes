/** @format */

import React from 'react'
import {View, Text, Share, TouchableOpacity, Image} from 'react-native'
import {Constants, Languages} from '@common'
import {warn} from '@app/Omni'
import {Button} from '@components'
import {connect} from 'react-redux'
import styles from './styles'

class ProductDetailFooter extends React.PureComponent {
  constructor(props) {
    super(props);

    this.inCartTotal = 0;
    this.isInWishList = false;
    this.state = {
      selectVariation: null,
    }
  }

  share = () => {
    const {product} = this.props
    Share.share({
      message: product.description.replace(/(<([^>]+)>)/gi, ''),
      url: product.permalink,
      title: product.name,
    })
  }

  addToCart = (go = false) => {
    const {addCartItem, product, onViewCart} = this.props

    if (this.inCartTotal < Constants.LimitAddToCart) {
      addCartItem(product, this.state.selectVariation)
    } else {
      alert(Languages.ProductLimitWaring)
    }
    if (go) onViewCart()
  }

  addToWishList = (isAddWishList) => {
    if (isAddWishList) {
      this.props.removeWishListItem(this.props.product)
    } else this.props.addWishListItem(this.props.product)
  }

  render() {
    const {
      product,
      wishListItems,
      cartItems,
      onChat,
      user,
      author,
    } = this.props

    const isAddWishList =
      wishListItems.filter((item) => item.product.id === product.id).length > 0
    const isAddToCart = !!(
      cartItems &&
      cartItems.filter((item) => item.product.id === product.id).length > 0
    )
    const enableChat = author != null && user != null && author.id != user.id
    const hitSlop = {top: 15, right: 15, left: 15, bottom: 15}
    return (
      <View
        style={[
          styles.bottomView,
          Constants.RTL && {flexDirection: 'row-reverse'},
        ]}>
        <View style={styles.buttonContainer}>
          {enableChat && (
            <TouchableOpacity
              activeOpacity={0.8}
              hitSlop={hitSlop}
              style={styles.withChat}
              onPress={() => onChat(author)}>
              <Text style={styles.chatText}>{Languages.chatWithUs}</Text>
            </TouchableOpacity>
          )}
          <View style={styles.buttonAction}>
            <Button
              type="image"
              source={require('@images/icons/icon-share.png')}
              imageStyle={styles.imageButton}
              buttonStyle={styles.buttonStyle}
              onPress={this.share}
            />
            <Button
              type="image"
              isAddWishList={isAddWishList}
              source={require('@images/icons/icon-love.png')}
              imageStyle={styles.imageButton}
              buttonStyle={styles.buttonStyle}
              onPress={() => this.addToWishList(isAddWishList)}
            />
            <Button
              type="image"
              isAddToCart={isAddToCart}
              source={require('@images/icons/icon-cart.png')}
              imageStyle={styles.imageButton}
              disabled={!product.in_stock}
              buttonStyle={styles.buttonStyle}
              onPress={() => product.in_stock && this.addToCart()}
            />
          </View>
        </View>

        <Button
          text={product.in_stock ? Languages.BUYNOW : Languages.OutOfStock}
          style={[styles.btnBuy, !product.in_stock && styles.outOfStock]}
          textStyle={styles.btnBuyText}
          disabled={!product.in_stock}
          onPress={() => {
            product.in_stock && this.addToCart(true)
          }}
        />
      </View>
    )
  }
}

const mapStateToProps = ({user, wishList, carts}, ownProps) => {
  return {
    user: user.user,
    wishListItems: wishList.wishListItems,
    cartItems: carts.cartItems,
    ...ownProps,
  }
}

function mergeProps(stateProps, dispatchProps, ownProps) {
  const {dispatch} = dispatchProps
  const CartRedux = require('@redux/CartRedux')
  const WishListRedux = require('@redux/WishListRedux')

  return {
    ...ownProps,
    ...stateProps,
    addCartItem: (product, variation) => {
      CartRedux.actions.addCartItem(dispatch, product, variation)
    },
    addWishListItem: (product) => {
      WishListRedux.actions.addWishListItem(dispatch, product)
    },
    removeWishListItem: (product) => {
      WishListRedux.actions.removeWishListItem(dispatch, product)
    },
  }
}

export default connect(
  mapStateToProps,
  undefined,
  mergeProps
)(ProductDetailFooter)
