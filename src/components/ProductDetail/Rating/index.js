/** @format */

import React, { Component } from 'react'
import { View, TouchableOpacity, Text } from 'react-native'
import styles from './styles'
import { Events, Languages, Color } from '@common'
import { Rating } from '@components'

export default class ProductDetailRating extends Component {
  _writeReview = () => {
    const { product } = this.props
    Events.openModalReview(product)
  }

  render() {
    const { product } = this.props

    return product.rating_count != 0 ? (
      <View style={styles.price_wrapper}>
        <Rating rating={product.average_rating} size={19} />
        <Text style={[styles.textRating, { color: Color.blackTextDisable }]}>
          {`(${product.rating_count})`}
        </Text>
        <TouchableOpacity onPress={this._writeReview}>
          <Text style={[styles.textRating, { color: Color.blackTextDisable }]}>
            {Languages.writeReview}
          </Text>
        </TouchableOpacity>
      </View>
    ) : (
      <View />
    )
  }
}
