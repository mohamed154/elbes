/** @format */

import { StyleSheet, Platform, Dimensions } from 'react-native'
import { Color, Styles, Device, Config, Constants } from '@common'
const { width } = Dimensions.get('window')

export default StyleSheet.create({
  price_wrapper: {
    flexDirection: 'row',
    marginBottom: 8,
  },
  textRating: {
    fontSize: Styles.FontSize.small,
    marginLeft: 10,
  },
})
