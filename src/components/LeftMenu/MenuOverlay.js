/** @format */

import React, { PureComponent } from "react";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import SideMenu from "react-native-drawer";
import { Drawer } from "@components";
import UserProfile from "../../containers/UserProfile";

class MenuOverlay extends PureComponent {
  static propTypes = {
    goToScreen: PropTypes.func,
    routes: PropTypes.object,
    isOpenMenu: PropTypes.bool.isRequired,
  };

  toggleMenu = (isOpen) => {
    if (!isOpen) {
      this.props.toggleMenu(isOpen);
    }
  };

  render() {
    const { isOpenMenu } = this.props;

    return (
      <SideMenu
        ref={(_drawer) => (this.drawer = _drawer)}
        type="overlay"
        tapToClose
        open={isOpenMenu}
        onClose={() => this.toggleMenu(false)}
        panCloseMask={0.3}
        panThreshold={0.3}
        openDrawerOffset={0.2}
        styles={{drawer: { shadowColor: '#000000', shadowOpacity: 0.8, shadowRadius: 3},}}
        useInteractionManager
        // content={<Drawer goToScreen={this.props.goToScreen} />}
        content={<UserProfile navigation={this.props.goToScreen} />}
        tweenHandler={(ratio) => ({
          main: { opacity:(2-ratio)/2 }
        })}>
        {this.props.routes}
      </SideMenu>
    );
  }
}

const mapStateToProps = ({ sideMenu }) => ({
  isOpenMenu: sideMenu.isOpen,
});

const mergeProps = (stateProps, dispatchProps, ownProps) => {
  const { dispatch } = dispatchProps;
  const { actions: sideMenuActions } = require("@redux/SideMenuRedux");
  return {
    ...ownProps,
    ...stateProps,
    toggleMenu: (isOpen) => dispatch(sideMenuActions.toggleMenu(isOpen)),
  };
};
export default connect(
  mapStateToProps,
  null,
  mergeProps
)(MenuOverlay);
