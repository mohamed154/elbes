/** @format */

import React, { PureComponent } from "react";
import { View, Modal } from "react-native";
import ProgressBarAnimated from "react-native-progress-bar-animated";
import styles from "./style";

class ProcessModal extends PureComponent {
  static defaultProps = {
    progress: 0,
  };

  state = {
    modalVisible: false,
  };

  show = () => {
    this.setState({ modalVisible: true });
  };

  hide = (callback) => {
    this.setState({ modalVisible: false }, callback);
  };

  render() {
    const { progress } = this.props;

    return (
      <Modal
        animationType="fade"
        transparent
        visible={this.state.modalVisible}
        onRequestClose={this.hide}>
        <View style={styles.background}>
          <ProgressBarAnimated
            width={250}
            maxValue={100}
            value={progress * 100}
            backgroundColorOnComplete="#6CC644"
          />
        </View>
      </Modal>
    );
  }
}

export default ProcessModal;
