/** @format */

import React, { PureComponent } from "react";

import { FlatList, Dimensions } from "react-native";
import { withTheme } from "@common";
import Item from "./Item";
const { width } = Dimensions.get("window");
class Promotions extends PureComponent {
  static defaultProps = {
    items: [],
  };

  _getItemLayout = (data, index) => {
    return { length: width, offset: width * index, index };
  };

  render() {
    const { items } = this.props;
    return (
      <FlatList
        keyExtractor={(item, index) => `${index}`}
        horizontal={true}
        data={items}
        getItemLayout={this._getItemLayout}
        showsHorizontalScrollIndicator={false}
        renderItem={({ item }) => <Item item={item} />}
      />
    );
  }
}

export default withTheme(Promotions);
