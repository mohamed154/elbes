/** @format */

import React, {
  StyleSheet,
  Platform,
  Dimensions,
  PixelRatio,
  I18nManager,
} from "react-native";
import { Color, Constants, Styles } from "@common";

const { width, height, scale } = Dimensions.get("window"),
  vw = width / 100,
  vh = height / 100,
  vmin = Math.min(vw, vh),
  vmax = Math.max(vw, vh);

export default StyleSheet.create({
  bannerViewShadow: {
    width: width - 42,
    marginTop: 20,
    marginBottom: 20,
    marginLeft: 15,
    borderRadius: 3,
    height: (height * 17) / 100,

    shadowOffset: { width: 0, height: 5 },
    shadowColor: "#000",
    backgroundColor: "#fff",
    shadowOpacity: 0.05,
    shadowRadius: 4,
    elevation: 3,
  },

  bannerView: {
    width: width - 20,
    overflow: "hidden",
  },

  bannerImage: {
    width: width - 42,
    borderRadius: 3,
    height: (height * 17) / 100,
    resizeMode: "contain",
  },
  bannerOverlay: {
    width: width - 42,
    alignItems: I18nManager.isRTL ? "flex-end" : "flex-start",
    height: (height * 10) / 100,
    justifyContent: I18nManager.isRTL ? "flex-end" : "flex-start",
    position: "absolute",
    bottom: 0,
  },
});
