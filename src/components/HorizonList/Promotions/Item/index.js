/** @format */

import React, { PureComponent } from "react";
import { View, Image } from "react-native";
import { WebBrowser } from "@expo";
import { Images } from "@common";
import { TouchableScale } from "@components";
import css from "./styles";

export default class Item extends PureComponent {
  _onPress = () => {
    WebBrowser.openBrowserAsync(this.props.item.link);
  };

  render() {
    const { item } = this.props;

    return (
      <TouchableScale
        key={"post" + item}
        onPress={this._onPress}
        style={css.bannerView}>
        <View activeOpacity={1} style={css.bannerViewShadow}>
          <Image
            source={item.image}
            defaultSource={Images.imageHolder}
            style={css.bannerImage}
          />
        </View>
      </TouchableScale>
    );
  }
}
