/** @format */

import React, {PureComponent} from "react";
import PropTypes from "prop-types";
import {
  FlatList,
  Text,
  TouchableOpacity,
  View,
  I18nManager,
  ScrollView,
  Dimensions
} from "react-native";
import {Constants, Images, Languages, AppConfig, withTheme} from "@common";
import Icon from "react-native-vector-icons/Entypo";
import {HorizonLayout} from "@components";
import {find} from "lodash";
import styles from "./styles";
import {connect} from "react-redux";
import Categories from "./Categories";
import Promotions from "./Promotions";

const { width } = Dimensions.get('window');

class HorizonList extends PureComponent {
  static propTypes = {
    config: PropTypes.object,
    index: PropTypes.number,
    fetchPost: PropTypes.func,
    onShowAll: PropTypes.func,
    list: PropTypes.array,
    fetchProductsByCollections: PropTypes.func,
    setSelectedCategory: PropTypes.func,
    onViewProductScreen: PropTypes.func,
    showCategoriesScreen: PropTypes.func,
    collection: PropTypes.object,
    addCartItem: PropTypes.func
  };

  constructor(props) {
    super(props);

    this.page = 1;
    this.limit = Constants.pagingLimit;
    this.defaultList = [
      {
        id: 1,
        name: Languages.loading,
        images: [Images.PlaceHolder],
      },
      {
        id: 2,
        name: Languages.loading,
        images: [Images.PlaceHolder],
      },
      {
        id: 3,
        name: Languages.loading,
        images: [Images.PlaceHolder],
      },
    ];

    this.state = {sliderIndex: 0, reversed: false}
  }

  /**
   * handle load more
   */
  _nextPosts = () => {
    const {config, index, fetchPost, collection} = this.props;
    this.page += 1;
    if (!collection.finish) {
      fetchPost({config, index, page: this.page});
    }
  };

  _viewAll = () => {
    const {
      config,
      onShowAll,
      index,
      list,
      fetchProductsByCollections,
      setSelectedCategory,
    } = this.props;
    const selectedCategory = find(
      list,
      (category) => category.id === config.category
    );
    setSelectedCategory(selectedCategory);
    fetchProductsByCollections(config.category, config.tag, this.page, index);
    onShowAll(config, index);
  };

  showProductsByCategory = (config) => {
    const {
      // onShowAll,
      // index,
      list,
      // fetchProductsByCollections,
      setSelectedCategory,
      onViewCategory
    } = this.props;
    const selectedCategory = find(
      list,
      (category) => category.id === config.category
    );
    setSelectedCategory(selectedCategory);
    // fetchProductsByCollections(config.category, config.tag, this.page, index);
    // onShowAll(config, index);
    onViewCategory(config);
  };

  onViewProductScreen = (product, type) => {
    this.props.onViewProductScreen({product, type});
  };

  renderItem = ({item, index}) => {
    const {layout} = this.props.config;
    const {addCartItem, cartItems, onViewCartScreen} = this.props;

    if (item === null) return <View key="post_"/>;
    return (
      <HorizonLayout
        product={item}
        key={`post-${index}`}
        onViewPost={() => this.onViewProductScreen(item, index)}
        layout={layout}
        addCartItem={addCartItem}
        cartItems={cartItems}
        onViewCartScreen={onViewCartScreen}
      />
    );
  };

  setRef = (c) => {
    this.listRef = c;
  };

  scrollToIndex = (index, animated) => {
    this.listRef && this.listRef.scrollToIndex({index, animated})
  };

  componentDidMount() {
    const {collection} = this.props;
    const {layout} = this.props.config;
    if (layout === 10) {
      const that = this;
      // setInterval(() => {
      //   const list =
      //     typeof collection.list !== "undefined" && collection.list.length !== 0
      //       ? config.tag === 71 ? collection.list.reverse() : collection.list
      //       : this.defaultList;
      //   // var sliderIndex = this.state.sliderIndex;
      //   // var nextIndex = 0;
      //   // if (sliderIndex < list.length-1) {
      //   //   nextIndex = sliderIndex + 1;
      //   // }
      //   // this.listRef && this.listRef.scrollToIndex({index: nextIndex, animated: true});
      //   // that.setState({sliderIndex: nextIndex})
      // }, 3000);
    }
  }

  render() {
    const {
      showCategoriesScreen,
      collection,
      config,
      theme: {
        colors: {link, text},
      },
    } = this.props;

    const sortByDateCreated = (p1, p2) => Date.parse(p2.date_created) - Date.parse(p1.date_created)

    const list =
      typeof collection.list !== "undefined" && collection.list.length !== 0
        ? config.tag === 71 ? collection.list.sort(sortByDateCreated) : collection.list
        : this.defaultList;
    const isPaging = !!config.paging;

    const renderHeader = () => (
      <View style={styles.header}>
        <View style={styles.headerLeft}>
          <Text style={[styles.tagHeader, {color: text}]}>
            {this.props.lang === 'ar' ? config.nameAR : config.name}
          </Text>
        </View>
        {!config.hideShowAll && (
          <TouchableOpacity
            onPress={
              config.layout != Constants.Layout.circle
                ? this._viewAll
                : showCategoriesScreen
            }
            style={styles.headerRight}>
            <Text style={[styles.headerRightText, {color: link}]}>
              {Languages.seeAll}
            </Text>
            <Icon
              style={styles.icon}
              color={AppConfig.MainColor}
              size={20}
              name={
                I18nManager.isRTL ? "chevron-small-left" : "chevron-small-right"
              }
            />
          </TouchableOpacity>
        )}
      </View>
    );

    const renderContent = () => {
      const {config} = this.props;
      switch (config.layout) {
        case Constants.Layout.circle:
          // return (
          //   <Categories
          //     categories={this.props.list}
          //     items={config.items}
          //     onPress={this.showProductsByCategory}
          //   />
          // );
          return (
            <View></View>
          );
        default:
          return (
            <ScrollView>
              <FlatList
                ref={this.setRef}
                contentContainerStyle={styles.flatlist}
                data={list}
                keyExtractor={(item) => `post__${item.id}`}
                renderItem={this.renderItem}
                showsHorizontalScrollIndicator={false}
                horizontal
                pagingEnabled={isPaging}
                onEndReached={false && this._nextPosts}
                onMomentumScrollEnd={(event) => {
                  let sliderIndex = event.nativeEvent.contentOffset.x ? event.nativeEvent.contentOffset.x / width : 0;
                  this.setState({sliderIndex})
                }}
                onScrollToIndexFailed={() => {
                }}
              />
            </ScrollView>
          );
      }
    };

    return (
      <View
        style={[
          styles.flatWrap,
          config.color && {
            backgroundColor: config.color,
          },
        ]}>
        {config.name && renderHeader()}
        {renderContent()}
      </View>
    );
  }
}

// export default withTheme(HorizonList);

const mapStateToProps = ({language}, ownProps) => {
  return {
    ...ownProps,
    lang: language.lang,
  }
};

const mergeProps = (stateProps, dispatchProps, ownProps) => {
  const {dispatch} = dispatchProps;
  const LangRedux = require("@redux/LangRedux");
  return {
    ...ownProps,
    ...stateProps,
    switchLanguage: (language) => LangRedux.actions.switchLanguage(dispatch, language),
  };
};

export default connect(
  mapStateToProps,
  null,
  mergeProps
)(withTheme(HorizonList));
