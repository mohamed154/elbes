/** @format */

import React, { PureComponent } from 'react'
import { View, TextInput, TouchableOpacity, Image } from 'react-native'
import { Icons, Languages, withTheme } from '@common'
import styles from './styles'
import Icon from "react-native-vector-icons/Ionicons";

@withTheme
export default class SearchChat extends PureComponent {
  _onSearch = (text) => {}

  render() {
    const { placeholder, style } = this.props
    const {
      theme: {
        colors: {text, lineColor },
      },
    } = this.props;

    return (
      <View style={[styles.container, style, {backgroundColor: lineColor }]}>
        <View style={styles.searchWrap}>
          <Icon name={Icons.Ionicons.Search} size={20} color={text} />

          <TextInput
            style={[styles.input, {color: text}]}
            autoCapitalize={'none'}
            autoCorrect={false}
            placeholder={placeholder ? placeholder : Languages.search + '...'}
            placeholderTextColor="#999"
            underlineColorAndroid={'transparent'}
            clearButtonMode={'while-editing'}
            // value={searchText}
            onChangeText={this._onSearch}
            // onSubmitEditing={this._onSearch}
          />
        </View>
      </View>
    )
  }
}
