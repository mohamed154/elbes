/** @format */

import React, { PureComponent } from 'react'
import { View, Text, TouchableOpacity, Image, StyleSheet } from 'react-native'
import { Images, withTheme } from '@common'
import styles from './styles'

@withTheme
export default class ChatToolBar extends PureComponent {
  render() {
    const { label, onBack } = this.props
    const {
      theme: {
        colors: {text, lineColor },
      },
    } = this.props;

    return (
      <View style={[styles.navbar, {backgroundColor: lineColor}]}>
        <TouchableOpacity onPress={onBack} style={styles.arrowBack}>
          <Image
            source={Images.icons.LongBack}
            style={[styles.image, {tintColor:text} ]}
            
          />
        </TouchableOpacity>
        <Text style={[styles.label, {color: text}]}>{label}</Text>
        {/*<View style={styles.right}>*/}
          {/*<TouchableOpacity onPress={this._call}>*/}
            {/*<Image source={Images.icons.call} style={styles.call} />*/}
          {/*</TouchableOpacity>*/}
          {/*<TouchableOpacity onPress={this._call}>*/}
            {/*<Image source={Images.icons.videocall} style={styles.vcall} />*/}
          {/*</TouchableOpacity>*/}
        {/*</View>*/}
        <View style={[StyleSheet.absoluteFill]} />
      </View>
    )
  }
}
