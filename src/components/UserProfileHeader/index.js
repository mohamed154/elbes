/** @format */

import React, {PureComponent} from "react";
import PropTypes from "prop-types";
import {View, Text, TouchableOpacity, Image, ImageBackground, Dimensions, I18nManager } from "react-native";
import {Languages, Tools, withTheme, Images} from "@common";
import styles from "./styles";

class UserProfileHeader extends PureComponent {
  //   constructor(props) {
  //     super(props)
  //   }
  static propTypes = {
    onLogin: PropTypes.func.isRequired,
    onLogout: PropTypes.func.isRequired,
    user: PropTypes.object,
  };

  loginHandle = () => {
    if (this.props.user.name === Languages.Guest) {
      this.props.onLogin();
    } else {
      this.props.onLogout();
    }
  };

  render() {
    const {user} = this.props;
    const avatar = Tools.getAvatar(user);
    const {
      theme: {
        colors: {background, text},
      },
    } = this.props;

    return (
      <View style={[styles.container, {backgroundColor: background}]}>
        <ImageBackground source={Images.profileHeaderBack} style={{width: '100%', height: '100%'}}>
          <View style={styles.header}>
            <Image source={avatar} style={styles.avatar}/>
            <View style={styles.textContainer}>
              <Text style={[styles.fullName, {color: text}, I18nManager.isRTL && {textAlign: 'left'}]}>{user.name}</Text>
              <Text style={[styles.address, {color: text}, I18nManager.isRTL && {textAlign: 'left'}]}>
                {user ? user.address : ""}
              </Text>

              <TouchableOpacity
                onPress={this.loginHandle}
                hitSlop={{top: 20, bottom: 20, left: 20, right: 20}}>
                <Text style={[styles.loginText, I18nManager.isRTL && {textAlign: 'left'}]}>
                  {user.name === Languages.Guest
                    ? Languages.Login
                    : Languages.Logout}
                </Text>
              </TouchableOpacity>
            </View>
          </View>
        </ImageBackground>
      </View>
    );
  }
}

export default withTheme(UserProfileHeader);
