/** @format */

import Button from "./Button/Button";
import ButtonIndex from "./Button";
import ProductSize from "./ProductSize";
import ProductColor from "./ProductColor";
import TextInput from "./TextInput";
import ShippingMethod from "./ShippingMethod";
import TabBarIcon from "./TabBarIcon";
import ToolbarIcon from "./ToolbarIcon";
import NavigationBarIcon from "./NavigationBarIcon";
import CartIcon from "./CartIcon";
import CartIcons from "./CartIcons";
import TabBar from "./TabBar";
import Search from "./Search";
import Rating from "./Rating";
import PostBanner from "./PostBanner";
import PostList from "./PostList";
import FlatButton from "./FlatButton";
import ShopButton from "./ShopButton";
import PostLayout from "./PostLayout";
import LogoSpinner from "./LogoSpinner";
import Spinner from "./Spinner";
import Spinkit from "./Spinkit";
import WishListIcon from "./WishListIcon";
import ProductPrice from "./ProductPrice";
import Empty from "./Empty";
import CategorySlider from "./CategorySlider";
import Video from "./Video";
import WebView from "./WebView";
import ProductItem from "./ProductItem";
import StepIndicator from "./StepIndicator";
import Accordion from "./Accordion";
import Drawer from "./Drawer";
import ProductRelated from "./ProductRelated";
import AdMob from "./AdMob";
import ImageCache from "./ImageCache";
import AnimatedHeader from "./AnimatedHeader";
import AppIntro from "./AppIntro";
import SafeAreaView from "./SafeAreaView";
import UserProfileHeader from "./UserProfileHeader";
import UserProfileItem from "./UserProfileItem";
import CurrencyPicker from "./CurrencyPicker";
import Text from "./Text";
import ModalBox from "./Modal";
import ModalLayout from "./Modal/Layout";
import HorizonLayout from "./HorizonLayout";
import HorizonList from "./HorizonList";
import ProductList from "./ProductList";
import CategoryCarousel from "./CategoryCarousel";
import SideMenu from "./SideMenu";
import ProductVendor from "./ProductList/VendorProducts";
import SearchBar from "./SearchBar";
import ModalReview from "./Modal/Review";
import Review from "./Review";
import CartButton from "./CartButton";
import TouchableScale from "./TouchableScale";
import ProductCatalog from "./ProductCatalog";
import ChatToolBar from "./Chat/ToolBar";
import ChatSearch from "./Chat/Search";
import ProductDetailFooter from "./ProductDetail/Footer";
import ProductDetailToolbar from "./ProductDetail/Toolbar";
import ProductDetailRating from "./ProductDetail/Rating";
import ProductTags from "./ProductTags";
import AddressItem from "./AddressItem";

import Chips from "./Chips";
import ConfirmCheckout from "./ConfirmCheckout";
import SplitCategories from "./SplitCategories";
import VendorInfo from "./VendorInfo";

import SelectCategory from "./SelectCategory";
import HeaderPage from "./HeaderPage";
import PostHeading from "./PostHeading";
import SelectImage from "./SelectImage";
import ProcessModal from './ProcessModal'
import PostNewsDialog from './PostNewsDialog'
import Slider from './Slider'

export {
  ProductList,
  Drawer,
  Button,
  ButtonIndex,
  ProductSize,
  ProductColor,
  TextInput,
  ShippingMethod,
  TabBarIcon,
  ToolbarIcon,
  NavigationBarIcon,
  CartIcon,
  CartIcons,
  TabBar,
  Search,
  Rating,
  PostBanner,
  PostList,
  FlatButton,
  ShopButton,
  PostLayout,
  LogoSpinner,
  Spinner,
  Spinkit,
  WishListIcon,
  ProductPrice,
  Empty,
  CategorySlider,
  Video,
  WebView,
  ProductItem,
  StepIndicator,
  Accordion,
  ProductRelated,
  HorizonLayout,
  HorizonList,
  AdMob,
  ImageCache,
  AnimatedHeader,
  AppIntro,
  SafeAreaView,
  UserProfileHeader,
  UserProfileItem,
  CurrencyPicker,
  Text,
  CategoryCarousel,
  ModalBox,
  SideMenu,
  ModalLayout,
  ProductVendor,
  SearchBar,
  ModalReview,
  Review,
  CartButton,
  TouchableScale,
  ChatToolBar,
  ChatSearch,
  ProductDetailFooter,
  ProductDetailToolbar,
  ProductDetailRating,
  ProductCatalog,
  ProductTags,
  AddressItem,
  Chips,
  ConfirmCheckout,
  SplitCategories,
  VendorInfo,
  HeaderPage,
  SelectCategory,
  PostHeading,
  SelectImage,
  ProcessModal,
  PostNewsDialog,
  Slider
};
