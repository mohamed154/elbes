import RefAPI from './CustomAPI'
import WPAPI from './WPAPI'
import firebaseApp from './Firebase'
export {
    RefAPI,
    WPAPI,
    firebaseApp
}