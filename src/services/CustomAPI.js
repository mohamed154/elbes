/** @format */
/**
 * TODO: need refactor
 */

import { AppConfig } from "@common";
import { Platform } from "react-native";
import { warn } from "@app/Omni";
/**
 * init class API
 * @param opt
 * @returns {WordpressAPI}
 * @constructor
 */
function WordpressAPI(opt) {
  if (!(this instanceof WordpressAPI)) {
    return new WordpressAPI(opt);
  }
  const newOpt = opt || {};
  this._setDefaultsOptions(newOpt);
}

/**
 * Default option
 * @param opt
 * @private
 */
WordpressAPI.prototype._setDefaultsOptions = async function(opt) {
  this.url = opt.url ? opt.url : AppConfig.WooCommerce.url;

  this.namespace = opt.namespace ? opt.namespace : "mstore/v1";
  this.namespaceDokan = opt.namespaceDokan ? opt.namespaceDokan : "dokan/v1";
};

WordpressAPI.prototype.getProductsByVendor = async function(data) {
  const storeId = data.storeId;

  const requestUrl = `${this.url}/wp-json/${
    this.namespace
  }/stores/${storeId}/products`;

  return this._request(requestUrl).then((data) => {
    return data;
  });
};

WordpressAPI.prototype.getVendor = async function(vendorId) {
  const requestUrl = `${this.url}/wp-json/${
    this.namespaceDokan
  }/stores/${vendorId}`;

  return this._request(requestUrl).then((data) => {
    return data;
  });
};

WordpressAPI.prototype.getAllVendors = async function(page) {
  const space = AppConfig.WooCommerce.useWCV
    ? this.namespace
    : this.namespaceDokan;

  const requestUrl = `${this.url}/wp-json/${space}/stores?page=${page}`;

  return this._request(requestUrl).then((data) => {
    return data;
  });
};

WordpressAPI.prototype.getCategories = async function() {
  const requestUrl = `${this.url}/wp-json/wc/V2/products/categories`;

  return this._request(requestUrl).then((data) => {
    // alert(JSON.stringify(data));
    return data;
  });
};

WordpressAPI.prototype.createComment = async function(data, callback) {
  let requestUrl = `${this.url}/api/mstore_user/post_comment/?insecure=cool`;
  alert(requestUrl);
  if (data) {
    requestUrl += `&${this.join(data, "&")}`;
  }
  // console.log(requestUrl)
  return this._request(requestUrl, data, callback);
};

WordpressAPI.prototype.getJWTToken = function(username, password) {
  var requestUrl = this.url + "/wp-json/jwt-auth/v1/token";
  var options = {
    method: "POST",
    headers: {
      Accept: "application/json",
      "Content-Type": "application/json",
    },
    body: JSON.stringify({ username, password }),
  };
  return fetch(requestUrl, options).then((response) => response.json());
};

WordpressAPI.prototype.join = function(obj, separator) {
  const arr = [];
  for (const key in obj) {
    if (obj.hasOwnProperty(key)) {
      arr.push(`${key}=${obj[key]}`);
    }
  }
  return arr.join(separator);
};

WordpressAPI.prototype.getNonce = function() {
  const requestUrl =
    this.url +
    "/api/get_nonce/?controller=mstore_user&method=generate_auth_cookie";

  return this._request(requestUrl).then(function(data) {
    return data;
  });
};

WordpressAPI.prototype.getNonceCreatePost = function() {
  const requestUrl = `${
    this.url
  }/api/get_nonce/?controller=posts&method=create_post`;
  return this._request(requestUrl).then((data) => {
    return data;
  });
};

WordpressAPI.prototype.generateAuthCookie = async function(email, password) {
  const data = await this.getNonce();
  if (typeof data.status !== "undefined" && data.status == "ok") {
    const nonce = data.nonce;
    const requestUrl = `${
      this.url
    }/api/mstore_user/generate_auth_cookie/?insecure=cool&nonce=${nonce}&username=${email}&password=${password}`;

    // warn(["user login", requestUrl]);

    return this._request(requestUrl);
  }
};

WordpressAPI.prototype.createPost = async function(user, title, content, obj) {
  const response = await this.getNonceCreatePost();
  const cookieName = await this.generateAuthCookie(user.email, user.password);
  if (typeof response.status !== "undefined" && response.status == "ok") {
    const nonce = response.nonce;
    const requestUrl = `${
      this.url
    }/api/posts/create_post/?insecure=cool&nonce=${nonce}&title=${title}&email=${
      user.email
    }&username=${user.username}&password=${user.password}&author=${
      user.username
    }&status='draft'&type='product'&meta_input=${JSON.stringify(obj)}&cookie=${
      cookieName.cookie_name
    }&content=${content}`;
    console.log("request " + requestUrl);
    return this._request(requestUrl);
  }
};

WordpressAPI.prototype._request = function(url, callback) {
  // console.log("request url ", url);
  return fetch(url)
    .then((response) => response.text()) // Convert to text instead of res.json()
    .then((text) => {
      if (Platform.OS === "android") {
        text = text.replace(/\r?\n/g, "").replace(/[\u0080-\uFFFF]/g, ""); // If android , I've removed unwanted chars.
      }
      return text;
    })
    .then((response) => JSON.parse(response))

    .catch((error, data) => {
      // console.log('1=error network -', error, data);
    })
    .then((responseData) => {
      // console.log("response ", responseData);
      if (typeof callback === "function") {
        callback();
      }
      // console.log('request result from ' + url, responseData);
      return responseData;
    })
    .catch((error) => {
      // console.log('2=error network -- ', error.message);
    });
};

export default new WordpressAPI();
